#!/usr/bin/env python3
# lista ssh tunnels
# ps aux | grep ssh

import ldap

from django.conf import settings
from django.dispatch import receiver
# import django_auth_ldap.backend.populate_user
import django_auth_ldap.backend
from django_auth_ldap.backend import LDAPBackend
import code
from .models import Userprofile
from django.contrib.auth.models import User
from .models import Policy


used_groups = {
    "cn=Studenci,o=fis": "Studenci",
    "cn=RadaWydzialu,o=fis": "Rada Wydziału",
    "cn=Nauczyciele_akademiccy,o=fis": "Nauczyciele akademiccy",
    "cn=Doktoranci,o=fis": "Doktoranci",
    "cn=InfStos,o=fis": "Studenci - Informatyka Stosowana",
    "cn=FizMed,o=fis": "Studenci - Fizyka Medyczna",
    "cn=FizTech,o=fis": "Studenci - Fizyka Techniczna",
    "cn=Samodzielni_pracownicy,o=fis": "Pracownicy samodzielni",
    "cn=Pracownicy,o=fis": "Pracownicy",
    "all": "all"
}


def group_is_supported(group_name):
    if group_name in used_groups.keys():
        return True
    return False


def ldap_group_name_to_beauty_name(group_name):
    return used_groups.get(group_name)


@receiver(django_auth_ldap.backend.populate_user,
          sender=LDAPBackend)
def save_user(sender, user, ldap_user,  **kwargs):
    # code.interact(local=locals())
    try:
        # user_exists = Userprofile.objects.get(user=user)
        user = User.objects.get(pk=user.id)
        user_exists = Userprofile.objects.get(user=user)
    except User.DoesNotExist:
        user.save()
        userprofile = Userprofile(user=user)
        userprofile.save()
    refresh_user_policy(user=user, ldap_user=ldap_user)
    # code.interact(local=locals())


def refresh_user_policy(user, ldap_user):
    refresh_db_policy()
    policies = Policy.objects.all()
    user_policies = get_user_ldap_groups(user, ldap_user)

    for policy in policies:
        if policy not in user.userprofile.policy_groups.all():
            if policy.policytype in user_policies:
                user.userprofile.policy_groups.add(policy)
                user.save()


def refresh_db_policy():
    policies = Policy.objects.all()
    ldap_groups = get_ldap_groups()
    for policy in policies:
        if policy.is_ldap_group:
            if policy.policytype not in ldap_groups:
                policy.delete()
    for ldap_group in ldap_groups:
        policytype_list = [
            policy.policytype
            for policy in policies]
        if ldap_group not in policytype_list:
            new_policy = Policy(policytype=ldap_group,
                                is_ldap_group=True)
            new_policy.save()


def get_ldap_groups(ldap_uri=settings.AUTH_LDAP_SERVER_URI,
                    search_filter=settings.SEARCH_GROUP_FILTER):
    ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, False)
    ldap.set_option(ldap.OPT_REFERRALS, False)

    # The next lines will also need to be changed to support your search requirements and directory
    baseDN = "o=fis"
    searchScope = ldap.SCOPE_SUBTREE
    retrieveAttributes = None
    searchFilter = search_filter

    try:
        l = ldap.initialize(ldap_uri)
        ldap_result_id = l.search(baseDN, searchScope,
                                  searchFilter,
                                  retrieveAttributes)
        result_set = []
        while 1:
            result_type, result_data = l.result(
                ldap_result_id, 0)
            if (result_data == []):
                break
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    result_set.append(result_data[0][0])
        # code.interact(local=locals())
        return result_set
    except ldap.LDAPError as e:
        print(e)


def get_user_ldap_groups(user, ldap_user=None):
    if ldap_user is None:
        ldapbackend = LDAPBackend()
        ldapbackend.settings.GLOBAL_OPTIONS =\
            settings.AUTH_LDAP_GLOBAL_OPTIONS
        ldapbackend.settings.SERVER_URI =\
            settings.AUTH_LDAP_SERVER_URI
        ldapbackend.settings.BIND_DN =\
            settings.AUTH_LDAP_BIND_DN
        ldapbackend.settings.BIND_PASSWORD =\
            settings.AUTH_LDAP_BIND_PASSWORD
        ldapbackend.settings.USER_ATTR_MAP =\
            settings.AUTH_LDAP_USER_ATTR_MAP
        ldapbackend.settings.GROUP_SEARCH =\
            settings.AUTH_LDAP_GROUP_SEARCH
        ldapbackend.settings.GROUP_TYPE =\
            settings.AUTH_LDAP_GROUP_TYPE
        ldapbackend.settings.BIND_AS_AUTHENTICATING_USER =\
            settings.AUTH_LDAP_BIND_AS_AUTHENTICATING_USER
        user = ldapbackend.authenticate(
            request=settings.AUTH_LDAP_SERVER_URI,
            username=user.username,
            password=user.password)
        # code.interact(local=locals())
        group_membership_list = user.ldap_user.attrs['groupmembership']
        return list(filter(lambda x: x is not None,
                           list(map(lambda x: x if group_is_supported(x) else None,
                                    group_membership_list))))
    else:
        group_membership_list = ldap_user.attrs['groupmembership']
        return list(filter(lambda x: x is not None,
                           list(map(lambda x: x if group_is_supported(x) else None,
                                    group_membership_list))))
