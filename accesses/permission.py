# import  rest_framework.permissions import BasePermission
import code
from .models import Userprofile


class IsOwnerOrReadOnly(object):
    """
    Object-level permission to only allow ownersof an object
    to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    @staticmethod
    def has_object_permission(request, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS
        # requests.
        # code.interact(local=locals())
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `owner`.
        return obj.owner == request.user


class IsOwner(object):
    """
    Object-level permission to only allow owners of
    an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    @staticmethod
    def has_object_permission(request, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        # Instance must have an attribute named `owner`.
        return obj.owner == request.user


class IsOwnerOrHavePolicyAccess(object):
    """
    Object-level permission to only allow owners of
    an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    @staticmethod
    def has_object_permission(request, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        # Instance must have an attribute named `owner`.
        if obj.owner == request.user:
            return True
        try:
            user_groups = Userprofile.objects.get(
                user=request.user).policy_groups.all()
            exercise_groups = obj.policies.all()
            # code.interact(local=locals())
            for group in exercise_groups:
                # code.interact(local=locals())
                if group in user_groups:
                    return True
            return False
        except Exception as e:
            print(e)
            raise


class IsAdministrator(object):
    """
    Object-level permission to only allow owners of
    an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """
    @staticmethod
    def has_permission(request):
        try:
            is_admin = Userprofile.objects.get(
                user=request.user).is_administrator
            if is_admin:
                return True
            return False
        except Exception as e:
            print(e)
            raise


from collections import Counter

from rest_framework.throttling import SimpleRateThrottle
from django.contrib.auth.models import User

from rest_framework.views import exception_handler
from rest_framework.exceptions import Throttled
from rest_framework.exceptions import ErrorDetail


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if isinstance(exc, Throttled): # check that a Throttled exception is raised
        # code.interact(local=locals())
        custom_response_data = { # prepare custom response data

            'detail': ErrorDetail('Przekroczona liczba prób. Spróbuj ponownie za %ds.' % exc.wait, response.data['detail'].code),
            'availableIn': '%ds'%exc.wait
        }
        response.data = custom_response_data # set the custom response data on response object

    return response


class UserLoginRateThrottle(SimpleRateThrottle):
    rate="123"
    def parse_rate(self, rate):
        """
        Given the request rate string, return a two tuple of:
        <allowed number of requests>, <period of time in seconds>

        So we always return a rate for 6 request per 10 minutes.

        Args:
            string: rate to be parsed, which we ignore.

        Returns:
            tuple:  <allowed number of requests>, <period of time in seconds>
        """
        return (6, 300)

    def get_cache_key(self, request, view):
        user = User.objects.filter(username=request.data.get('username'))
        ident = user[0].pk if user else self.get_ident(request)
        return self.cache_format % {
            'scope': self.scope,
            'ident': ident
        }
