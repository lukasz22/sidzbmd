from django.db import models
from enum import Enum
from django.contrib.auth.models import User


# class PolicyChoice(Enum):
#     STUDENT = "for_Studenci"
#     DOKTORANT = "for_Doktoranci"
#     NAUCZYCIEL = "for_Nauczyciele_akademiccy"
#     PRACOWNIK = "for_Pracownicy"
#     RESZTA = "for_pozostali"


class Policy(models.Model):
    """
    Policy model
    """
    policytype = models.CharField(max_length=255)
    is_ldap_group = models.BooleanField(default=True)

    def __unicode__(self):
        return "{0}".format(self.type)


class ExerciseVisibilityPermission(models.Model):
    name = models.CharField(max_length=150)
    owner = models.ForeignKey(User,
                              on_delete=models.CASCADE, null=True)
    group = models.ForeignKey(Policy,
                              on_delete=models.CASCADE)
    exercise_vis = models.BooleanField(default=False)
    intro_vis = models.BooleanField(default=True)
    hint_vis = models.BooleanField(default=True)
    solution_vis = models.BooleanField(default=True)

    class Meta:
        unique_together = ('name', 'owner',)
        verbose_name_plural = 'categories'
