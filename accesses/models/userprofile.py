from django.db import models
from django.contrib.auth.models import User
from .policy import Policy


class Userprofile(models.Model):
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE)
    policy_groups = models.ManyToManyField(Policy)
    is_administrator = models.BooleanField('administrator status', default=False)

    class Meta:
        db_table = "userprofile"


# from django.contrib.auth.models import AbstractUser
# from django.utils.translation import ugettext_lazy as _

# from .managers import CustomUserManager


# class CustomUser(AbstractUser):
#     username = None
#     email = models.EmailField(_('email address'), unique=True)
#
#     USERNAME_FIELD = 'email'
#     REQUIRED_FIELDS = []
#
#     objects = CustomUserManager()
#
#     def __str__(self):
#         return self.email

# python manage.py graph_models -a -g -o my_project_visualized.png
