from rest_framework import serializers
from django.contrib.auth.models import User
from ..models import Policy
import code

from accesses.ldap_lib import ldap_group_name_to_beauty_name


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'first_name',
                  'last_name')


# class SingleGroupSerializer(serializers.Serializer):
#     name = serializers.CharField(max_length=200)


class UserGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Policy
        fields = ('id', 'policytype', 'is_ldap_group')

    def to_representation(self, obj):
        # code.interact(local=locals())
        return {
            "id": obj.id,
            "policytype": ldap_group_name_to_beauty_name(obj.policytype),
            "is_ldap_group": obj.is_ldap_group
        }
