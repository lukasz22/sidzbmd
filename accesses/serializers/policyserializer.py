from rest_framework import serializers
from ..models import ExerciseVisibilityPermission
from ..models import Policy
import code

from accesses.ldap_lib import ldap_group_name_to_beauty_name
from django.db import models


def multiple_of_ten(value):
    raise serializers.CharField('Not a multiple of ten')

class ExerciseVisPermSerializer(serializers.ModelSerializer):
    owner = models.CharField(validators=[multiple_of_ten])
    # group = PolicySerializer(read_only=True)

    class Meta:
        model = ExerciseVisibilityPermission
        fields = ('id', 'name', 'owner',
                  'group', 'exercise_vis',
                  'intro_vis', 'hint_vis',
                  'solution_vis')
    # def update(self, instance, validated_data):
    #     code.interact(local=locals())
    #     instance.name = validated_data.get('name', instance.name)
    #     instance.owner = validated_data.get('group', instance.owner)
    #     instance.exercise_vis = validated_data.get('exercise_vis', instance.exercise_vis)
    #     instance.intro_vis = validated_data.get('intro_vis', instance.intro_vis)
    #     instance.hint_vis = validated_data.get('hint_vis', instance.hint_vis)
    #     instance.solution_vis = validated_data.get('solution_vis', instance.solution_vis)
    #     instance.save()
    #     return instance

    def to_representation(self, obj):
        # code.interact(local=locals())
        # policy = Policy.objects.get(pk=obj.group)
        # code.interact(local=locals())
        ex_serializer = PolicySerializer(obj.group)
        return {
            "id": obj.id,
            "name": obj.name,
            "owner": None if not hasattr(obj.owner, 'username') else obj.owner.username,
            "group": ex_serializer.data,
            "exercise_vis": obj.exercise_vis,
            "intro_vis": obj.intro_vis,
            "hint_vis": obj.hint_vis,
            "solution_vis": obj.solution_vis
        }

class PolicySerializer(serializers.ModelSerializer):
    class Meta:
        model = Policy
        fields = ('id', 'policytype')

    def to_representation(self, obj):
        # code.interact(local=locals())
        return {
            "id": obj.id,
            "policytype": ldap_group_name_to_beauty_name(obj.policytype)
        }

class ExerciseVisPermContainerSerializer(serializers.Serializer):
    total = serializers.IntegerField()
    userpermissions = ExerciseVisPermSerializer(many=True)

    def to_representation(self, obj):
        return {
          "total": obj['total'],
          "userpermissions": obj['userpermissions']
        }
