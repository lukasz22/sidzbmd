from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import get_object_or_404
#from django_auth_ldap.backend import LDAPBackend
#from django.conf import settings

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import status

from accesses.serializers.policyserializer import ExerciseVisPermSerializer
from accesses.serializers.policyserializer import ExerciseVisPermContainerSerializer
from accesses.serializers.userserializer import UserSerializer
from accesses.serializers.userserializer import UserGroupSerializer

from .models import Userprofile
from .models import Policy
from .models import ExerciseVisibilityPermission

from accesses.permission import IsAdministrator
from accesses.ldap_lib import group_is_supported
#from accesses.ldap_lib import get_ldap_groups

from functools import reduce
import operator
import code

class ExVisPermView(viewsets.ViewSet):

    def list(self, request):
        if IsAdministrator.has_permission(request):
            ex_vis_perm = ExerciseVisibilityPermission.objects.all()
        else:
            user = User.objects.get(username=request.user.username)
            # ex_vis_perm = user.exercisevisibilitypermission_set.all()
            # code.interact(local=locals())
            ex_vis_perm = ExerciseVisibilityPermission.objects.filter(
                Q(owner=user) | Q(owner=None))
        serializer = ExerciseVisPermSerializer(ex_vis_perm,
                                               many=True)
        return Response(serializer.data)

    def create(self, request):
        if not request.POST._mutable:
            request.POST._mutable = True
        try:
            request = self.update_owner_in_request(request)
        except User.DoesNotExist as e:
             return Response({"owner":["Podany login użytkownika nie istnieje "
                             "lub użytkownik nie zalogował się ani razu do serwisu."]},
                             status=status.HTTP_400_BAD_REQUEST)
        serializer = ExerciseVisPermSerializer(
            data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk):
        user = User.objects.get(
            username=request.user.username)
        # code.interact(local=locals())
        queryset = get_object_or_404(ExerciseVisibilityPermission, pk=pk)
        if IsAdministrator.has_permission(request):
            ex_vis_perm = queryset
        else:
            try:
                ex_vis_perm = user.exercisevisibilitypermission_set.get(
                    pk=pk)
            except ExerciseVisibilityPermission.DoesNotExist:
                return Response("forbidden", status=status.HTTP_403_FORBIDDEN)
        serializer = ExerciseVisPermSerializer(ex_vis_perm)
        return Response(serializer.data) # not list

    def update(self, request, pk):
        # code.interact(local=locals())
        user = User.objects.get(
            username=request.user.username)
        queryset = get_object_or_404(ExerciseVisibilityPermission, pk=pk)
        if not request.POST._mutable:
            request.POST._mutable = True
        try:
            request = self.update_owner_in_request(request)
        except User.DoesNotExist as e:
             return Response({"owner":["Podany login użytkownika nie istnieje "
                             "lub użytkownik nie zalogował się ani razu do serwisu."]},
                             status=status.HTTP_400_BAD_REQUEST)
        if IsAdministrator.has_permission(request):
            ex_vis_perm = queryset
        else:
            if request.user != queryset.owner:
                return Response(data='nie masz uprawnien',
                                status=status.HTTP_403_FORBIDDEN)
            ex_vis_perm = user.exercisevisibilitypermission_set.get(pk=pk)
        serializer = ExerciseVisPermSerializer(
            ex_vis_perm, data=request.data)
        # code.interact(local=locals())
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        user = User.objects.get(
            username=request.user.username)
        if pk is None:
            if IsAdministrator.has_permission(request):
                ans = ExerciseVisibilityPermission.objects.all().delete()
            else:
                ans = user.exercisevisibilitypermission_set.filter().delete()
            return Response(ans, status=status.HTTP_200_OK)
        queryset = get_object_or_404(ExerciseVisibilityPermission, pk=pk)
        if not queryset:
            return Response("bad request", status=status.HTTP_400_BAD_REQUEST)
        # code.interact(local=locals())
        if request.user == queryset.owner:
            ans = user.exercisevisibilitypermission_set.filter(
                pk=pk).delete()
            return Response(ans, status=status.HTTP_200_OK)
        elif IsAdministrator.has_permission(request):
            ans = ExerciseVisibilityPermission.objects.get(pk=pk).delete()
            return Response(ans, status=status.HTTP_200_OK)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    def convertToBoolean(self, value):
        if value in ['True', 'true', '1']:
            return True
        elif value in ['False', 'false', '0']:
            return False
        return ''

    @action(methods=['get'], detail=False)
    def find(self, request):
        # queryset = ExerciseVisibilityPermission.objects.filter(owner=request.user)
        if IsAdministrator.has_permission(request):
            queryset = ExerciseVisibilityPermission.objects.all()
        else:
            queryset = ExerciseVisibilityPermission.objects.filter(
                Q(owner=request.user) | Q(owner=None))
        operation = request.GET.get('operation', 'and')
        name = request.GET.get('name', '')
        group = request.GET.get('group', '')
        owner = request.GET.get('owner', '')#
        exercise_vis = request.GET.get('exercisevis', '')
        intro_vis = request.GET.get('introvis', '')
        hint_vis = request.GET.get('hintvis', '')
        solution_vis = request.GET.get('solutionvis', '')
        exercise_vis = self.convertToBoolean(exercise_vis)
        intro_vis = self.convertToBoolean(intro_vis)
        hint_vis = self.convertToBoolean(hint_vis)
        solution_vis = self.convertToBoolean(solution_vis)
        if operation == 'and':
            def funkcja(x, y):
                return operator.and_(x, y)
        elif operation == 'or':
            def funkcja(x, y):
                return operator.or_(x, y)
        else:
            # code.interact(local=locals())
            raise Exception
        q_list = [Q(name__icontains=name),
                  Q(group__policytype__icontains=group),
                  Q(exercise_vis=exercise_vis),
                  Q(intro_vis=intro_vis),
                  Q(hint_vis=hint_vis),
                  Q(solution_vis=solution_vis),
                  Q(owner__username__icontains=owner)]
                  # Q(language__id=language)]
        q_list_no_empty = [
            q_el for q_el in q_list if q_el.children[0][1] != '']
        # code.interact(local=locals())
        q_reduce = reduce(funkcja,
                          q_list_no_empty) if q_list_no_empty else Q()
        mod_queryset = queryset.filter(
            q_reduce).order_by('name')
        serializer = ExerciseVisPermSerializer(
            self.get_range(request, mod_queryset),
            many=True)
        serializer_wrapper = ExerciseVisPermContainerSerializer(
            {
             'total': len(mod_queryset),
             'userpermissions': serializer.data})
        # code.interact(local=locals())
        response = Response(serializer_wrapper.data)
        return response

    def get_range(self, request, queryset):
        page_number = int(request.GET.get('page', 1))
        page_size = int(request.GET.get('pagesize',
                                        len(queryset)))
        first = (page_number-1)*page_size
        last = page_number*page_size
        return queryset[first:last]

    def update_owner_in_request(self, request):
        if request.user.userprofile.is_administrator:
            owner_username = request.data.get('owner', request.user.username)
        else:
            owner_username = request.user.username
        request.data['owner'] = str(User.objects.get(username=owner_username).id)
        return request


class UserView(viewsets.ViewSet):

    def list(self, request):
        queryset = User.objects.get(
            username=request.user.username)
        serializer = UserSerializer(queryset)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def groups(self, request):
        # ldapbackend = LDAPBackend()
        # ldapbackend.settings.GLOBAL_OPTIONS =\
        #     settings.AUTH_LDAP_GLOBAL_OPTIONS
        # ldapbackend.settings.SERVER_URI =\
        #     settings.AUTH_LDAP_SERVER_URI
        # ldapbackend.settings.BIND_DN =\
        #     settings.AUTH_LDAP_BIND_DN
        # ldapbackend.settings.BIND_PASSWORD =\
        #     settings.AUTH_LDAP_BIND_PASSWORD
        # ldapbackend.settings.USER_ATTR_MAP =\
        #     settings.AUTH_LDAP_USER_ATTR_MAP
        # ldapbackend.settings.GROUP_SEARCH =\
        #     settings.AUTH_LDAP_GROUP_SEARCH
        # ldapbackend.settings.GROUP_TYPE =\
        #     settings.AUTH_LDAP_GROUP_TYPE
        # ldapbackend.settings.BIND_AS_AUTHENTICATING_USER =\
        #     settings.AUTH_LDAP_BIND_AS_AUTHENTICATING_USER
        # user = ldapbackend.authenticate(
        #     username=request.user.username,
        #     password=request.user.password)
        # code.interact(local=locals())
        # groups_data = [
        #     {'groupname': group.policytype}
        #     for group in request.user.userprofile.policy_groups.all()]
        # code.interact(local=locals())
        policy_groups_queryset = request.user.userprofile.policy_groups.all()
        id_policy_type_list = list(policy_groups_queryset.values('id', 'policytype'))
        # code.interact(local=locals())
        id_none_list = list(map(lambda policy: policy['id']
                                if group_is_supported(policy['policytype']) else None,
                                id_policy_type_list))

        id_list = list(filter(lambda id: id is not None, id_none_list))
        # code.interact(local=locals())
        serializer = UserGroupSerializer(Policy.objects.filter(id__in=id_list),
                                         many=True)
        # if not serializer.is_valid():
        #     return Response(serializer.errors,
        #                     status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(serializer.data)


class GroupView(viewsets.ViewSet):
    def list(self, request):
        policy = Policy.objects.all()
        # ex_vis_perm = user.exercisevisibilitypermission_set.all()
        # serializer = ExerciseVisPermSerializer(ex_vis_perm,
        #                                        many=True)
        # return Response(serializer.data)
        # dns_groups = get_ldap_groups()
        # groups_data = [
        #     {'groupname': group}
        #     for group in dns_groups]
        # code.interact(local=locals())

        id_policy_type_list = list(policy.values('id', 'policytype'))
        # code.interact(local=locals())
        id_none_list = list(map(lambda policy: policy['id']
                                if group_is_supported(policy['policytype']) else None,
                                id_policy_type_list))

        id_list = list(filter(lambda id: id is not None, id_none_list))

        serializer = UserGroupSerializer(Policy.objects.filter(id__in=id_list),
                                         many=True)
        # if not serializer.is_valid():
        #     return Response(serializer.errors,
        #                     status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(serializer.data)


################
from rest_framework_jwt.utils import jwt_decode_handler
def jwt_response_payload_handler(token, user=None, request=None):
    decoded_token = jwt_decode_handler(token)
    obj = Userprofile.objects.get(user__username=user.username)
    return {
        'token': token,
        'is_admin': obj.is_administrator,
        'exp': decoded_token['exp'] # A Unix timestamp is the number of seconds
                                 # between a particular date
                                 #and January 1, 1970 at UTC.
    }

################
from .permission import UserLoginRateThrottle
from rest_framework_jwt.views import ObtainJSONWebToken

class ThrottleObtainAuthToken(ObtainJSONWebToken):
    throttle_classes = (UserLoginRateThrottle,)

throttle_obtain_jwt_token = ThrottleObtainAuthToken.as_view()
