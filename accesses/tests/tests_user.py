from django.test import TestCase
from django.contrib.auth.models import User
from django.db.models import Q

from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient
from rest_framework.test import force_authenticate
from rest_framework import status

from ..models import ExerciseVisibilityPermission

from accesses.serializers.policyserializer import ExerciseVisPermSerializer
from accesses import serializers
from accesses import views
from accesses.views import ExVisPermView

import code

from django.test import TestCase
from django.contrib.auth.models import User
#from django.contrib.auth import authenticate, login, logout




class UserViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))

    def tearDown(self):
        print("E---------end test----------")

    def test_get_user_detail(self):
        factory = APIRequestFactory()
        request = factory.get('/user/')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        view = views.UserView.as_view({'get': 'list'})
        serializer = serializers.UserSerializer(user)
        response = view(request)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_get_user_groups(self):
        factory = APIRequestFactory()
        request = factory.get('/user/groups')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        view = views.UserView.as_view({'get': 'groups'})
        response = view(request)
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertIsInstance(response.data, list)
        self.assertIsInstance(response.data[0], dict)

# ssh -N -f -L 9999:orion.fis.agh.edu.pl:636 3bartocha@taurus.fis.agh.edu.pl
class JwtTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        pass

    def test_jwt_auth_jwt_header(self):
        client = APIClient()
        user_num_before = len(User.objects.all())
        data = {"username": "3bartocha",
                "password": "zmienioneHaslo22"}
        response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
        user_num_after = len(User.objects.all())
        self.assertEqual(user_num_after, user_num_before)
        self.assertIsInstance(response.data['token'], str)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        client.credentials(
            HTTP_AUTHORIZATION='JWT ' + response.data['token'])
        response = client.get("/sidzbmd/api/exercises/")
        # print(response)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertIsInstance(response.data['exercises'], list)
        self.assertEqual(len(response.data['exercises']), 22)

    def test_jwt_auth_jwt_no_header(self):
        client = APIClient()
        data = {"username": "3bartocha",
                "password": "zmienioneHaslo22"}
        response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
        self.assertIsInstance(response.data['token'], str)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        response = client.get("/sidzbmd/api/exercises/")
        self.assertEqual(response.status_code,
                         status.HTTP_401_UNAUTHORIZED)
        self.assertIsInstance(response.data, dict)
        self.assertEqual(response.data['detail'].code,
                         'not_authenticated')

    def test_jwt_auth_bad_password(self):
        client = APIClient()
        user_num_before = len(User.objects.all())
        data = {"username": "3bartocha",
                "password": "NiepoprawneHaslo"}
        response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
        user_num_after = len(User.objects.all())
        print(response)
        self.assertEqual(user_num_after, user_num_before)
        self.assertIsInstance(response.data, dict)
        self.assertEqual(
            response.data['non_field_errors'][0].code,
            'invalid')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)

    def test_jwt_auth_bad_username(self):
        client = APIClient()
        user_num_before = len(User.objects.all())
        data = {"username": "3bartochaQ",
                "password": "NiepoprawneHaslo"}
        response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
        user_num_after = len(User.objects.all())
        print(response)
        self.assertEqual(user_num_after, user_num_before)
        self.assertIsInstance(response.data, dict)
        # code.interact(local=locals())

        self.assertEqual(
            response.data['non_field_errors'][0].code,
            'invalid')
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)

class JwtThrottleTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        cache.clear()
        pass

    def test_jwt_auth_jwt_header_throttle_exercise(self):
        client = APIClient()
        user_num_before = len(User.objects.all())
        data = {"username": "3bartocha",
                "password": "zmienioneHaslo22"}
        response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
        response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
        response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
        user_num_after = len(User.objects.all())
        self.assertEqual(user_num_after, user_num_before)
        self.assertIsInstance(response.data['token'], str)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        client.credentials(
            HTTP_AUTHORIZATION='JWT ' + response.data['token'])
        for it in range(10):
            response = client.get("/sidzbmd/api/exercises/")
            print(response)
            self.assertEqual(response.status_code,
                             status.HTTP_200_OK)
            self.assertIsInstance(response.data['exercises'], list)
            self.assertEqual(len(response.data['exercises']), 22)

    def test_jwt_auth_bad_password_throttle_user_known(self):
        client = APIClient()
        user_num_before = len(User.objects.all())
        data = {"username": "john2",
                "password": "NiepoprawneHaslo"}
        for it in range(10):
            response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
            user_num_after = len(User.objects.all())
            print(response)
            print(response.data)
            self.assertEqual(user_num_after, user_num_before)
            self.assertIsInstance(response.data, dict)
            if it < 6:
                self.assertEqual(
                    response.data['non_field_errors'][0].code,
                    'invalid')
                self.assertEqual(response.status_code,
                             status.HTTP_400_BAD_REQUEST)
            else:
                # code.interact(local=locals())
                self.assertEqual(
                    response.data['detail'].code,
                    'throttled')
                self.assertEqual(response.status_code,
                             status.HTTP_429_TOO_MANY_REQUESTS)
            print("------")

    def test_jwt_auth_bad_password_throttle_user_unknown(self):
        client = APIClient()
        user_num_before = len(User.objects.all())
        data = {"username": "3bajorek",
                "password": "NiepoprawneHaslo"}
        for it in range(10):
            response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
            user_num_after = len(User.objects.all())
            print(response)
            print(response.data)
            self.assertEqual(user_num_after, user_num_before)
            self.assertIsInstance(response.data, dict)
            if it < 6:
                self.assertEqual(
                    response.data['non_field_errors'][0].code,
                    'invalid')
                self.assertEqual(response.status_code,
                             status.HTTP_400_BAD_REQUEST)
            else:
                # code.interact(local=locals())
                self.assertEqual(
                    response.data['detail'].code,
                    'throttled')
                self.assertEqual(response.status_code,
                             status.HTTP_429_TOO_MANY_REQUESTS)
            print("------")

from django.core.cache import cache

class UserDoesntExistTestCase(TestCase):
    fixtures = ["user_no_exist_fixtures.json"]

    def setUp(self):
        cache.clear()
        pass

    def test_jwt_auth_jwt_header(self):
        client = APIClient()
        user_num_before = len(User.objects.all())
        data = {"username": "3bartocha",
                "password": "zmienioneHaslo22"}
        response = client.post("/sidzbmd/api/api-token-auth/",
                               data, format='json')
        user_num_after = len(User.objects.all())
        self.assertEqual(user_num_after,
                         user_num_before+1)
        self.assertIsInstance(response.data['token'], str)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        client.credentials(
            HTTP_AUTHORIZATION='JWT ' + response.data['token'])
        response = client.get("/sidzbmd/api/exercises/")
        print(response)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertIsInstance(response.data['exercises'], list)
        # self.assertEqual(len(response.data), 21)
        self.assertEqual(len(response.data['exercises']), 20)
        # chyba dlatego ze nie ma mnie w grupie rada wydzialu

        # DONE wyświetlanie danych Usera, w tym jego grupy
        # DONE wyswietlanie dostepnych grup
        # DONE podkategorie
            # DONE jeszcze dodawanie i usuwanie podkategorii
        # DONE  poprawić wyszukiwanie po kategoriach
        # DONE rozszerzyć tabele policy o pole: full, only_zadanie, zadanie_intro
            # zadanie_hint, zadanie z rozwiazaniem; intro_true, hint_true
        # DONE generowanie dokumentu na pdostawie recept
        # zwracanie kodu latex z obrazkami jako zip ?
        # dołożenie obrazków do pdf
        # post post aby posprzatac po wygenerowaniu dokumentu
        # django celery ?
        # DONE załączniki
            # DONE przy dodawaniu zadania możesz dodać załączniki
            # DONE przy usuwaniu zadania usuwasz zalaczniki
            #
            # DONE dodawać, usuwać załączniki
            # DONE !!! TESTY do załączniki !!!
            # DONE files clean up ,when deleted
        # DONE dodawanie permission visibility
            # DONE implementacja
            # DONE !!! TESTY permission visibility !!!

        # DONE języki

# import ldap
# from django.contrib.auth import authenticate, login, logout
# from django_auth_ldap.config import LDAPSearch, GroupOfNamesType
# LDAPSearch("o=fis",ldap.SCOPE_SUBTREE,"(uid=%(user)s)")
# user = authenticate(username="3bartocha", password="zmienioneHaslo22")
