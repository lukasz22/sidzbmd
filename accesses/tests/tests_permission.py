from django.test import TestCase
from django.contrib.auth.models import User
from django.db.models import Q

from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate
from rest_framework import status

from accesses.models import ExerciseVisibilityPermission
from accesses.serializers.policyserializer import ExerciseVisPermSerializer

from accesses import serializers
from accesses import views
from accesses.views import ExVisPermView

import code


class ExVisPermViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))

    def tearDown(self):
        print("E---------end test----------")

    def test_get_permissions(self):
        factory = APIRequestFactory()
        view = views.ExVisPermView.as_view({'get': 'list'})
        request = factory.get('user/exercise/permissions/')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request)
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        self.assertEqual(exvis_num_after,
                         exvis_num_before)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(len(response.data), 4)

    def test_get_permission(self):
        factory = APIRequestFactory()
        view = views.ExVisPermView.as_view({'get': 'retrieve'})
        request = factory.get('user/exercise/permissions/1')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request, pk="1")
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        self.assertEqual(exvis_num_after,
                         exvis_num_before)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(response.data['id'], 1)

    def test_get_permission_not_my(self):
        factory = APIRequestFactory()
        view = views.ExVisPermView.as_view({'get': 'retrieve'})
        request = factory.get('user/exercise/permissions/4')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request, pk="4")
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        self.assertEqual(exvis_num_after,
                         exvis_num_before)
        self.assertEqual(response.status_code,
                         status.HTTP_403_FORBIDDEN)

    def test_create_permission(self):
        factory = APIRequestFactory()
        data = {
            "name": "Doktoranci zadanie plus wskazówka",
            "group": "4",
            "exercise_vis": True,
            "intro_vis": False,
            "hint_vis": True,
            "solution_vis": False
        }
        view = views.ExVisPermView.as_view({'post': 'create'})
        request = factory.post('user/exercise/permissions/', data,
                               format='json')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exvis_num_before_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request)
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        exvis_num_after_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exvis_num_after,
                         exvis_num_before+1)
        self.assertEqual(exvis_num_after_user,
                         exvis_num_before_user+1)

    def test_update_permission(self):
        factory = APIRequestFactory()
        data = {
            "name": "Update for_students",
            "group": "3",
            "exercise_vis": True,
            "intro_vis": False,
            "hint_vis": True,
            "solution_vis": False
        }
        view = views.ExVisPermView.as_view({'put': 'update'})
        request = factory.put('user/exercise/permissions/', data,
                              format='json')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exvis_num_before_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request, pk="1")
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        exvis_num_after_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exvis_num_after,
                         exvis_num_before)
        self.assertEqual(exvis_num_after_user,
                         exvis_num_before_user)
        # code.interact(local=locals())
        self.assertEqual(response.data["name"],
                         data["name"])
        self.assertEqual(
            int(ExerciseVisibilityPermission.objects.get(pk="1").group.id),
            int(data["group"]))

    def test_update_permission_not_my(self):
        factory = APIRequestFactory()
        data = {
            "name": "Update for_students",
            "group": "4",
            "exercise_vis": True,
            "intro_vis": False,
            "hint_vis": True,
            "solution_vis": False
        }
        view = views.ExVisPermView.as_view({'put': 'update'})
        request = factory.put('user/exercise/permissions/', data,
                              format='json')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exvis_num_before_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request, pk="4")
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        exvis_num_after_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        self.assertEqual(response.status_code,
                         status.HTTP_403_FORBIDDEN)
        self.assertEqual(exvis_num_after,
                         exvis_num_before)
        self.assertEqual(exvis_num_after_user,
                         exvis_num_before_user)

    def test_destroy_permission(self):
        factory = APIRequestFactory()
        request = factory.delete("user/exercise/permissions/1",
                                 format='json')
        view = views.ExVisPermView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exvis_num_before_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request, pk="1")
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        exvis_num_after_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exvis_num_after,
                         exvis_num_before-1)
        self.assertEqual(exvis_num_after_user,
                         exvis_num_before_user-1)

    def test_destroy_permission_not_my(self):
        factory = APIRequestFactory()
        request = factory.delete("user/exercise/permissions/4",
                                 format='json')
        view = views.ExVisPermView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        exvis_num_before_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request, pk="1")
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        exvis_num_after_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        self.assertEqual(response.status_code,
                         status.HTTP_403_FORBIDDEN)
        self.assertEqual(exvis_num_after,
                         exvis_num_before)
        self.assertEqual(exvis_num_after_user,
                         exvis_num_before_user)

    def test_destroy_permissions(self):
        factory = APIRequestFactory()
        request = factory.delete("user/exercise/permissions/",
                                 format='json')
        view = views.ExVisPermView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exvis_num_before_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        exvis_num_before = ExerciseVisibilityPermission.objects.count()
        response = view(request)
        exvis_num_after = ExerciseVisibilityPermission.objects.count()
        exvis_num_after_user = ExerciseVisibilityPermission.objects.filter(
            owner=user).count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exvis_num_after,
                         exvis_num_before-3)
        self.assertEqual(exvis_num_after_user,
                         0)

    def test_find_userpermission_bu_name(self):
        factory = APIRequestFactory()
        view = views.ExVisPermView.as_view({'get': 'find'})
        request = factory.get('/user/exercise/permissions/find/?name=students')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = ExerciseVisibilityPermission.objects.filter(
            Q(name__icontains='students') & Q(owner__username='3bartocha')).order_by("name")
        serializer = ExerciseVisPermSerializer(
             languages, many=True)
        self.assertEqual(response.data['userpermissions'], serializer.data)
        self.assertEqual(len(response.data['userpermissions']), 1)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_userpermission_by_solutionvis_true(self):
        factory = APIRequestFactory()
        view = views.ExVisPermView.as_view({'get': 'find'})
        request = factory.get('/user/exercise/permissions/find/?solutionvis=True')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = ExerciseVisibilityPermission.objects.filter(
            Q(solution_vis=True) & Q(owner__username='3bartocha')).order_by("name")
        serializer = ExerciseVisPermSerializer(
             languages, many=True)
        self.assertEqual(response.data['userpermissions'], serializer.data)
        self.assertEqual(len(response.data['userpermissions']), 2)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_userpermission_by_solutionvis_false(self):
        factory = APIRequestFactory()
        view = views.ExVisPermView.as_view({'get': 'find'})
        request = factory.get('/user/exercise/permissions/find/?solutionvis=False')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = ExerciseVisibilityPermission.objects.filter(
            Q(solution_vis=False) & Q(Q(owner__username='3bartocha') | Q(owner=None))).order_by("name")
        serializer = ExerciseVisPermSerializer(
             languages, many=True)
        # code.interact(local=locals())
        self.assertEqual(response.data['userpermissions'], serializer.data)
        self.assertEqual(len(response.data['userpermissions']), 2)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
