from django.test import TestCase
from django.contrib.auth.models import User
from django.db.models import Q

from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate
from rest_framework import status

from ..models import ExerciseVisibilityPermission
from ..models import Policy

from accesses.serializers.policyserializer import ExerciseVisPermSerializer
from accesses import serializers
from accesses import views
from accesses.views import ExVisPermView

from accesses.ldap_lib import get_user_ldap_groups
from accesses.ldap_lib import refresh_db_policy

import mock

import code

class GroupViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))

    def tearDown(self):
        print("E---------end test----------")

    def test_get_user_groups(self):
        factory = APIRequestFactory()
        request = factory.get('/groups')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        view = views.GroupView.as_view({'get': 'list'})
        response = view(request)
        print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertIsInstance(response.data, list)
        self.assertIsInstance(response.data[0], dict)


class RefreshPolicyModelTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        pass

    @mock.patch('accesses.ldap_lib.get_ldap_groups')
    def test_policy_model(self, user_ldap_func):
        policies_len_before = len(Policy.objects.all())
        user_ldap_func.return_value = ["cn=RadaWydzialu,o=fis",
                                       "nowa_grupa_ldap"]
        # code.interact(local=locals())
        self.assertEqual(policies_len_before, 5)
        refresh_db_policy()
        policies_len_after = len(Policy.objects.all())
        self.assertEqual(policies_len_after, 3)

    def test_get_user_ldap_groups(self):
        user = User.objects.get(username="3bartocha")
        zmienna = get_user_ldap_groups(user)
        # code.interact(local=locals())
        print(zmienna)
        expected = ['cn=InfStos,o=fis',
                    'cn=Studenci,o=fis']#,
                    # 'cn=UnixUsers,o=fis',
                    # 'cn=RadaWydzialu,o=fis',
                    #'cn=Rok_5,o=fis']
        # expected = ['cn=Studenci,o=fis',
        #             'cn=RadaWydzialu,o=fis']
        self.assertEqual(zmienna, expected)
