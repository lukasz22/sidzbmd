
rm -rf db.sqlite3
rm -rf ./tex_resources/migrations
rm -rf ./recipes/migrations
python manage.py flush --no-input
echo 'drop owned by lukasz;' | python manage.py dbshell
python manage.py makemigrations tex_resources
python manage.py makemigrations accesses
python manage.py makemigrations auth
python manage.py makemigrations recipes
python manage.py migrate --run-syncdb
python manage.py loaddata many_exercises.json
python manage.py loaddata sample_preambules.json
