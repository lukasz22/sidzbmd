from django.shortcuts import render
from rest_framework import viewsets
from recipes.latexlib import LatexExercise, LatexFile, LatexAttachment
from rest_framework.decorators import action
from django.contrib.auth.models import User
# from recipes.serializers import LatexExerciseSerializer
# from recipes.serializers import LatexDocumentSerializer
from tex_resources.models.exercise import Exercise
import subprocess
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse

from recipes.models.models import ExerciseRecipe
from recipes.models.models import ExerciseDocumentRecipe
from recipes.serializers import ExerciseDocumentRecipeSerializer
from recipes.serializers import ExerciseRecipeSerializer
from recipes.serializers import DocumentContainerSerializer
from recipes.serializers import DocumentPDFContainerSerializer

import shutil
from functools import reduce
from django.db.models import Q
from accesses.permission import IsOwnerOrHavePolicyAccess
import operator
import os
import base64

from recipes.latexlib import LatexAttachment
import code
from copy import deepcopy

from tex_resources.models.attachment import AttachmentTypes
from tex_resources.models.attachment import Attachment

from tex_resources.models.preambule import Preambule
from django.shortcuts import get_object_or_404

class ExerciseRecipeView(viewsets.ViewSet):

    def list(self, request, document_pk):
        queryset = get_object_or_404(ExerciseDocumentRecipe, pk=document_pk)
        if queryset.owner != request.user:
            return Response("forbidden", status=status.HTTP_403_FORBIDDEN)
        # code.interact(local=locals())
        mykwargs = {"owner": request.user}
        serializer = ExerciseRecipeSerializer(
            queryset.exercise_recipes.all().order_by('position_number'), many=True, **mykwargs)
        return Response(serializer.data)

    def retrieve(self, request, document_pk, pk):
        queryset = get_object_or_404(ExerciseDocumentRecipe, pk=document_pk)
        if queryset.owner != request.user:
            return Response("forbidden", status=status.HTTP_403_FORBIDDEN)
        obj = get_object_or_404(queryset.exercise_recipes, pk=pk)
        try:
            mykwargs = {"owner": request.user}
            serializer = ExerciseRecipeSerializer(
                queryset.exercise_recipes.get(id=pk), **mykwargs)
        except ExerciseRecipe.DoesNotExist:
            return Response("forbidden", status=status.HTTP_403_FORBIDDEN)
        return Response(serializer.data) # not list

    def create(self, request, document_pk):
        queryset = get_object_or_404(ExerciseDocumentRecipe, pk=document_pk)
        if queryset.owner == request.user:
            if not request.POST._mutable:
                request.POST._mutable = True
            # code.interact(local=locals())
            for i in range(len(request.data)):
                request.data[i]['document'] = document_pk

            mykwargs = {"owner": request.user}
            serializer = ExerciseRecipeSerializer(
                data=request.data, many=True, **mykwargs)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    def update(self, request, document_pk, pk=None):
        queryset = get_object_or_404(ExerciseDocumentRecipe, pk=document_pk)
        if queryset.owner == request.user:
            if not request.POST._mutable:
                request.POST._mutable = True
            mykwargs = {"owner": request.user}
            if pk is not None:
                request.data['document'] = document_pk
                recipe = get_object_or_404(queryset.exercise_recipes, pk=pk)
                serializer = ExerciseRecipeSerializer(
                    recipe, data=request.data, **mykwargs)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data,
                                    status=status.HTTP_200_OK)
                return Response(serializer.errors,
                                status=status.HTTP_400_BAD_REQUEST)
            else:
                is_valid, serializers = luki_function(request.data, request.user, queryset, document_pk)
                if is_valid:
                    for serializer in serializers:
                        serializer.save()
                    datas = [serializer.data for serializer in serializers]
                    return Response(datas,
                                    status=status.HTTP_200_OK)
                else:
                    errors = [serializer.errors for serializer in serializers]
                    return Response(errors,
                                    status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, document_pk, pk=None):
        queryset = get_object_or_404(ExerciseDocumentRecipe, pk=document_pk)
        if queryset.owner == request.user:
            if pk is None:
                ans = ExerciseRecipe.objects.filter(
                    Q(document=queryset)).delete()
                return Response(ans, status=status.HTTP_200_OK)
            else:
                recipe = get_object_or_404(queryset.exercise_recipes, pk=pk)
                ans = ExerciseRecipe.objects.filter(
                    Q(pk=pk) & Q(document=queryset)).delete()
                return Response(ans, status=status.HTTP_200_OK)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)


class DocumentGeneratorView(viewsets.ViewSet):

    def list(self, request):
        queryset = ExerciseDocumentRecipe.objects.filter(owner=request.user)
        mykwargs = {"owner": request.user}
        serializer = ExerciseDocumentRecipeSerializer(queryset, many=True,
                                                      **mykwargs)
        return Response(serializer.data)

    def retrieve(self, request, pk):
        # code.interact(local=locals())
        queryset0 = get_object_or_404(ExerciseDocumentRecipe, pk=pk)
        if queryset0.owner == request.user:
            queryset = ExerciseDocumentRecipe.objects.get(Q(pk=pk) &
                                                          Q(owner=request.user))
            mykwargs = {"owner": request.user}
            serializer = ExerciseDocumentRecipeSerializer(queryset, **mykwargs)
            return Response(serializer.data) # not list
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    def create(self, request):
        if not request.POST._mutable:
            request.POST._mutable = True
        mykwargs = {"owner": request.user}
        serializer = ExerciseDocumentRecipeSerializer(data=request.data,
                                                      **mykwargs)
        if serializer.is_valid():
            serializer.save()
            # code.interact(local=locals())

            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        if pk is None:
            ans = ExerciseDocumentRecipe.objects.filter(
                Q(owner=request.user)).delete()
            return Response(ans, status=status.HTTP_200_OK)
        else:
            queryset = get_object_or_404(ExerciseDocumentRecipe, pk=pk)
            if queryset.owner == request.user:
                ans = ExerciseDocumentRecipe.objects.filter(
                    Q(pk=pk) & Q(owner=request.user)).delete()
                return Response(ans, status=status.HTTP_200_OK)
            else:
                return Response(data='nie masz uprawnien',
                                status=status.HTTP_403_FORBIDDEN)

    def update(self, request, pk, format='json'):
        queryset = get_object_or_404(ExerciseDocumentRecipe, pk=pk)
        if queryset.owner == request.user:
            document = ExerciseDocumentRecipe.objects.get(Q(pk=pk) &
                                                          Q(owner=request.user))
            mykwargs = {"owner": request.user}
            # code.interact(local=locals())
            ex_rec_serial_list = []

            serializer = ExerciseDocumentRecipeSerializer(document,
                                                          data=request.data,
                                                          **mykwargs)
            is_valid, serializers = luki_function(request.data['exercise_recipes'], request.user, document, pk)
            if serializer.is_valid() and is_valid:
                serializer.save()
                for serializer_ex in serializers:
                    serializer_ex.save()
                datas = [serializer_ex.data for serializer_ex in serializers]
                return Response(serializer.data,
                                status=status.HTTP_200_OK)
            else:
                errors = [serializer_ex.errors for serializer_ex in serializers]
                return Response(errors + [serializer.errors],
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    def prepare_latex_file_obj(self, serializer):
        tex_file = LatexFile(
            document_title=serializer.data["document_title"],
            author=serializer.data["author"],
            date=serializer.data["date"],
            preambule=get_preambule_content(),
            is_title_part=serializer.data["is_title_part"])
        # code.interact(local=locals())
        for single_exercise in serializer.data["exercise_recipes"]:
            # db_exercise = Exercise.objects.get(
            #     pk=single_exercise['exercise_id'])
            tex_exercise = LatexExercise(
                title=single_exercise["exercise"]["title"],
                intro=single_exercise["exercise"]["intro"],
                content=single_exercise["exercise"]["task_content"],
                hint=single_exercise["exercise"]["hint"],
                solution=single_exercise["exercise"]["solution"],
                is_hint=single_exercise["is_hint"],
                is_title=single_exercise["is_title"],
                is_intro=single_exercise["is_intro"],
                is_solution=single_exercise["is_solution"])
            for single_attachment in single_exercise["exercise"]["attachments"]:
                # code.interact(local=locals())
                tex_attachment = LatexAttachment(
                    width=single_attachment["width"],
                    image_path=Attachment.objects.get(id=single_attachment['id']).image.path,
                    caption=single_attachment["caption"],
                    label=single_attachment["label"])
                # att_type = AttachmentTypes(int(single_attachment["attachment_type"])).name.split("_")[1]
                att_type = single_attachment["attachment_type"].split("_")[1]
                tex_exercise.add_attachment_obj(
                    att_type,
                    tex_attachment)
            tex_file.add_exercise_obj(tex_exercise)
        return tex_file

    @action(methods=['post'], detail=True)
    def generate(self, request, pk):
        queryset = get_object_or_404(ExerciseDocumentRecipe, pk=pk)
        if queryset.owner == request.user:
            queryset = ExerciseDocumentRecipe.objects.get(Q(pk=pk) &
                                                          Q(owner=request.user))
            mykwargs = {"owner": request.user}
            # code.interact(local=locals())
            serializer = ExerciseDocumentRecipeSerializer(queryset,
                                                          **mykwargs)
            # code.interact(local=locals())
            if queryset:
                tex_file = self.prepare_latex_file_obj(serializer)
                # code.interact(local=locals())
                try:
                    if tex_file.is_valid(repeat_num=3):
                        serializer_wrapper = DocumentPDFContainerSerializer(
                            {
                             'base64_string': tex_file.data["base64_string"],
                             'filename': serializer.data["filename"]})
                        return Response(serializer_wrapper.data)
                    else:
                        return Response(
                            {'latex_errorlog': '"{0}"'.format(tex_file.errors),
                             'validation': 'Wystąpił błąd podczas generowania dokumentu PDF. '
                                           'Upewnij się, że kod LaTeX jest poprawny'},
                            status=status.HTTP_400_BAD_REQUEST)
                except IOError as e:
                    return Response(
                        {'ioerrorlog': '"{0}"'.format(e.stdout)},
                        status=status.HTTP_400_BAD_REQUEST)
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    @action(methods=['post'], detail=True)
    def generatetex(self, request, pk):
        queryset = get_object_or_404(ExerciseDocumentRecipe, pk=pk)
        if queryset.owner == request.user:
            queryset = ExerciseDocumentRecipe.objects.get(Q(pk=pk) &
                                                          Q(owner=request.user))
            mykwargs = {"owner": request.user}
            # code.interact(local=locals())
            serializer = ExerciseDocumentRecipeSerializer(queryset,
                                                          **mykwargs)
            # code.interact(local=locals())
            if queryset:
                tex_file = self.prepare_latex_file_obj(serializer)
                tex_file.zip()
                serializer_wrapper = DocumentPDFContainerSerializer(
                    {
                     'base64_string': tex_file.data["zip_as_base64_string"],
                     'filename': serializer.data["filename"]})
                return Response(serializer_wrapper.data)
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    @action(methods=['get'], detail=False)
    def find(self, request):
        queryset = ExerciseDocumentRecipe.objects.filter(owner=request.user)
        operation = request.GET.get('operation', 'and')
        document_title = request.GET.get('documenttitle', '')
        author = request.GET.get('author', '')
        filename = request.GET.get('filename', '')
        name = request.GET.get('name', '')
        description = request.GET.get('description', '')
        if operation == 'and':
            def funkcja(x, y):
                return operator.and_(x, y)
        elif operation == 'or':
            def funkcja(x, y):
                return operator.or_(x, y)
        else:
            # code.interact(local=locals())
            raise Exception
        q_list = [Q(document_title__icontains=document_title),
                  Q(author__icontains=author),
                  Q(filename__icontains=filename),
                  Q(name__icontains=name),
                  Q(description__icontains=description)]
                  # Q(language__id=language)]
        q_list_no_empty = [
            q_el for q_el in q_list if q_el.children[0][1] != '']
        # code.interact(local=locals())
        q_reduce = reduce(funkcja,
                          q_list_no_empty) if q_list_no_empty else Q()
        mod_queryset = queryset.filter(
            q_reduce).order_by('name')
        mykwargs = {"owner": request.user}
        serializer = ExerciseDocumentRecipeSerializer(
            self.get_range(request, mod_queryset),
            many=True,
            **mykwargs)
        serializer_wrapper = DocumentContainerSerializer(
            {
             'total': len(mod_queryset),
             'documents': serializer.data})
        # code.interact(local=locals())
        # code.interact(local=locals())
        response = Response(serializer_wrapper.data)
        return response

    def get_range(self, request, queryset):
        page_number = int(request.GET.get('page', 1))
        page_size = int(request.GET.get('pagesize',
                                        len(queryset)))
        first = (page_number-1)*page_size
        last = page_number*page_size
        return queryset[first:last]

def luki_function(request_data, request_user, queryset, document_pk):
    mykwargs = {"owner": request_user}
    recipes = list(queryset.exercise_recipes.all())
    if len(recipes) is not len(request_data):
        return "Nie podano wszystkich szablonow dla podanego dokumentu.", status.HTTP_400_BAD_REQUEST
    for pos_num, rr in enumerate(request_data, 1):
        rr['position_number'] =  pos_num
        rr['document'] =  document_pk
    serializers = [ExerciseRecipeSerializer(
        get_recipe_from_queryset(recipes,
                                 recipe['id']),
        data=recipe,
        **mykwargs) for pos_num,recipe in enumerate(request_data, 1)]
    is_valids = [serializer.is_valid() for serializer in serializers]
    if all(is_valids):
        return True, serializers
        # for serializer in serializers:
        #     serializer.save()
        # datas = [serializer.data for serializer in serializers]
        # return datas, status.HTTP_200_OK
    return False, serializers
    # errors = [serializer.errors for serializer in serializers]
    # return errors, status.HTTP_400_BAD_REQUEST

def get_recipe_from_queryset(recipes, id):
    result = list(filter(lambda di: di.id == id, recipes ))
    if result:
        return result[0]
    else:
        raise

def get_preambule_content():
    queryset = Preambule.objects.all()
    preambule_obj = get_object_or_404(queryset, pk=1)
    return preambule_obj.content
