from django.db import models
from tex_resources.models.exercise import Exercise
from django.contrib.auth.models import User

# Create your models here.


class ExerciseDocumentRecipe(models.Model):
    owner = models.ForeignKey('auth.User',
                              on_delete=models.CASCADE, default=None)
    document_title = models.CharField(max_length=300, blank=True)
    author = models.CharField(max_length=150, blank=True)
    date = models.CharField(max_length=150, blank=True)
    is_title_part = models.BooleanField()
    filename = models.CharField(max_length=150)
    name = models.CharField(max_length=300)
    description = models.CharField(max_length=500, blank=True)


class ExerciseRecipe(models.Model):
    """
    ExerciseDocumentSettings
    """
    owner = models.ForeignKey('auth.User',
                              on_delete=models.CASCADE, default=None)
    position_number = models.PositiveIntegerField(blank=True)
    is_title = models.BooleanField()
    is_intro = models.BooleanField()
    is_hint = models.BooleanField()
    is_solution = models.BooleanField()
    document = models.ForeignKey('ExerciseDocumentRecipe',
                                 related_name='exercise_recipes',
                                 on_delete=models.CASCADE, default=None)
    exercise = models.ForeignKey('tex_resources.Exercise',
                                 on_delete=models.CASCADE, default=None)

    # class Meta:
    #     db_table = 'exerciserecipe'
