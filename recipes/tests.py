from django.test import TestCase

# Create your tests here.
from recipes.latexlib import LatexExercise, LatexFile, LatexAttachment
import unittest
import subprocess
import code
import shutil
import os
from django.conf import settings
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate
from recipes.views import views
from django.contrib.auth.models import User
from rest_framework import status

from recipes.models.models import ExerciseDocumentRecipe
from recipes.models.models import ExerciseRecipe
from recipes.serializers import DocumentContainerSerializer
from recipes.serializers import DocumentPDFContainerSerializer

from recipes.serializers import ExerciseDocumentRecipeSerializer
import base64

from tex_resources.models.preambule import Preambule
from django.shortcuts import get_object_or_404

class LatexLibTestCase(TestCase):
    fixtures = ["sample_preambules.json"]
    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        self.dirToRemove = None

    def tearDown(self):
        print("E---------end test----------")
        # shutil.rmtree(self.dirToRemove)

    def test_latexlib_generate(self):
        latex_file = LatexFile(
            document_title="Tytul dokumentu",
            preambule=get_preambule_content())
        self.pre_test_latexlib(latex_file)
        latex_file.generate(debug_print=False)

    def test_latexlib_zip(self):
        latex_file = LatexFile(
            document_title="Tytul dokumentu",
            preambule=get_preambule_content())
        self.pre_test_latexlib(latex_file)
        latex_file.zip()

    def pre_test_latexlib(self, latex_file):
        # code.interact(local=locals())
        path_to_image = os.path.join(os.getcwd(), 'obrazki/agh.jpg')
        exercise = LatexExercise(
            title="Jakiś tytuł", intro="Jakieś intro",
            content="Jakiś content", hint="Jakiś hint",
            solution="Jakieś solution \\ref{sthLabel3}",
            is_hint=True, is_title=True, is_intro=True, is_solution=True)
        exercise2 = LatexExercise(
            title="Jakiś tytuł2", intro="Jakieś intro2",
            content="Jakiś content2", hint="Jakiś hint2",
            solution="Jakieś solution2",
            is_hint=True, is_title=True, is_intro=True, is_solution=True)
        latex_attachment_intro_1 = LatexAttachment(
            width=0.1,
            image_path=path_to_image,
            caption='sthCaption',
            label='sthLabel1')
        latex_attachment_hint_2 = LatexAttachment(
            width=0.1,
            image_path=path_to_image,
            caption='sthCaption',
            label='sthLabel2')
        latex_attachment_solution_1 = LatexAttachment(
            width=0.1,
            image_path=path_to_image,
            caption='sthCaption',
            label='sthLabel3')
        latex_attachment_solution_2 = LatexAttachment(
            width=0.1,
            image_path=path_to_image,
            caption='sthCaption',
            label='sthLabel4')
        # latex_file = LatexFile(
        #     document_title="Tytul dokumentu")

        exercise.add_attachment_obj("intro", latex_attachment_intro_1)
        exercise2.add_attachment_obj("hint", latex_attachment_hint_2)
        exercise.add_attachment_obj("solution", latex_attachment_solution_1)
        exercise2.add_attachment_obj("solution", latex_attachment_solution_2)
        latex_file.add_exercise_obj(exercise)
        latex_file.add_exercise_obj(exercise2)
        self.dirToRemove = latex_file.temp_dir
        # shutil.rmtree(latex_file.temp_dir)
        # code.interact(local=locals())

    def test_latexlib_fail_document_title(self):
        exercise = LatexExercise(
            title="Jakiś tytuł", intro="Jakieś intro",
            content="Jakiś content", hint="Jakiś hint",
            solution="Jakieś solution",
            is_hint=True, is_title=True, is_intro=True, is_solution=True)
        exercise2 = LatexExercise(
            title="Jakiś tytuł2", intro="Jakieś intro2",
            content="Jakiś content2", hint="Jakiś hint2",
            solution="Jakieś solution2",
            is_hint=True, is_title=True, is_intro=True, is_solution=True)
        # print(exercise.generate_latex_code())
        latex_file = LatexFile(
            document_title="Tytul_dokumentu",
            preambule=get_preambule_content())
        latex_file.add_exercise_obj(exercise)
        latex_file.add_exercise_obj(exercise2)
        # code.interact(local=locals())
        self.dirToRemove = latex_file.temp_dir
        with self.assertRaises(subprocess.CalledProcessError):
            latex_file.generate()
        # shutil.rmtree(latex_file.temp_dir)


class RecipeViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))

    def tearDown(self):
        print("E---------end test----------")

    def test_get_exercise_recipes(self):
        factory = APIRequestFactory()
        view = views.ExerciseRecipeView.as_view({'get': 'list'})
        request = factory.get('documents/1/recipes/')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        ex_doc_recipe_num_before = ExerciseRecipe.objects.count()
        response = view(request, document_pk="2")
        ex_doc_recipe_num_after = ExerciseRecipe.objects.count()
        self.assertEqual(ex_doc_recipe_num_after,
                         ex_doc_recipe_num_before)
        # code.interact(local=locals())
        self.assertEqual(
            response.data[2]['exercise']['solution'],
            None)
        self.assertEqual(
            isinstance(response.data[1]['exercise']['solution'], str),
            True)
        self.assertEqual(len(response.data), 3)

    def test_get_exercise_recipe(self):
        factory = APIRequestFactory()
        view = views.ExerciseRecipeView.as_view({'get': 'retrieve'})
        request = factory.get('documents/2/recipes/')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        ex_doc_recipe_num_before = ExerciseRecipe.objects.count()
        response = view(request, document_pk="2", pk="4")
        ex_doc_recipe_num_after = ExerciseRecipe.objects.count()
        self.assertEqual(ex_doc_recipe_num_after,
                         ex_doc_recipe_num_before)
        # code.interact(local=locals())
        self.assertEqual(
            response.data['exercise']['solution'],
            None)
        self.assertEqual(
            isinstance(response.data['exercise']['hint'], str),
            True)
        # code.interact(local=locals())
        self.assertEqual(len(response.data), 7)

    def test_create_exercise_recipe(self):
        data = [{
                "is_title": False,
                "is_intro": False,
                "is_hint": False,
                "is_solution": False,
                "exercise": 6
        }]
        factory = APIRequestFactory()
        request = factory.post("document/2/recipes/", data,
                               format='json')
        view = views.ExerciseRecipeView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()
        ex_rec_num_before_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()

        self.assertEqual(ex_rec_num_before_document_pk,
                         3)
        response = view(request, document_pk="2")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        ex_rec_num_after_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before+1)
        self.assertEqual(ex_rec_num_after_document_pk,
                         ex_rec_num_before_document_pk+1)
        self.assertEqual(response.data[0]['exercise']['id'],
                         data[0]['exercise'])
        self.assertEqual(response.data[0]['position_number'],
                         4)

    def test_create_exercise_recipe_set_position_number_10(self):
        self.pre_test_create_exercise_recipe_set_position_number(10, 4)

    def test_create_exercise_recipe_set_position_number_4(self):
        self.pre_test_create_exercise_recipe_set_position_number(4, 4)

    def test_create_exercise_recipe_set_position_number_2(self):
        self.pre_test_create_exercise_recipe_set_position_number(2, 2)

    def test_create_exercise_recipe_set_position_number_1(self):
        self.pre_test_create_exercise_recipe_set_position_number(1, 1)

    def test_create_exercise_recipe_set_position_number_None(self):
        self.pre_test_create_exercise_recipe_set_position_number(1234, 4)

    def pre_test_create_exercise_recipe_set_position_number(
            self,
            start_position_number,
            end_position_number):
        data = [{
                "is_title": False,
                "is_intro": False,
                "is_hint": False,
                "is_solution": False,
                "exercise": 6,
                "position_number": start_position_number
        }]
        factory = APIRequestFactory()
        request = factory.post("document/2/recipes/", data,
                               format='json')
        view = views.ExerciseRecipeView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()
        ex_rec_num_before_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()

        self.assertEqual(ex_rec_num_before_document_pk,
                         3)
        response = view(request, document_pk="2")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        ex_rec_num_after_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before+1)
        self.assertEqual(ex_rec_num_after_document_pk,
                         ex_rec_num_before_document_pk+1)
        self.assertEqual(response.data[0]['exercise']['id'],
                         data[0]['exercise'])
        self.assertEqual(response.data[0]['position_number'],
                         end_position_number)
        our_list = list(ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.values('position_number').order_by('position_number'))
        self.assertEqual(our_list,
                         [{'position_number': 1},
                          {'position_number': 2},
                          {'position_number': 3},
                          {'position_number': 4}])

    def test_update_exercise_recipe_multiple(
            self):
        data = [{"id": 5,
                 # "position_number": 1,
                 "is_title": True,
                 "is_intro": True,
                 "is_hint": True,
                 "is_solution": True,
                 "exercise": "5"},
                {"id": 4,
                 "owner": "1",
                 # "position_number": 2,
                 "is_title": True,
                 "is_intro": True,
                 "is_hint": True,
                 "is_solution": True,
                 "exercise": "8"},
                {"id": 3,
                 "owner": "1",
                 # "position_number": 3,
                 "is_title": True,
                 "is_intro": True,
                 "is_hint": True,
                 "is_solution": True,
                 "exercise": "22"}]
        factory = APIRequestFactory()
        request = factory.put("document/2/recipes/", data,
                               format='json')
        view = views.ExerciseRecipeView.as_view({'put': 'update'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()
        ex_rec_num_before_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()

        self.assertEqual(ex_rec_num_before_document_pk,
                         3)
        response = view(request, document_pk="2")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        ex_rec_num_after_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before)
        self.assertEqual(ex_rec_num_after_document_pk,
                         ex_rec_num_before_document_pk)
        # code.interact(local=locals())
        # self.assertEqual(response.data['exercise']['id'],
        #                  data['exercise'])
        # self.assertEqual(response.data['position_number'],
        #                  3)
        our_list = list(ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.values('id','position_number').order_by('position_number'))
        self.assertEqual(our_list,
                         [{'id': 5, 'position_number': 1},
                          {'id': 4, 'position_number': 2},
                          {'id': 3, 'position_number': 3}])

    def test_delete_exercise_recipes(self):
        factory = APIRequestFactory()
        # code.interact(local=locals())
        request = factory.delete("document/2/recipes/",
                               format='json')
        view = views.ExerciseRecipeView.as_view({'delete': 'destroy'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()
        ex_rec_num_before_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()

        response = view(request, document_pk="2")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        ex_rec_num_after_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before-3)
        self.assertEqual(ex_rec_num_after_document_pk,
                         ex_rec_num_before_document_pk-3)

    def test_delete_exercise_recipe(self):
        factory = APIRequestFactory()
        request = factory.delete("document/2/recipes/3",
                               format='json')
        view = views.ExerciseRecipeView.as_view({'delete': 'destroy'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()
        ex_rec_num_before_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()

        response = view(request, document_pk="2", pk="3")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        ex_rec_num_after_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before-1)
        self.assertEqual(ex_rec_num_after_document_pk,
                         ex_rec_num_before_document_pk-1)

    def test_update_exercise_recipe(self):
        data = {
                "is_title": False,
                "is_intro": False,
                "is_hint": False,
                "is_solution": False,
                "exercise": 6
        }
        factory = APIRequestFactory()
        request = factory.put("document/2/recipes/", data,
                              format='json')
        view = views.ExerciseRecipeView.as_view({'put': 'update'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()
        ex_rec_num_before_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        self.assertEqual(ExerciseRecipe.objects.get(pk=3).position_number,
                         2)
        response = view(request, document_pk="2", pk="3")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        ex_rec_num_after_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before)
        self.assertEqual(ex_rec_num_after_document_pk,
                         ex_rec_num_before_document_pk)
        self.assertEqual(response.data['exercise']['id'],
                         data['exercise'])
        self.assertEqual(ExerciseRecipe.objects.get(pk=3).exercise.id,
                         data['exercise'])
        self.assertEqual(response.data['position_number'],
                         2)

    def test_update_exercise_recipe_set_position_number_1(self):
        self.pre_test_update_exercise_recipe(1, 1)

    def test_update_exercise_recipe_set_position_number_2(self):
        self.pre_test_update_exercise_recipe(2, 2)

    def test_update_exercise_recipe_set_position_number_3(self):
        self.pre_test_update_exercise_recipe(3, 3)

    def test_update_exercise_recipe_set_position_number_4(self):
        self.pre_test_update_exercise_recipe(4, 3)

    def pre_test_update_exercise_recipe(
            self,
            start_position_number,
            end_position_number):
        data = {
                "is_title": False,
                "is_intro": False,
                "is_hint": False,
                "is_solution": False,
                "exercise": 6,
                "position_number": start_position_number
        }
        factory = APIRequestFactory()
        request = factory.put("document/2/recipes/", data,
                              format='json')
        view = views.ExerciseRecipeView.as_view({'put': 'update'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()
        ex_rec_num_before_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        self.assertEqual(ExerciseRecipe.objects.get(pk=3).position_number,
                         2)
        response = view(request, document_pk="2", pk="3")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        ex_rec_num_after_document_pk = ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before)
        self.assertEqual(ex_rec_num_after_document_pk,
                         ex_rec_num_before_document_pk)
        self.assertEqual(response.data['exercise']['id'],
                         data['exercise'])
        self.assertEqual(ExerciseRecipe.objects.get(pk=3).exercise.id,
                         data['exercise'])
        self.assertEqual(response.data['position_number'],
                         end_position_number)
        our_list = list(ExerciseDocumentRecipe.objects.get(
            pk=2).exercise_recipes.values('position_number').order_by('position_number'))
        self.assertEqual(our_list,
                         [{'position_number': 1},
                          {'position_number': 2},
                          {'position_number': 3}])


class DocumentViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))

    def tearDown(self):
        print("E---------end test----------")

    def test_get_document_recipes(self):
        factory = APIRequestFactory()
        view = views.DocumentGeneratorView.as_view({'get': 'list'})
        request = factory.get('documents')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        ex_doc_recipe_num_before = ExerciseDocumentRecipe.objects.count()
        response = view(request)
        ex_doc_recipe_num_after = ExerciseDocumentRecipe.objects.count()
        self.assertEqual(ex_doc_recipe_num_after,
                         ex_doc_recipe_num_before)
        # code.interact(local=locals())
        self.assertEqual(
            response.data[0]['exercise_recipes'][2]['exercise']['solution'],
            None)
        self.assertEqual(
            isinstance(response.data[0]['exercise_recipes'][1]['exercise']['solution'],str),
            True)
        self.assertEqual(len(response.data), 2)

    def test_get_document_recipee(self):
        factory = APIRequestFactory()
        view = views.DocumentGeneratorView.as_view({'get': 'retrieve'})
        request = factory.get('documents')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        ex_doc_recipe_num_before = ExerciseDocumentRecipe.objects.count()
        response = view(request, pk=2)
        ex_doc_recipe_num_after = ExerciseDocumentRecipe.objects.count()
        self.assertEqual(ex_doc_recipe_num_after,
                         ex_doc_recipe_num_before)
        # code.interact(local=locals())
        self.assertEqual(
            response.data['exercise_recipes'][2]['exercise']['solution'],
            None)
        self.assertEqual(
            isinstance(response.data['exercise_recipes'][1]['exercise']['solution'], str),
            True)

    def test_create_document_recipe(self):
        data = {
          "document_title": "Nowy dokument",
          "author": "Luki Skywalker",
          "date": "Informatyka Stosowana V ROK",
          "is_title_part": True,
          "filename": "nazwa_pliku",
          "name": "Drugi dokument dla uczniow",
          "description": "Opis dokumentu drugiego, ktorego też nie będzie w pdf",
          "exercise_recipes": [
            {
                "is_title": True,
                "is_intro": True,
                "is_hint": True,
                "is_solution": True,
                "exercise": "22",
                "position_number": 1
            },
            {
                "is_title": True,
                "is_intro": True,
                "is_hint": True,
                "is_solution": True,
                "exercise": "7",
                "position_number": 2
            }
          ]
        }
        factory = APIRequestFactory()
        request = factory.post("/document/", data,
                               format='json')
        view = views.DocumentGeneratorView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()

        response = view(request)
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exercise_num_after,
                         exercise_num_before+1)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before+2)

    def test_create_document_recipe_without_position_numbers(self):
        data = {
          "document_title": "Nowy dokument",
          "author": "Luki Skywalker",
          "date": "Informatyka Stosowana V ROK",
          "is_title_part": True,
          "filename": "nazwa_pliku",
          "name": "Drugi dokument dla uczniow",
          "description": "Opis dokumentu drugiego, ktorego też nie będzie w pdf",
          "exercise_recipes": [
            {
                "is_title": True,
                "is_intro": True,
                "is_hint": True,
                "is_solution": True,
                "exercise": "22"
            },
            {
                "is_title": True,
                "is_intro": True,
                "is_hint": True,
                "is_solution": True,
                "exercise": "7"
            }
          ]
        }
        factory = APIRequestFactory()
        request = factory.post("/document/", data,
                               format='json')
        view = views.DocumentGeneratorView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()

        response = view(request)
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()
        # code.interact(local=locals())
        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exercise_num_after,
                         exercise_num_before+1)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before+2)
        self.assertEqual(1,
                         response.data['exercise_recipes'][0]['position_number'])
        self.assertEqual(2,
                         response.data['exercise_recipes'][1]['position_number'])

    def test_create_document_recipe_bad_position_numbers(self):
        data = {
          "document_title": "Nowy dokument",
          "author": "Luki Skywalker",
          "date": "Informatyka Stosowana V ROK",
          "is_title_part": True,
          "filename": "nazwa_pliku",
          "name": "Drugi dokument dla uczniow",
          "description": "Opis dokumentu drugiego, ktorego też nie będzie w pdf",
          "exercise_recipes": [
            {
                "is_title": True,
                "is_intro": True,
                "is_hint": True,
                "is_solution": True,
                "exercise": "22",
                "position_number": 1
            },
            {
                "is_title": True,
                "is_intro": True,
                "is_hint": True,
                "is_solution": True,
                "exercise": "7",
                "position_number": 5
            }
          ]
        }
        factory = APIRequestFactory()
        request = factory.post("/document/", data,
                               format='json')
        view = views.DocumentGeneratorView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)

        response = view(request)
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual('Numery pozycji niepoprawne. Podaj '
                         'kolejne liczby naturalne.',
                         str(response.data["exercise_recipes"][0]))

    def test_create_document_recipe_bad_position_numbers_the_same(self):
        data = {
          "document_title": "Nowy dokument",
          "author": "Luki Skywalker",
          "date": "Informatyka Stosowana V ROK",
          "is_title_part": True,
          "filename": "nazwa_pliku",
          "name": "Drugi dokument dla uczniow",
          "description": "Opis dokumentu drugiego, ktorego też nie będzie w pdf",
          "exercise_recipes": [
            {
                "is_title": True,
                "is_intro": True,
                "is_hint": True,
                "is_solution": True,
                "exercise": "22",
                "position_number": 7
            },
            {
                "is_title": True,
                "is_intro": True,
                "is_hint": True,
                "is_solution": True,
                "exercise": "7",
                "position_number": 7
            }
          ]
        }
        factory = APIRequestFactory()
        request = factory.post("/document/", data,
                               format='json')
        view = views.DocumentGeneratorView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)

        response = view(request)
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual('Numery pozycji nie sa unikalne',
                         str(response.data["exercise_recipes"][0]))



    def test_destroy_document_recipe(self):
        factory = APIRequestFactory()
        request = factory.delete("document/",
                                 format='json')
        view = views.DocumentGeneratorView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        document_recipe_num_before = len(
            ExerciseDocumentRecipe.objects.all())
        ex_recipe_num_before = len(
            ExerciseRecipe.objects.all())
        response = view(request, pk="1")
        document_recipe_num_after = len(
            ExerciseDocumentRecipe.objects.all())
        ex_recipe_num_after = len(
            ExerciseRecipe.objects.all())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(document_recipe_num_after,
                         document_recipe_num_before-1)
        self.assertEqual(ex_recipe_num_after,
                         ex_recipe_num_before-2)

    def test_destroy_document_recipes(self):
        factory = APIRequestFactory()
        request = factory.delete("document/",
                                 format='json')
        view = views.DocumentGeneratorView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        document_recipe_num_before = len(
            ExerciseDocumentRecipe.objects.all())
        ex_recipe_num_before = len(
            ExerciseRecipe.objects.all())
        response = view(request)
        document_recipe_num_after = len(
            ExerciseDocumentRecipe.objects.all())
        ex_recipe_num_after = len(
            ExerciseRecipe.objects.all())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(document_recipe_num_after,
                         document_recipe_num_before-2)
        self.assertEqual(ex_recipe_num_after,
                         ex_recipe_num_before-5)

    def test_update_document_recipe(self):
        # exercise_recipe nie zostana updatowane tutaj ?
        data = {
          "document_title": "Update dokument",
          "is_title_part": True,

          "author": "Tim Barton",
          "filename": "update_nazwa_pliku",
          "name": "Drugi dokument dla uczniow",
          "description": "Opis dokumentu drugiego, ktorego też nie będzie w pdf",
          "exercise_recipes": [
            {
                "id": 1,
                "is_title": False,
                "is_intro": False,
                "is_hint": True,
                "is_solution": True,
                "exercise": "22",
                "position_number": 1
            },
            {
                "id": 2,
                "is_title": True,
                "is_intro": False,
                "is_hint": True,
                "is_solution": True,
                "exercise": "7",
                "position_number": 2
            }
          ]
        }
        factory = APIRequestFactory()
        request = factory.put("/document/", data,
                               format='json')
        view = views.DocumentGeneratorView.as_view({'put': 'update'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()

        response = view(request, pk="1")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()

        # print(response.data)
        # code.interact(local=locals())

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before)

        self.assertEqual(response.data['author'],
                         data['author'])
        # code.interact(local=locals())
        self.assertEqual(response.data['exercise_recipes'][0]['id'],
                         data['exercise_recipes'][0]['id'])
        self.assertEqual(response.data['exercise_recipes'][0]['is_title'],
                         data['exercise_recipes'][0]['is_title'])
        self.assertEqual(response.data['exercise_recipes'][0]['is_intro'],
                         data['exercise_recipes'][0]['is_intro'])

    def test_update_document_recipe_without_position_nums(self):
        # exercise_recipe nie zostana updatowane tutaj ?
        data = {
          "document_title": "Update dokument",
          "is_title_part": True,
          "author": "Tim Barton",
          "filename": "update_nazwa_pliku",
          "name": "Drugi dokument dla uczniow",
          "description": "Opis dokumentu drugiego, ktorego też nie będzie w pdf",
          "exercise_recipes": [
            {
                "id": 2,
                "is_title": False,
                "is_intro": False,
                "is_hint": True,
                "is_solution": True,
                "exercise": "22",
            },
            {
                "id": 1,
                "is_title": True,
                "is_intro": False,
                "is_hint": True,
                "is_solution": True,
                "exercise": "7",
            }
          ]
        }
        factory = APIRequestFactory()
        request = factory.put("/document/", data,
                               format='json')
        view = views.DocumentGeneratorView.as_view({'put': 'update'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_before = ExerciseRecipe.objects.count()

        response = view(request, pk="1")
        exercise_num_after = ExerciseDocumentRecipe.objects.count()
        ex_rec_num_after = ExerciseRecipe.objects.count()

        # print(response.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(ex_rec_num_after,
                         ex_rec_num_before)

        self.assertEqual(response.data['author'],
                         data['author'])
        # code.interact(local=locals())
        self.assertEqual(response.data['exercise_recipes'][0]['id'],
                         data['exercise_recipes'][0]['id'])
        self.assertEqual(response.data['exercise_recipes'][0]['is_title'],
                         data['exercise_recipes'][0]['is_title'])
        self.assertEqual(response.data['exercise_recipes'][0]['is_intro'],
                         data['exercise_recipes'][0]['is_intro'])

        self.assertEqual(1,
                         response.data['exercise_recipes'][0]['position_number'])
        self.assertEqual(2,
                         response.data['exercise_recipes'][0]['id'])
        self.assertEqual(2,
                         response.data['exercise_recipes'][1]['position_number'])
        self.assertEqual(1,
                         response.data['exercise_recipes'][1]['id'])


    def test_find_document_bu_name(self):
        factory = APIRequestFactory()
        view = views.DocumentGeneratorView.as_view({'get': 'find'})
        request = factory.get('/document/find/?documenttitle=solution')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = ExerciseDocumentRecipe.objects.filter(
            document_title__icontains='solution').order_by("name")
        mykwargs = {"owner": request.user}
        serializer = ExerciseDocumentRecipeSerializer(
             languages, many=True, **mykwargs)
        self.assertEqual(response.data['documents'], serializer.data)
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_document_bu_name_2(self):
        factory = APIRequestFactory()
        view = views.DocumentGeneratorView.as_view({'get': 'find'})
        request = factory.get('/document/find/?author=artocha')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = ExerciseDocumentRecipe.objects.filter(
            author__icontains='artocha').order_by("name")
        mykwargs = {"owner": request.user}
        serializer = ExerciseDocumentRecipeSerializer(
            languages, many=True, **mykwargs)
        # code.interact(local=locals())
        self.assertEqual(len(response.data['documents']), 2)
        self.assertEqual(response.data['documents'], serializer.data)
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)


class DocumentGeneratorViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))

    def tearDown(self):
        # shutil.rmtree(os.path.join(
        #     settings.BASE_DIR,
        #     "recipes/static/temp_files"))
        print("E---------end test----------")

    def test_example(self):
        factory = APIRequestFactory()
        view = views.DocumentGeneratorView.as_view(
            {'post': 'generate'})
        request = factory.post('/document/generate/',
                               format="json")
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request, pk=1)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        # code.interact(local=locals())
        self.assertEqual(isinstance(response.data["base64_string"].decode(), str), True)
        with open(os.path.join(settings.BASE_DIR,
                               "example.pdf"), "wb") as file:
            file.write(base64.b64decode(response.data["base64_string"]))

    def test_example_zip(self):
        factory = APIRequestFactory()
        view = views.DocumentGeneratorView.as_view(
            {'post': 'generatetex'})
        request = factory.post('/document/generatetex/',
                               format="json")
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request, pk=1)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        # code.interact(local=locals())
        self.assertEqual(isinstance(response.data["base64_string"].decode(), str), True)
        with open(os.path.join(settings.BASE_DIR,
                               "example.zip"), "wb") as file:
            file.write(base64.b64decode(response.data["base64_string"]))

    def test_fail_example(self):
        factory = APIRequestFactory()
        view = views.DocumentGeneratorView.as_view(
            {'post': 'generate'})
        request = factory.post('/document/2/generate/',
                               format="json")
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request, pk=2)
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        # code.interact(local=locals())
        self.assertEqual(len(response.data['latex_errorlog']) > 0,
                         True)

from tex_resources.models.preambule import Preambule
from django.shortcuts import get_object_or_404

def get_preambule_content():
    preambule_obj = Preambule.objects.get(pk=1)
    return preambule_obj.content
