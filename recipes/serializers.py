from rest_framework import serializers

from recipes.models.models import ExerciseDocumentRecipe
from recipes.models.models import ExerciseRecipe
from tex_resources.models.exercise import Exercise

from tex_resources.serializers.exerciseserializer import ExerciseSerializer
import code

from tex_resources.models.lib import filter_exercises

from recipes.latexlib import LatexExercise, LatexFile
from rest_framework import serializers

from django.db.models import Q


def sort_exervise_recipe_by_position_number(e):
    return (e['position_number'], e['id'])


class ExerciseRecipeSerializer(serializers.ModelSerializer):

    exercise = serializers.IntegerField()
    id = serializers.ReadOnlyField()

    def __init__(self, *args, **kwargs):
        # code.interact(local=locals())
        self.owner = kwargs.pop('owner', None)
        super(ExerciseRecipeSerializer, self).__init__(*args, **kwargs)

    def validate(self, data):
        """
        Check that the start is before the stop.
        """
        if self.partial:
            return data
        # code.interact(local=locals())
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        db_exercise = Exercise.objects.get(
            pk=data['exercise'])

        tex_exercise = LatexExercise(
            title=db_exercise.title,
            intro=db_exercise.intro,
            content=db_exercise.task_content,
            hint=db_exercise.hint,
            solution=db_exercise.solution,
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
            # is_hint=single_exercise["is_hint"],
            # is_intro=single_exercise["is_intro"],
            # is_solution=single_exercise["is_solution"])
        tex_file.add_exercise_obj(tex_exercise)
        # code.interact(local=locals())
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany szablon uniemożliwia wygenerowanie dokumentu PDF. Sprawdź, czy podpięte zadanie zawiera poprawne dane.")

    def to_representation(self, obj):
        # code.interact(local=locals())
        mod_queryset = [
            exer for exer in filter_exercises(self.owner,
                                              [obj.exercise])]
        # code.interact(local=locals())
        ex_serializer = ExerciseSerializer(mod_queryset[0])
        # code.interact(local=locals())
        return {
            "id": obj.id,
            "position_number": obj.position_number,
            "is_title": obj.is_title,
            "is_intro": obj.is_intro,
            "is_hint": obj.is_hint,
            "is_solution": obj.is_solution,
            "exercise": ex_serializer.data
        }
        return super(ExerciseRecipeSerializer,
                     self).to_representation(obj)

    def create(self, validated_data):
        # code.interact(local=locals())

        ex_recipes_count = validated_data["document"].exercise_recipes.count()
        for id, item in enumerate(validated_data["document"].exercise_recipes.order_by('position_number')):
            item.position_number = id + 1
            item.save()
        ex_recipes_q = validated_data["document"].exercise_recipes.order_by(
            'position_number')

        item_list = list(ex_recipes_q.values('id', 'position_number').order_by('position_number'))

        if validated_data.get("position_number", None) is None:
            validated_data["position_number"] = ex_recipes_count + 1
        else:
            item_list.append({"id": -1,
                              "position_number":
                              validated_data["position_number"]})
            # code.interact(local=locals())
            item_list.sort(key=sort_exervise_recipe_by_position_number)
            # code.interact(local=locals())
            is_set = False
            for id, pk_pos_num in enumerate(item_list):
                if not is_set and validated_data["position_number"] == pk_pos_num["position_number"]:
                    is_set = True
                    validated_data["position_number"] = id+1
                else:
                    ex_recipe = ex_recipes_q.get(id=pk_pos_num["id"])
                    ex_recipe.position_number = id + 1
                    ex_recipe.save()

        validated_data["owner"] = self.owner
        validated_data["exercise"] = Exercise.objects.get(
            pk=validated_data["exercise"])
        recipe = ExerciseRecipe.objects.create(**validated_data)
        return recipe

    def update(self, instance, validated_data):
        ex_recipes_q = instance.document.exercise_recipes.filter(
            ~Q(id=instance.id)).order_by('position_number')
        item_list = list(ex_recipes_q.values('id', 'position_number').order_by('position_number'))
        if validated_data.get("position_number", None) is None:
            pass
        else:
            for i in range(len(item_list)):
                item_list[i]["position_number"] = i+1
            item_list.append({"id": -1,
                              "position_number":
                              validated_data["position_number"]})
            # code.interact(local=locals())
            item_list.sort(key=sort_exervise_recipe_by_position_number)
            # code.interact(local=locals())
            is_set = False
            for id, pk_pos_num in enumerate(item_list):
                if not is_set and validated_data["position_number"] == pk_pos_num["position_number"]:
                    is_set = True
                    validated_data["position_number"] = id+1
                else:
                    ex_recipe = ex_recipes_q.get(id=pk_pos_num["id"])
                    ex_recipe.position_number = id + 1
                    ex_recipe.save()
            instance.position_number = validated_data["position_number"]

        instance.is_title = validated_data.get('is_title',
                                               instance.is_title)
        instance.is_intro = validated_data.get('is_intro',
                                               instance.is_intro)
        instance.is_hint = validated_data.get('is_hint',
                                              instance.is_hint)
        instance.is_solution = validated_data.get(
            'is_solution', instance.is_solution)
        instance.exercise = Exercise.objects.get(
            pk=validated_data.get('exercise',
                                  instance.exercise.id))
        instance.save()
        return instance

    class Meta:
        model = ExerciseRecipe
        fields = ('id', 'is_title', 'is_intro', 'is_hint', 'is_solution',
                  'exercise', 'document', 'position_number')


class ExerciseDocumentRecipeSerializer(serializers.ModelSerializer):
    exercise_recipes = ExerciseRecipeSerializer(many=True)
    # exercise_recipes = serializers.ListField(child=serializers.IntegerField())
    # serializers.PrimaryKeyRelatedField

    def __init__(self, *args, **kwargs):
        self.owner = kwargs.pop('owner', None)
        super(ExerciseDocumentRecipeSerializer, self).__init__(*args, **kwargs)

    def validate_document_title(self, data):
        """
        Check that the start is before the stop.
        """
        tex_file = LatexFile(
            document_title=data,
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # for single_exercise in data["exercise_recipes"]:
        #     db_exercise = Exercise.objects.get(
        #         pk=single_exercise['exercise'])
        #     # code.interact(local=locals())
        #     tex_exercise = LatexExercise(
        #         title=db_exercise.title,
        #         intro=db_exercise.intro,
        #         content=db_exercise.task_content,
        #         hint=db_exercise.hint,
        #         solution=db_exercise.solution,
        #         is_hint=True,
        #         is_intro=True,
        #         is_solution=True)
        #         # is_hint=single_exercise["is_hint"],
        #         # is_intro=single_exercise["is_intro"],
        #         # is_solution=single_exercise["is_solution"])
        #     tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany tekst uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_author(self, data):
        """
        Check that the start is before the stop.
        """
        tex_file = LatexFile(
            document_title="XXX",
            author=data,
            date="XXX",
            preambule=get_preambule_content())
        # for single_exercise in data["exercise_recipes"]:
        #     db_exercise = Exercise.objects.get(
        #         pk=single_exercise['exercise'])
        #     # code.interact(local=locals())
        #     tex_exercise = LatexExercise(
        #         title=db_exercise.title,
        #         intro=db_exercise.intro,
        #         content=db_exercise.task_content,
        #         hint=db_exercise.hint,
        #         solution=db_exercise.solution,
        #         is_hint=True,
        #         is_intro=True,
        #         is_solution=True)
        #         # is_hint=single_exercise["is_hint"],
        #         # is_intro=single_exercise["is_intro"],
        #         # is_solution=single_exercise["is_solution"])
        #     tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany tekst uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_date(self, data):
        """
        Check that the start is before the stop.
        """
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date=data,
            preambule=get_preambule_content())
        # for single_exercise in data["exercise_recipes"]:
        #     db_exercise = Exercise.objects.get(
        #         pk=single_exercise['exercise'])
        #     # code.interact(local=locals())
        #     tex_exercise = LatexExercise(
        #         title=db_exercise.title,
        #         intro=db_exercise.intro,
        #         content=db_exercise.task_content,
        #         hint=db_exercise.hint,
        #         solution=db_exercise.solution,
        #         is_hint=True,
        #         is_intro=True,
        #         is_solution=True)
        #         # is_hint=single_exercise["is_hint"],
        #         # is_intro=single_exercise["is_intro"],
        #         # is_solution=single_exercise["is_solution"])
        #     tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError(
            "Podany tekst uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_exercise_recipes(self, data):
        item_list = [ex_recipe["position_number"] for ex_recipe in data if "position_number" in ex_recipe]
        ex_recipes_size = len(data)
        length = len(item_list)
        if 0 < length < ex_recipes_size:
            raise serializers.ValidationError(
                "Część pozycji podana. Część nie podana."
                "Wymagana jedna konwencja.")
        elif length == 0:
            return data
        elif length == len(set(item_list)):
            s_n_value = sum(item_list)
            a_n = max(item_list)
            s_n_expected = (1+a_n)*a_n/2.
            if s_n_expected == s_n_value:
                return data
            else:
                raise serializers.ValidationError(
                    "Numery pozycji niepoprawne. "
                    "Podaj kolejne liczby naturalne.")

        else:
            raise serializers.ValidationError(
                "Numery pozycji nie sa unikalne")

    def to_representation(self, obj):
        mykwargs = {"owner": self.owner}
        # code.interact(local=locals())
        # queryset = [ExerciseRecipe.objects.get(pk=recipe_id) for recipe_id in obj.exercise_recipes]
        serializer = ExerciseRecipeSerializer(obj.exercise_recipes.order_by(
            'position_number'),
                                              many=True,
                                              **mykwargs)
        # return super(ExerciseDocumentRecipeSerializer,
        #              self).to_representation(obj)
        # code.interact(local=locals())
        return {
            "id": obj.id,
            "document_title": obj.document_title,
            "author": obj.author,
            "date": obj.date,
            "is_title_part": obj.is_title_part,
            "filename": obj.filename,
            "exercise_recipes": serializer.data,
            "name": obj.name,
            "description": obj.description
        }

    def create(self, validated_data):
        # code.interact(local=locals())
        exercise_recipes_data = validated_data.pop('exercise_recipes')
        validated_data["owner"] = self.owner
        document = ExerciseDocumentRecipe.objects.create(**validated_data)
        # code.interact(local=locals())
        for num, recipe_data in enumerate(exercise_recipes_data):
            # code.interact(local=locals())
            # recipe_data["owner"] = self.owner.id
            recipe_data["position_number"] = num+1
            recipe_data["exercise"] = Exercise.objects.get(
                pk=recipe_data["exercise"])
            ExerciseRecipe.objects.create(document=document, owner=self.owner,
                                          **recipe_data)
        return document

    def update(self, instance, validated_data):
        # code.interact(local=locals())
        instance.document_title = validated_data.get('document_title',
                                                     instance.document_title)
        instance.author = validated_data.get('author',
                                             instance.author)
        instance.date = validated_data.get('date',
                                           instance.date)
        instance.is_title_part = validated_data.get('is_title_part',
                                                    instance.is_title_part)
        instance.filename = validated_data.get(
            'filename', instance.filename)
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get('description',
                                                  instance.description)
        instance.save()
        return instance

    class Meta:
        model = ExerciseDocumentRecipe
        fields = ('id', 'document_title', 'author', 'filename', 'date', 'is_title_part',
                  'exercise_recipes', 'name', 'description')


class DocumentContainerSerializer(serializers.Serializer):
    total = serializers.IntegerField()
    documents = ExerciseDocumentRecipeSerializer(many=True)

    def to_representation(self, obj):
        return {
          "total": obj['total'],
          "documents": obj['documents']
        }


class DocumentPDFContainerSerializer(serializers.Serializer):
    base64_string = serializers.CharField()
    filename = serializers.CharField()

    def to_representation(self, obj):
        return {
          "base64_string": obj['base64_string'],
          "filename": obj['filename']
        }

from tex_resources.models.preambule import Preambule
from django.shortcuts import get_object_or_404

def get_preambule_content():
    queryset = Preambule.objects.all()
    preambule_obj = get_object_or_404(queryset, pk=1)
    return preambule_obj.content
