import os
import code
import uuid
import subprocess
import base64
import shutil
from zipfile import ZipFile
PDF_GEN_APP_DIR = os.path.dirname(os.path.dirname(
    os.path.abspath(__file__)))
# from recipes.latexlib import LatexAttachment

class LatexAttachment(object):
    def __init__(self, width=1, image_path=None,
                 caption=None, label=None):
        self.width = width
        self.image_path = image_path
        self.basename = os.path.basename(image_path)
        self.caption = caption
        self.label = label

    def generate_latex_code(self, use_abs_images_path=True):
        caption_part = ''
        if self.caption:
            caption_part = "\\caption{{{0}}}".format(self.caption)
        label_part = ''
        if self.label:
            label_part = "\\label{{{0}}}\n".format(self.label)
        if use_abs_images_path:
            image_name = self.image_path
        else:
            image_name = self.basename
        return "\\begin{{figure}}[ht]\n"\
               "\\centering\n"\
               "\\subfigure{{\\includegraphics[width={0}\\textwidth]{{{1}}}}}"\
               "{2}\n"\
               "{3}\n"\
               "\\end{{figure}}\n\n".format(self.width, image_name,
                                            caption_part, label_part)


class LatexExercise(object):
    def __init__(self, title=None, intro=None,
                 content=None, hint=None, solution=None,
                 is_title=False,
                 is_intro=False, is_hint=False,
                 is_solution=False):
        self.title = title
        self.intro = intro
        self.content = content
        self.hint = hint
        self.solution = solution
        self.is_title = is_title
        self.is_intro = is_intro
        self.is_hint = is_hint
        self.is_solution = is_solution
        self.attachments = {"intro": [],
                            "content": [],
                            "hint": [],
                            "solution": []}
        self.visible_attachments_abs_path = []

    def generate_latex_code(self, use_abs_images_path=True):
        self.visible_attachments_abs_path = []
        intro_part = ''
        if self.is_intro and self.intro not in (None, ''):
            intro_part = r'\begin{addmargin}[3em]{3em}% 1em left, 2em right''\n'\
                         r'\textit{'+self.intro+'}''\n'\
                         r'\end{addmargin}''\n'\
                         r'\noindent''\n'
            for intro_attachment in self.attachments["intro"]:
                intro_part += intro_attachment.generate_latex_code(use_abs_images_path)
                self.visible_attachments_abs_path.append(intro_attachment.image_path)
        hint_part = ''
        if self.is_hint and self.hint not in (None, ''):
            hint_part = r'\textbf{Wskazówka.} '+self.hint+'\n\n'\
                        r'\noindent''\n'
            for hint_attachment in self.attachments["hint"]:
                hint_part += hint_attachment.generate_latex_code(use_abs_images_path)
                self.visible_attachments_abs_path.append(hint_attachment.image_path)
        solution_part = ''
        # code.interact(local=locals())
        if self.is_solution and self.solution not in (None, ''):
            # code.interact(local=locals())
            solution_part = r'\textbf{Rozwiązanie.} '+self.solution+'\n\n'\
                            r'\noindent''\n'
            for solution_attachment in self.attachments["solution"]:
                solution_part += solution_attachment.generate_latex_code(use_abs_images_path)
                self.visible_attachments_abs_path.append(solution_attachment.image_path)
        title_part = ''
        if self.is_title and self.title not in (None, ''):
            title_part = self.title

        latex_code = r'\section{Zadanie. '+title_part+'}''\n'\
                     + intro_part +\
                     r'\textbf{Treść.} '+self.content+'\n\n'\
                     r'\noindent''\n'\
                     + hint_part\
                     + solution_part
        return latex_code

    def add_attachment(self, type, width=None, image_path=None,
                       caption=None, label=None):
        self.attachments[type].append(LatexAttachment(width, image_path,
                                                      caption, label))

    def add_attachment_obj(self, type, attachment):
        if not isinstance(attachment, LatexAttachment):
            raise
        self.attachments[type].append(attachment)


class LatexFile(object):
    def __init__(self, path=None, document_title=None,
                 author=None, date=None, preambule=None, is_title_part=True, filename="testDokument"):
        self.template_path = os.path.join(PDF_GEN_APP_DIR,
                                          "media/template.tex")
        self.temp_dir = os.path.join(
            PDF_GEN_APP_DIR, "media/temp_files",
            str(uuid.uuid4()))
        if not os.path.exists(self.temp_dir):
            os.makedirs(self.temp_dir)
        self.preambule = preambule
        self.document_title = document_title
        self.author = author
        self.date = date
        self.is_title_part = is_title_part
        self.exercises = []
        self.filename = filename
        self.data = {"base64_string": None}
        self.errors = None
        self.tex_file_path = None
        self.tex_file_name = "temp_to_edit.tex"

    def add_exercise(self, title=None, intro=None,
                     content=None, hint=None, solution=None):
        self.exercises.append(LatexExercise(
            title, intro, content, hint, solution))

    def add_exercise_obj(self, exercise):
        if not isinstance(exercise, LatexExercise):
            raise
        self.exercises.append(exercise)

    def is_valid(self, repeat_num=1):
        try:
            pdf_file = self.generate(repeat_num=repeat_num)
            with open(pdf_file, "rb") as file:
                self.data["base64_string"] = base64.b64encode(file.read())
            return True
        except subprocess.CalledProcessError as e:
            self.errors = e.stdout
            return False
        finally:
            shutil.rmtree(self.temp_dir)

    def create_empty_tex_file(self, debug_print=False):
        file_to_edit = os.path.join(
            self.temp_dir, self.tex_file_name)
        shutil.copyfile(self.template_path,
                        file_to_edit)
        with open(file_to_edit, "r+") as file:
            content = file.read()
            file.seek(0)
            file.truncate()
            file.write(content)
        self.tex_file_path = file_to_edit
        with open(self.tex_file_path , "rb") as file:
            self.data["base64_string_empty_file"] = base64.b64encode(file.read())

    def create_filled_tex_file(self, debug_print=False, use_abs_images_path=True):
        exs_latex_code = "".join([
            exercise.generate_latex_code(use_abs_images_path)
            for exercise in self.exercises])
        self.abs_path_image_vis_list = []
        for exercise in self.exercises:
            self.abs_path_image_vis_list.extend(exercise.visible_attachments_abs_path)
        # self.abs_path_image_vis_list = [
        #     exercise.visible_attachments_abs_path for exercise in self.exercises
        # ]
        PDF_GEN_APP_DIR = os.path.dirname(os.path.dirname(
            os.path.abspath(__file__)))
        file_to_edit = os.path.join(
            self.temp_dir, self.tex_file_name)
        shutil.copyfile(self.template_path,
                        file_to_edit)
        with open(file_to_edit, "r+") as file:
            content = file.read()
            # code.interact(local=locals())
            content = content.replace(r"%%%PREAMBULE%%%",
                                      self.preambule)
            content = content.replace(r"%%%EXERCISE%%%",
                                      exs_latex_code)
            content = content.replace(r"%%%TITLE%%%",
                                      self.document_title)

            date_part = ''
            if self.date is not None:
                date_part = self.date
            content = content.replace(r"%%%DATE%%%",
                                      date_part)

            author_part = ''
            if self.author is not None:
                author_part = self.author
            content = content.replace(r"%%%AUTHOR%%%",
                                      author_part)

            title_header_part = ''
            if self.is_title_part:
                title_header_part = r"\maketitle"
            content = content.replace(r"%%%MAKETITLE%%%",
                                      title_header_part)

            if debug_print:
                print(content)
            # code.interact(local=locals())
            file.seek(0)
            file.truncate()
            file.write(content)
        self.tex_file_path = file_to_edit
        return file_to_edit

    def generate(self, debug_print=False, repeat_num=1):
        # if self.tex_file_path is None:
        self.create_filled_tex_file(debug_print)
        for iteration in range(repeat_num):
            output = subprocess.run(
                ["pdflatex", "-halt-on-error",
                 "--jobname={0}".format(self.filename),
                 "-output-directory={0}".format(self.temp_dir),
                 self.tex_file_path],
                stdout=subprocess.PIPE, check=True)
            # code.interact(local=locals())
        self.file_path = os.path.join(self.temp_dir,
                            "{0}.pdf".format(self.filename))
        return self.file_path

    def zip(self):
        # if self.tex_file_path is None:
        self.create_filled_tex_file(use_abs_images_path=False)
        try:
            zip_file_path = os.path.join(
                self.temp_dir,
                "{0}.zip".format(self.filename))
            with ZipFile(zip_file_path, 'w') as myzip:
                myzip.write(self.tex_file_path, arcname=self.tex_file_name)
                # code.interact(local=locals())
                for abs_path in self.abs_path_image_vis_list:
                    myzip.write(abs_path, arcname=os.path.basename(abs_path))
            with open(zip_file_path, "rb") as file:
                self.data["zip_as_base64_string"] = base64.b64encode(file.read())
        except Exception as e:
            print(e)
            raise
        finally:
            shutil.rmtree(self.temp_dir)
