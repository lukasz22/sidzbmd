from django.db import models


class Category(models.Model):
    """
    Category model
    """
    category_name = models.CharField(max_length=150, unique=True)
    parent = models.ForeignKey('self',
                               blank=True,
                               on_delete=models.SET_NULL,
                               null=True,
                               related_name='children')

    class Meta:
        # unique_together = ('category_name', 'parent',)
        verbose_name_plural = 'categories'

    def __str__(self):
        full_path = [self.category_name]
        branch = self.parent
        while branch is not None:
            full_path.append(branch.category_name)
            branch = branch.parent
        return ' -> '.join(full_path[::-1])
