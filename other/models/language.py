from django.db import models


class Language(models.Model):
    """
    Language model
    """
    name = models.CharField(max_length=60)

    def __unicode__(self):
        return "{0}".format(self.name)
