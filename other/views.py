from django.shortcuts import get_object_or_404

from rest_framework.decorators import action
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status

from .models import Language
from .models import Category

from .serializers import LanguageSerializer
from .serializers import CategorySerializer

from .serializers import CategoryTreeSerializer
from .serializers import CategoryFlatTreeSerializer

from accesses.permission import IsAdministrator

import code


class LanguageViewSet(viewsets.ModelViewSet):
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer
    http_method_names = ['get']
    # permission_classes = []
    #  http http://127.0.0.1:8000/languages/


class CategoryView(viewsets.ViewSet):

    def list(self, request):
        # code.interact(local=locals())
        queryset = Category.objects.all()
        # .order_by('name')
        serializer = CategorySerializer(queryset, many=True)

        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        category = get_object_or_404(Category, pk=pk)
        serializer = CategorySerializer(category)
        return Response(serializer.data) # not list

    def create(self, request):
        if not IsAdministrator.has_permission(request):
            return Response("Nie posiadasz uprawnień do tworzenia kategorii. Skontaktuj się z administratorem.",
                            status=status.HTTP_403_FORBIDDEN)
        if not request.POST._mutable:
            request.POST._mutable = True
        serializer = CategorySerializer(
            data=request.data, context={'id': None, 'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk, format='json'):
        if not IsAdministrator.has_permission(request):
            return Response("Nie posiadasz uprawnień do aktualizowania kategorii. Skontaktuj się z administratorem.",
                            status=status.HTTP_403_FORBIDDEN)
        exercise = get_object_or_404(Category, pk=pk)
        # code.interact(local=locals())
        serializer = CategorySerializer(
            exercise,
            data=request.data, context={'id': pk, 'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_200_OK)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        if not IsAdministrator.has_permission(request):
            return Response("Nie posiadasz uprawnień do usuwania kategorii. Skontaktuj się z administratorem.",
                            status=status.HTTP_403_FORBIDDEN)
        if pk is not None:
            exercise = get_object_or_404(Category, pk=pk)
            ans = Category.objects.filter(pk=pk).delete()
            return Response(ans, status=status.HTTP_200_OK)
        else:
            ans = Category.objects.all().delete()
            return Response(ans, status=status.HTTP_200_OK)

    @action(methods=['get', 'delete'], detail=False)
    def treeall(self, request, pk=None):
        return self.tree(request, pk)

    @action(methods=['get', 'delete'], detail=True)
    def tree(self, request, pk=None):
        if pk is None:
            queryset = Category.objects.filter(
                parent=None).order_by('category_name')
        else:
            queryset = [get_object_or_404(Category, pk=pk)]
        if request.method == 'GET':
            serializer = CategoryTreeSerializer(queryset,
                                                many=True)
            # code.interact(local=locals())
            return Response(serializer.data,
                                status=status.HTTP_200_OK)
        elif not IsAdministrator.has_permission(request):
            return Response("Nie posiadasz uprawnień do usuwania kategorii. Skontaktuj się z administratorem.",
                            status=status.HTTP_403_FORBIDDEN)
        elif pk is None:
            ans = Category.objects.all().delete()
            return Response(ans, status=status.HTTP_200_OK)
        else:
            serializer = CategoryFlatTreeSerializer(queryset,
                                                    many=True)
            pk_list = [item for sublist in serializer.data
                            for item in sublist]
            ans = Category.objects.filter(pk__in=pk_list).delete()
            return Response(ans, status=status.HTTP_200_OK)
        # serializer = CategoryTreeSerializer(data=queryset)
        # if serializer.is_valid():
        #     return Response(serializer.data, status=status.HTTP_201_CREATED)
        # return Response(serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
