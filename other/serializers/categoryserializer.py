from rest_framework import serializers
from ..models import Category

import code
import json


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'category_name', 'parent')


    def validate_parent(self, data):
        """
        Check that the start is before the stop.
        """

        # code.interact(local=locals())
        if self.context['request'].method == 'POST':
            return data
        id = int(self.context['id'])
        branch = data
        while branch is not None:
            if branch.id == id:
                raise serializers.ValidationError("Wykryto pętlę w drzewie kategorii.")
            branch = branch.parent
        return data

        # code.interact(local=locals())
        return data

    def to_representation(self, obj):
        return {
            "id": obj.id,
            "category_name": obj.category_name,
            "parent": CategorySerializer(obj.parent).data,
            "full_name": str(obj)
        }


class CategoryTreeSerializer(serializers.BaseSerializer):
    def to_internal_value(self, data):
        raise NotImplementedError
        score = data.get('score')
        player_name = data.get('player_name')

        # Perform the data validation.
        if not score:
            raise serializers.ValidationError({
                'score': 'This field is required.'
            })
        if not player_name:
            raise serializers.ValidationError({
                'player_name': 'This field is required.'
            })
        if len(player_name) > 10:
            raise serializers.ValidationError({
                'player_name':
                    'May not be more than 10 characters.'
            })

        # Return the validated values.
        # This will be available as
        # the `.validated_data` property.
        return {
            'score': int(score),
            'player_name': player_name
        }

    def to_representation(self, obj):
        out = self.create_tree(obj, it=obj.id)
        return next(out)

    def create_tree(self, obj, it, result=[]):
        if it is obj.id:
            result = []
        result.append({})
        result[-1]['category_name'] = obj.category_name
        result[-1]['id'] = obj.id
        result[-1]['full_name'] = str(obj)
        children = obj.children.filter(
            parent=obj.id).order_by('category_name')
        if children:
            result[-1]['children'] = []
        for child in children:
            for out in self.create_tree(child, it, []):
                result[-1]['children'].extend(out)
        yield result


class CategoryFlatTreeSerializer(serializers.BaseSerializer):
    def to_representation(self, obj):
        out = self.create_tree(obj, it=obj.id)
        result = next(out)
        # code.interact(local=locals())
        result.remove(obj.id)
        return result

    def create_tree(self, obj, it, result=[]):
        if it is obj.id:
            result = []
        # result.append({})
        # result[-1]['category_name'] = obj.category_name
        # result[-1]['id'] = obj.id
        result.append(obj.id)
        # result[-1]['full_name'] = str(obj)
        children = obj.children.filter(
            parent=obj.id).order_by('category_name')
        # if children:
        #     result.append({})
        #     result[-1]['children'] = []
        for child in children:
            for out in self.create_tree(child, it, []):
                result = result + out
        yield result
