from django.test import TestCase #, Client
from django.contrib.auth.models import User

# from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate


from ..models import Language
from other import serializers

from other import views

import code

# https://jsonlint.com/
# python manage.py dumpdata > db_draft.json
# http://www.django-rest-framework.org/api-guide/testing/

class LanguageSerializerTestCase(TestCase):
    def setUp(self):
        pass

    def test_simply(self):
        val_dict = {'id': '1', 'name': 'fizyka'}
        serializer = serializers.LanguageSerializer(
            data=val_dict)
        try:
            self.assertEqual(serializer.is_valid(), True)
        except AssertionError as error:
            raise

    def test_long_name_field(self):
        name_field = ''.join(
            ['long_name' for i in range(61)])
        val_dict = {'id': '1', 'name': name_field}
        serializer = serializers.LanguageSerializer(
            data=val_dict)
        try:
            self.assertEqual(serializer.is_valid(), False)
            self.assertEqual(
                serializer.errors['name'][0].code,
                'max_length')
        except AssertionError as error:
            raise


class LanguageViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        lan1 = Language(name="angielski")
        lan1.save()
        lan2 = Language(name="polski")
        lan2.save()

    def test_get_languages(self):
        # client = Client()
        user = User.objects.get(username='JanProfesor')
        # response = client.get(reverse('language-list'))
        factory = APIRequestFactory()
        request = factory.get('/languages/')
        force_authenticate(request, user=user)
        view = views.LanguageViewSet.as_view({'get': 'list'})
        response = view(request)
        languages = Language.objects.all()
        serializer = serializers.LanguageSerializer(
            languages, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
