from django.test import TestCase #, Client
from django.contrib.auth.models import User
#from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate

from tex_resources.models.exercise import Exercise
from ..models import Category

from ..serializers import CategorySerializer
from ..serializers import CategoryTreeSerializer
from ..serializers import CategoryFlatTreeSerializer

from other import views

import json
import code

# https://jsonlint.com/
# python manage.py dumpdata > db_draft.json
# http://www.django-rest-framework.org/api-guide/testing/

class ManyCategoriesViewTestCase(TestCase):
    fixtures = ["many_exercises.json", "sample_preambules.json"]

# 1 Matematyka
# 9    Algebra
# 10    Matematyka Dyskretna
# 11    Geometria
# 2 Informatyka
# 3    Algorytmy
# 4    Programowanie
# 5        ProgramowanieCpp
# 6            ProgramowanieProc
# 7            ProgramowanieObiek
# 8        ProgramowanieJava

# [{
#     id:
#     category_name:
#     children: [
#         {
#             id:
#             category_name:
#             children: [
#                 {}
#             ]
#         },
#         {
#             id:
#             category_name:
#             children: [
#                 {}
#             ]
#         }
#     ]
# }]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))

    def tearDown(self):
        print("E---------end test----------")

    def test_list_tree_categories(self):
        factory = APIRequestFactory()
        request = factory.get('/categories/tree')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercise-list'))
        view = views.CategoryView.as_view({'get': 'tree'})
        languages = Category.objects.filter(parent=None)
        response = view(request)
        serializer = CategoryTreeSerializer(
            languages, many=True)
        # code.interact(local=locals())
        expected = [[{'full_name': 'Informatyka',
                      'category_name': 'Informatyka',
                      'children': [
                        {'full_name': 'Informatyka -> Algorytmy',
                         'category_name': 'Algorytmy', 'id': 3},
                        {'full_name': 'Informatyka -> Programowanie',
                         'category_name': 'Programowanie',
                         'children': [
                          {'full_name': 'Informatyka -> Programowanie -> Programowanie C++',
                           'category_name': 'Programowanie C++',
                            'children': [
                            {'full_name': 'Informatyka -> Programowanie -> Programowanie C++ -> Programowanie obiektowe',
                             'category_name': 'Programowanie obiektowe',
                             'id': 7},
                            {'full_name': 'Informatyka -> Programowanie -> Programowanie C++ -> Programowanie proceduralne',
                             'category_name': 'Programowanie proceduralne',
                             'id': 6}],
                           'id': 5},
                          {'full_name': 'Informatyka -> Programowanie -> Programowanie Java',
                           'category_name': 'Programowanie Java',
                           'id': 8}],
                         'id': 4}],
                    'id': 2}],
                    [{'full_name': 'Matematyka', 'category_name': 'Matematyka',
                      'children':
                        [{'full_name': 'Matematyka -> Algebra',
                          'category_name': 'Algebra',
                          'id': 9},
                         {'full_name': 'Matematyka -> Geometria',
                          'category_name': 'Geometria',
                          'id': 11},
                         {'full_name': 'Matematyka -> Matematyka Dyskretna',
                          'category_name': 'Matematyka Dyskretna',
                          'id': 10}],
                     'id': 1}]]
        print(json.dumps(response.data, indent=4))
        print("--------")
        print(json.dumps(serializer.data, indent=4))
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.data, expected)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_list_tree_subcategories(self):
        factory = APIRequestFactory()
        request = factory.get('/categories/tree')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercise-list'))
        view = views.CategoryView.as_view({'get': 'tree'})
        languages = Category.objects.filter(pk=4)
        response = view(request, pk=4)
        serializer = CategoryTreeSerializer(
            languages, many=True)
        # code.interact(local=locals())
        print(json.dumps(response.data, indent=4))
        print("--------")
        print(json.dumps(serializer.data, indent=4))
        expected = [[{'category_name': 'Programowanie',
                      'full_name': 'Informatyka -> Programowanie',
                      'children': [
                        {'category_name': 'Programowanie C++',
                         'full_name': 'Informatyka -> Programowanie -> Programowanie C++',
                         'children': [
                            {'category_name': 'Programowanie obiektowe',
                             'full_name': 'Informatyka -> Programowanie -> Programowanie C++ -> Programowanie obiektowe',
                             'id': 7},
                            {'category_name': 'Programowanie proceduralne',
                             'full_name': 'Informatyka -> Programowanie -> Programowanie C++ -> Programowanie proceduralne',
                             'id': 6}],
                         'id': 5},
                        {'category_name': 'Programowanie Java',
                         'full_name': 'Informatyka -> Programowanie -> Programowanie Java',
                         'id': 8}],
                      'id': 4}]]
        # code.interact(local=locals())
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.data, expected)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_create_category(self):
        data = {
          "category_name": "NowaKategoria",
          "parent": "1"
        }
        factory = APIRequestFactory()
        request = factory.post("/categories/", data,
                               format='json')
        view = views.CategoryView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        category_num_before = Category.objects.count()
        response = view(request)
        category_num_after = Category.objects.count()
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(category_num_after,
                         category_num_before+1)
        self.assertEqual(
            Category.objects.filter(
                category_name="NowaKategoria")[0].category_name,
            "NowaKategoria")

    def test_update_category(self):
        data = {
          "category_name": "NowaGeometria"
        }
        # client = Client()
        factory = APIRequestFactory()
        request = factory.put("/categories/", data,
                              format='json')
        view = views.CategoryView.as_view({'put': 'update'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        category_num_before = Category.objects.count()
        response = view(request, pk=11)
        category_num_after = Category.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(category_num_after,
                         category_num_before)
        self.assertEqual(
            Category.objects.get(pk=11).category_name,
            'NowaGeometria')
        self.assertEqual(
            Category.objects.get(pk=11).parent.id, 1)

    def test_destroy_category(self):
        factory = APIRequestFactory()
        request = factory.delete("/categories/",
                                 format='json')
        view = views.CategoryView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        category_num_before = Category.objects.count()
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=1)
        # code.interact(local=locals())
        category_num_after = Category.objects.count()
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        # self.assertEqual(category_num_after,
        #                  category_num_before-4)
        self.assertEqual(category_num_after,
                         category_num_before-1)
        self.assertEqual(
            len(Exercise.objects.filter(category__id=1)), 0)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(
            Exercise.objects.get(id=6).category, None)

    def test_destroy_categories(self):
        factory = APIRequestFactory()
        request = factory.delete("/categories/",
                                 format='json')
        view = views.CategoryView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        category_num_before = Category.objects.count()
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=None)
        # code.interact(local=locals())
        category_num_after = Category.objects.count()
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        # self.assertEqual(category_num_after,
        #                  category_num_before-4)
        self.assertEqual(category_num_before,
                         11)
        self.assertEqual(category_num_after,
                         0)
        self.assertEqual(
            len(Exercise.objects.filter(category__id=1)), 0)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(
            Exercise.objects.get(id=6).category, None)


    def test_destroy_many_category(self):
        factory = APIRequestFactory()
        request = factory.delete("/categories/tree",
                                 format='json')
        view = views.CategoryView.as_view(
            {'delete': 'tree'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        category_num_before = Category.objects.count()
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=5)
        # code.interact(local=locals())
        category_num_after = Category.objects.count()
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        # self.assertEqual(category_num_after,
        #                  category_num_before-4)
        self.assertEqual(category_num_after,
                         category_num_before-2)

    def test_destroy_all_category(self):
        factory = APIRequestFactory()
        request = factory.delete("/categories/treeall",
                                 format='json')
        view = views.CategoryView.as_view(
            {'delete': 'treeall'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        category_num_before = Category.objects.count()
        exercise_num_before = Exercise.objects.count()
        response = view(request)
        # code.interact(local=locals())
        category_num_after = Category.objects.count()
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        # self.assertEqual(category_num_after,
        #                  category_num_before-4)
        self.assertEqual(category_num_after,
                         category_num_before-11)


class CategoryViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))

    def tearDown(self):
        print("E---------end test----------")

    def test_list_categories(self):
        factory = APIRequestFactory()
        request = factory.get('/categories/')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercise-list'))
        view = views.CategoryView.as_view({'get': 'list'})
        response = view(request)
        languages = Category.objects.all()
        serializer = CategorySerializer(languages,
                                        many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_retrieve_categories(self):
        factory = APIRequestFactory()
        request = factory.get('/categories/')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercise-list'))
        view = views.CategoryView.as_view({'get': 'retrieve'})
        response = view(request, pk=1)
        languages = Category.objects.filter(pk=1)[0]
        serializer = CategorySerializer(languages)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)


class CategorySerializerTestCase(TestCase):
    def setUp(self):
        pass

    def test_simply(self):
        val_dict = {'id': '1', 'category_name': 'fizyka'}
        serializer = CategorySerializer(data=val_dict)
        try:
            self.assertEqual(serializer.is_valid(), True)
        except AssertionError as error:
            raise

    def test_long_name_field(self):
        name_field = ''.join(['long_name' for i in range(61)])
        val_dict = {'id': '1', 'category_name': name_field}
        serializer = CategorySerializer(data=val_dict)
        try:
            self.assertEqual(serializer.is_valid(), False)
            self.assertEqual(
                serializer.errors['category_name'][0].code,
                'max_length')
        except AssertionError as error:
            raise

class CategoryFlatTreeSerializerTestCase(TestCase):
    fixtures = ["many_categories.json"]
    def setUp(self):
        pass

    def test_flat_subcategory(self):
        # categories = Category.objects.filter(pk=4)
        # code.interact(local=locals())
        languages = Category.objects.filter(pk=5)
        serializer = CategoryFlatTreeSerializer(
            languages, many=True)
        print(serializer.data)
        # expected_data = [[{'category_name': 'Programowanie C++', 'id': 5},
        #                   {'category_name': 'Programowanie obiektowe', 'id': 7},
        #                   {'category_name': 'Programowanie proceduralne', 'id': 6}]]
        expected_data = [[7,6]]
        self.assertEqual(serializer.data, expected_data)
        self.assertEqual(len(serializer.data[0]), 2)

    def test_flat_none_category(self):
        # categories = Category.objects.filter(pk=4)
        # code.interact(local=locals())
        languages = Category.objects.filter(parent=None)

        serializer = CategoryFlatTreeSerializer(
            languages, many=True)
        expected_data = [[3, 4, 5, 7, 6, 8], [9, 11, 10]]
        self.assertEqual(serializer.data, expected_data)
        # print(serializer.data)
        # expected_data = [[{'category_name': 'Programowanie C++', 'id': 5},
        #                   {'category_name': 'Programowanie obiektowe', 'id': 7},
        #                   {'category_name': 'Programowanie proceduralne', 'id': 6}]]
        # self.assertEqual(serializer.data, expected_data)
        # code.interact(local=locals())
        self.assertEqual(len(serializer.data[0]), 6)
        self.assertEqual(len(serializer.data[1]), 3)
        # code.interact(local=locals())
