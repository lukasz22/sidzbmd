from django.core.files.base import ContentFile
from django.shortcuts import get_object_or_404

from rest_framework import serializers

from ..models.exercise import Exercise
from ..models.attachment import Attachment
from ..models.preambule import Preambule
from recipes.latexlib import LatexExercise, LatexFile, LatexAttachment

from datetime import datetime
import os
import code
import logging


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling
    image-uploads through raw post data.
    It uses base64 for encoding and decoding
    the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_representation(self, obj):
        import base64
        filename = obj.path
        logging.info("Making the alpha-ldap connection");
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(
                image_file.read())
        # code.interact(local=locals())
        return {
            'width': obj.width,
            'height': obj.height,
            'size': obj.size,
            'base64_string': encoded_string.decode()
        }

    def to_internal_value(self, data):

        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):

            # Check if the base64 string is
            # in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header
                # from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file.
            # Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12]
            # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(
                file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name,
                                            file_extension)
            mykwargs = {"abs_path": "siemka"}
            data = ContentFile(decoded_file,
                               name=complete_file_name)

        return super(Base64ImageField,
                     self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        if extension == "jpeg":
            extension = "jpg"
        return extension


class ContentFileOverride(ContentFile):
    def __init__(self, *args, **kwargs):
        self.abs_path = kwargs.pop('abs_path', None)
        super(ContentFileOverride, self).__init__(*args, **kwargs)


class AttachmentSerializer(serializers.ModelSerializer):
    image = Base64ImageField(
        max_length=None, use_url=True,
    )

    def validate_width(self, data):
        """
        Check that the start is before the stop.
        """
        if data <= 0:
            raise serializers.ValidationError("Wymagana wartość dodatnia")
        non_abs_path = "obrazki/agh.jpg"
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # code.interact(local=locals())
        tex_exercise = LatexExercise(
            title="XXX",
            intro="XXX",
            content="XXX",
            hint="XXX",
            solution="XXX",
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
        for att_type in ["intro", "content", "hint", "solution"]:
            # code.interact(local=locals())
            tex_attachment = LatexAttachment(
                width=data,
                image_path=non_abs_path,
                caption="XXX",
                label="XXX")
            tex_exercise.add_attachment_obj(
                att_type,
                tex_attachment)
        tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podana wartość skali uniemożliwia wygenerowanie dokumentu PDF.")


    def validate_caption(self, data):
        """
        Check that the start is before the stop.
        """
        non_abs_path = "obrazki/agh.jpg"

        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # code.interact(local=locals())
        tex_exercise = LatexExercise(
            title="XXX",
            intro="XXX",
            content="XXX",
            hint="XXX",
            solution="XXX",
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
        for att_type in ["intro", "content", "hint", "solution"]:
            # code.interact(local=locals())
            tex_attachment = LatexAttachment(
                width=1,
                image_path=non_abs_path,
                caption=data,
                label="XXX")
            tex_exercise.add_attachment_obj(
                att_type,
                tex_attachment)
        tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany tekst uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_label(self, data):
        """
        Check that the start is before the stop.
        """
        non_abs_path = "obrazki/agh.jpg"
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # code.interact(local=locals())
        tex_exercise = LatexExercise(
            title="XXX",
            intro="XXX",
            content="XXX",
            hint="XXX",
            solution="XXX",
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
        for att_type in ["intro", "content", "hint", "solution"]:
            # code.interact(local=locals())
            tex_attachment = LatexAttachment(
                width=1,
                image_path=non_abs_path,
                caption="XXX",
                label=data)
            tex_exercise.add_attachment_obj(
                att_type,
                tex_attachment)
        tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podana etykieta uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_image(self, data):
        """
        Check that the start is before the stop.
        """
        non_abs_path = "pic_folder/{0}".format(data.name)
        try:
            with open(non_abs_path, "wb") as image_file:
                image_file.write(data.read())
            tex_file = LatexFile(
                document_title="XXX",
                author="XXX",
                date="XXX",
                preambule=get_preambule_content())
            # code.interact(local=locals())
            tex_exercise = LatexExercise(
                title="XXX",
                intro="XXX",
                content="XXX",
                hint="XXX",
                solution="XXX",
                is_hint=True,
                is_title=True,
                is_intro=True,
                is_solution=True)
            for att_type in ["intro", "content", "hint", "solution"]:
                # code.interact(local=locals())
                tex_attachment = LatexAttachment(
                    width=1,
                    image_path=non_abs_path,
                    caption="XXX",
                    label="XXX{0}".format(att_type))
                tex_exercise.add_attachment_obj(
                    att_type,
                    tex_attachment)
            tex_file.add_exercise_obj(tex_exercise)
            if tex_file.is_valid():
                return data
            raise serializers.ValidationError("Podany obraz uniemożliwia wygenerowanie dokumentu PDF.")
        except Exception as e:
            raise e
        finally:
            # code.interact(local=locals())
            os.remove(non_abs_path)
            # code.interact(local=locals())

    class Meta:
        model = Attachment
        fields = ('id', 'image', 'name', 'exercise', 'attachment_type',
                  'caption', 'label', 'width')

def get_preambule_content():
    queryset = Preambule.objects.all()
    preambule_obj = get_object_or_404(queryset, pk=1)
    return preambule_obj.content
