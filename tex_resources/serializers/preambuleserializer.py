from rest_framework import serializers
from accesses.models import Userprofile
from ..models import Preambule
from django.contrib.auth.models import User


# class UserSimplySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'email')


class PreambuleSerializer(serializers.ModelSerializer):
    # user = UserSimplySerializer(read_only=True)

    class Meta:
        model = Preambule
        fields = ('id', 'preambule_title', 'content')
