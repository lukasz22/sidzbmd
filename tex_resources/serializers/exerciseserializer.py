from django.shortcuts import get_object_or_404

from rest_framework import serializers

from ..models import Exercise
from ..models import Attachment
from ..models import Preambule
from other.serializers import CategorySerializer
from other.serializers import LanguageSerializer
from recipes.latexlib import LatexExercise, LatexFile
from .attachmentserializer import AttachmentSerializer
from datetime import datetime
import os
import code


class ExerciseSerializer(serializers.ModelSerializer):
    attachments = AttachmentSerializer(many=True)

    def __init__(self, *args, **kwargs):
        # code.interact(local=locals())
        self.request = kwargs.pop('request', None)
        super(ExerciseSerializer, self).__init__(*args, **kwargs)

    def validate_title(self, data):
        """
        Check that the start is before the stop.
        """
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # code.interact(local=locals())
        tex_exercise = LatexExercise(
            title=data,
            intro="XXX",
            content="XXX",
            hint="XXX",
            solution="XXX",
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
            # is_hint=single_exercise["is_hint"],
            # is_title=True,
            # is_intro=single_exercise["is_intro"],
            # is_solution=single_exercise["is_solution"])
        tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany tekst uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_intro(self, data):
        """
        Check that the start is before the stop.
        """
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # code.interact(local=locals())
        tex_exercise = LatexExercise(
            title="XXX",
            intro=data,
            content="XXX",
            hint="XXX",
            solution="XXX",
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
            # is_hint=single_exercise["is_hint"],
            # is_title=True,
            # is_intro=single_exercise["is_intro"],
            # is_solution=single_exercise["is_solution"])
        tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany teskt uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_task_content(self, data):
        """
        Check that the start is before the stop.
        """
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # code.interact(local=locals())
        tex_exercise = LatexExercise(
            title="XXX",
            intro="XXX",
            content=data,
            hint="XXX",
            solution="XXX",
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
            # is_hint=single_exercise["is_hint"],
            # is_title=True,
            # is_intro=single_exercise["is_intro"],
            # is_solution=single_exercise["is_solution"])
        tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany tekst uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_hint(self, data):
        """
        Check that the start is before the stop.
        """
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # code.interact(local=locals())
        tex_exercise = LatexExercise(
            title="XXX",
            intro="XXX",
            content="XXX",
            hint=data,
            solution="XXX",
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
            # is_hint=single_exercise["is_hint"],
            # is_title=True,
            # is_intro=single_exercise["is_intro"],
            # is_solution=single_exercise["is_solution"])
        tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany tekst uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_solution(self, data):
        """
        Check that the start is before the stop.
        """
        tex_file = LatexFile(
            document_title="XXX",
            author="XXX",
            date="XXX",
            preambule=get_preambule_content())
        # code.interact(local=locals())
        tex_exercise = LatexExercise(
            title="XXX",
            intro="XXX",
            content="XXX",
            hint="XXX",
            solution=data,
            is_hint=True,
            is_title=True,
            is_intro=True,
            is_solution=True)
            # is_hint=single_exercise["is_hint"],
            # is_title=True,
            # is_intro=single_exercise["is_intro"],
            # is_solution=single_exercise["is_solution"])
        tex_file.add_exercise_obj(tex_exercise)
        if tex_file.is_valid():
            return data
        raise serializers.ValidationError("Podany tekst uniemożliwia wygenerowanie dokumentu PDF.")

    def validate_policies(self, data):
        # code.interact(local=locals())
        result = [i.name for i in data if i.owner and i.owner.id is not int(self.request.data["owner"])]
        if result:
            raise serializers.ValidationError("Wybrany użytkownik " +
                "nie jest właścicielem profili dostępu: " + ",".join(result) +
                ". Wybierz takie profile dostępu, dla których" +
                " jest on właścicielem.")
        return data

    class Meta:
        model = Exercise
        fields = ('id', 'title', 'intro', 'task_content',
                  'hint', 'solution', 'creation_date',
                  'modified_date', 'owner', 'policies',
                  'category', 'language',
                  'attachments')
#                  'modified_date', 'owner')

    def to_representation(self, obj):
        # code.interact(local=locals())
        return {
          "id": obj.id,
          "title": obj.title,
          "intro": obj.intro,
          "task_content": obj.task_content,
          "hint": obj.hint,
          "solution": obj.solution,
          "category": CategorySerializer(obj.category).data,
          "language": LanguageSerializer(obj.language).data,
          "owner": {"id": obj.owner.id, "username": obj.owner.username},
          "policies": [{"id": i.id, "name": i.name} for i in obj.policies.all()],
          "attachments": AttachmentSerializer(obj.attachments.all(), many=True).data
        }

    def create(self, validated_data):
        # code.interact(local=locals())
        attachments_data = validated_data.pop('attachments')
        policy_data = validated_data.pop('policies')
        exercise = Exercise.objects.create(**validated_data)
        exercise.policies.set(policy_data)
        # code.interact(local=locals())
        for attachment_data in attachments_data:
            Attachment.objects.create(exercise=exercise,
                                      **attachment_data)
        return exercise

    def update(self, instance, validated_data):
        attachments_data = validated_data.pop('attachments')
        # Unless the application properly enforces
        # that this field is
        # always set, the follow could raise a `DoesNotExist`,
        # which would need to be handled.
        instance.title = validated_data.get('title',
                                            instance.title)
        instance.intro = validated_data.get('intro',
                                            instance.intro)
        instance.owner = validated_data.get('owner',
                                            instance.owner)
        instance.task_content = validated_data.get(
            'task_content', instance.task_content)
        instance.hint = validated_data.get('hint',
                                           instance.hint)
        instance.solution = validated_data.get(
            'solution',
            instance.solution)
        instance.modified_date = datetime.now()
        # instance.policies.all().delete()
        instance.policies.set(validated_data.get(
                'policies',
                instance.policies.all))
        # for policy in validated_data.get(
        #         'policies',
        #         instance.policies.all()):
        #     instance.policies.add(policy)

        instance.category = validated_data.get(
            'category', instance.category)
        instance.language = validated_data.get(
            'language', instance.language)
        instance.save()
        return instance


class ExerciseContainerSerializer(serializers.Serializer):
    total = serializers.IntegerField()
    exercises = ExerciseSerializer(many=True)

    def to_representation(self, obj):
        return {
          "total": obj['total'],
          "exercises": obj['exercises']
        }

def get_preambule_content():
    queryset = Preambule.objects.all()
    preambule_obj = get_object_or_404(queryset, pk=1)
    return preambule_obj.content
