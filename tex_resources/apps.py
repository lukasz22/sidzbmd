from django.apps import AppConfig


class TexResourcesConfig(AppConfig):
    name = 'tex_resources'
