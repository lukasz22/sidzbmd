from copy import deepcopy
from tex_resources.models.attachment import AttachmentTypes
import code

def filter_exercises(request_user, exercises):
    # code.interact(local=locals())
    for exercise_orig in exercises:
        if request_user == exercise_orig.owner or request_user.userprofile.is_administrator:
            yield exercise_orig
        else:
            exercise = deepcopy(exercise_orig)
            # code.interact(local=locals())
            exercise_perm_ok_list = []
            exercise_vis_permissions =\
                exercise.policies.all()
            # if exercise.id == 22:
            #     code.interact(local=locals())
            for exercise_vis_permission in exercise_vis_permissions:
                if (exercise_vis_permission.group in
                        request_user.userprofile.policy_groups.all()):
                    exercise_perm_ok_list.append(
                        exercise_vis_permission)
                elif exercise_vis_permission.group.policytype == "all":
                    exercise_perm_ok_list.append(
                        exercise_vis_permission)
            ex_vis_list = [
                perm.exercise_vis
                for perm in exercise_perm_ok_list]
            intro_vis_list = [
                perm.intro_vis
                for perm in exercise_perm_ok_list]
            hint_vis_list = [
                perm.hint_vis
                for perm in exercise_perm_ok_list]
            solution_vis_list = [
                perm.solution_vis
                for perm in exercise_perm_ok_list]
            if True not in intro_vis_list:
                exercise.intro = None
                # code.interact(local=locals())
                for att in exercise.attachments.all():
                    if (str(att.attachment_type) == str(AttachmentTypes.exercise_intro.name)):
                        exercise.attachments.remove(att)
            if True not in hint_vis_list:
                exercise.hint = None
                for att in exercise.attachments.all():
                    if str(att.attachment_type) == str(AttachmentTypes.exercise_hint.name):
                        exercise.attachments.remove(att)
            if True not in solution_vis_list:
                # code.interact(local=locals())
                exercise.solution = None
                for att in exercise.attachments.all():
                    if str(att.attachment_type) == str(AttachmentTypes.exercise_solution.name):
                        exercise.attachments.remove(att)
            if True in ex_vis_list:
                yield exercise
