from django.db import models
from .exercise import Exercise

from enum import Enum


import os
from django.core.files.storage import default_storage
from django.db.models import FileField

from django.db.models.signals import post_delete

from django.core.validators import MinValueValidator

import code

def file_cleanup(sender, **kwargs):
    """
    File cleanup callback used to emulate the old delete
    behavior using signals. Initially django deleted linked
    files when an object containing a File/ImageField was deleted.

    Usage:
    >>> from django.db.models.signals import post_delete
    >>> post_delete.connect(file_cleanup, sender=MyModel, dispatch_uid="mymodel.file_cleanup")
    """
    # code.interact(local=locals())
    for fieldname in sender._meta.get_fields():
        field = sender._meta.get_field(fieldname.name)
        # field = None
        # code.interact(local=locals())
        if field and isinstance(field, models.ImageField) and fieldname.name == 'image':
            inst = kwargs['instance']
            f = getattr(inst, fieldname.name)
            # code.interact(local=locals())
            if (hasattr(f, 'path') and os.path.exists(f.path)):
                try:
                    default_storage.delete(f.path)
                except Exception:
                    pass


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class AttachmentTypes(ChoiceEnum):
    exercise_content = 0
    exercise_intro = 1
    exercise_hint = 2
    exercise_solution = 3


class Attachment(models.Model):
    """
    Attachment model
    """
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='pic_folder')
    width = models.FloatField(default=1,
                                validators=[MinValueValidator(0)])
    label = models.CharField(unique=True, max_length=255)
    caption = models.CharField(max_length=400, blank=True)
    exercise = models.ForeignKey(Exercise,
                                 related_name='attachments',
                                 on_delete=models.CASCADE,
                                 blank=True, null=True)
    attachment_type = models.CharField(max_length=255,
                                       choices=AttachmentTypes.choices())

    def __unicode__(self):
        return "{0}".format(self.name)


post_delete.connect(file_cleanup, sender=Attachment, dispatch_uid="gallery.image.file_cleanup")
