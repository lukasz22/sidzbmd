from django.db import models
from django.contrib.auth.models import User
from datetime import date
from django.utils.translation import gettext as _
from accesses.models import Policy, ExerciseVisibilityPermission
from enum import Enum



class Exercise(models.Model):
    """
    Exercise model
    """
    title = models.CharField(max_length=255)
    intro = models.TextField(null=True, blank=True)
    task_content = models.TextField()
    hint = models.TextField(null=True, blank=True)
    solution = models.TextField(null=True, blank=True)
    creation_date = models.DateTimeField(_("Date"),
                                         default=date.today)
    modified_date = models.DateTimeField(_("Date"),
                                         default=date.today)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    policies = models.ManyToManyField(
        ExerciseVisibilityPermission)
    category = models.ForeignKey('other.Category',
                                 null=True, blank=True,
                                 on_delete=models.SET_NULL)
    language = models.ForeignKey('other.Language',
                                 null=True, blank=True,
                                 on_delete=models.SET_NULL)

    def __unicode__(self):
        return "{0} {1}".format(self.exercise_id, self.title)
