from django.db import models
from django.contrib.auth.models import User
from datetime import date
from django.utils.translation import gettext as _
from accesses.models import Policy, ExerciseVisibilityPermission
from enum import Enum



class Preambule(models.Model):
    """
    Preambule model
    """
    preambule_title = models.CharField(max_length=255)
    content = models.TextField()


    def __unicode__(self):
        return "{0} {1}".format(self.preambule_id, self.preambule_title)
