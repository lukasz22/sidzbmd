from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models import Q

from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate

from tex_resources.models.exercise import Exercise
from tex_resources.models.attachment import Attachment
from tex_resources import serializers
from tex_resources import views

from unittest import skip
import os
from shutil import copyfile
import code

# https://jsonlint.com/
# python manage.py dumpdata > db_draft.json
# http://www.django-rest-framework.org/api-guide/testing/

class ExerciseViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        for index in range(9):
            copyfile(os.path.join(os.getcwd(), 'obrazki/agh.jpg'),
                     os.path.join(os.getcwd(), 'pic_folder/agh{0}.jpg').format(index+1))

    def tearDown(self):
        print("E---------end test----------")
        # os.rmdir("/home/lukasz/Dokumenty/IIstopien/"
        #          "magisterka/sidzbmd/pic_folder")

    # @skip("Don't want to test")
    def test_get_exercises_as_view(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'list'})
        request = factory.get('/exercises/')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)

    def test_get_exercises(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.get('/exercises/')
        user = User.objects.get(username='john2')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercises-list'))
        view = views.ExerciseView.as_view({'get': 'list'})
        response = view(request)
        out_solution = [
            exercise["id"]
            for exercise in response.data['exercises']
            if exercise["solution"] is None]
        out_intro = [
            exercise["intro"]
            for exercise in response.data['exercises']
            if exercise["intro"] is None]
        out_hint = [
            exercise["hint"]
            for exercise in response.data['exercises']
            if exercise["hint"] is None]

        self.assertEqual(out_solution, [21, 20])
        self.assertEqual(out_intro, [])
        self.assertEqual(out_hint, [])
        languages = Exercise.objects.filter(
            Q(id="20") | Q(id="21")).order_by("title")
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']),
                         len(serializer.data))
        # self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_get_exercises_JanProfesor(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.get('/exercises/')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercises-list'))
        view = views.ExerciseView.as_view({'get': 'list'})
        response = view(request)
        out_solution = [
            exercise["id"]
            for exercise in response.data['exercises']
            if exercise["solution"] is None]
        out_intro = [
            exercise["intro"]
            for exercise in response.data['exercises']
            if exercise["intro"] is None]
        out_hint = [
            exercise["hint"]
            for exercise in response.data['exercises']
            if exercise["hint"] is None]

        self.assertEqual(out_solution, [20])
        self.assertEqual(out_intro, [])
        self.assertEqual(out_hint, [])
        languages = Exercise.objects.filter(
            Q(id="20") | Q(id="21") | Q(id="22") |
            Q(id="23")).order_by("title")
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']),
                         len(serializer.data))
        # self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        # code.interact(local=locals())

    def test_get_none_exercises_policy_reason(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.get('/exercises/')
        user = User.objects.get(username='john2')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercises-list'))
        view = views.ExerciseView.as_view({'get': 'retrieve'})
        response = view(request, pk='1')
        languages = Exercise.objects.all()

        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(response.data, None)
        self.assertEqual(response.status_code,
                         status.HTTP_403_FORBIDDEN)

    def test_get_exercise(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.get('/exercises/1/')
        view = views.ExerciseView.as_view({'get': 'retrieve'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request, pk='1')
        languages = Exercise.objects.order_by(
            "title").get(pk=1)
        serializer = serializers.ExerciseSerializer(
            languages)
        self.assertEqual(dict(response.data),
                         serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    @skip("Don't want to test")
    def test_create_exercise_client(self):
        client = Client()
        data = {
          "title": "Rownanie kwadratowe_2",
          "intro": "intro_string",
          "task_content": "task_content_string",
          "hint": "hint_string",
          "solution": "solution_string",
          "owner": "1",
          "policies": [
             "1"
          ]
        }
        url = reverse('exercises-list')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = client.post(url, data, format='json')
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(Exercise.objects.count(), 3)
        self.assertEqual(Exercise.objects.get(pk=3).title,
                         'Rownanie kwadratowe2')

    def test_create_exercise(self):
        client = Client()
        data = {
          "title": "Rownanie kwadratowe2",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "1",
          "policies": [
             "1"
          ],
          "attachments": [
          ]
        }
        url = reverse('exercises-list')
        factory = APIRequestFactory()
        request = factory.post("/exercises/", data,
                               format='json')
        view = views.ExerciseView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exercise_num_after,
                         exercise_num_before+1)
        self.assertEqual(
            Exercise.objects.get(
                pk=exercise_num_before+1).title,
            'Rownanie kwadratowe2')

    def test_create_exercise_non_valid(self):
        client = Client()
        data = {
          "title": "Rownanie kwadratowe_2",
          "intro": "introstring",
          "task_content": "task_content_string",
          "hint": "hint_string",
          "solution": "solution_string",
          "owner": "1",
          "policies": [
             "1"
          ],
          "attachments": [
          ]
        }
        url = reverse('exercises-list')
        factory = APIRequestFactory()
        request = factory.post("/exercises/", data,
                               format='json')
        view = views.ExerciseView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request)
        exercise_num_after = Exercise.objects.count()
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(isinstance(response.data['title'][0], ErrorDetail), True)
        # self.assertEqual(isinstance(response.data['intro'][0], ErrorDetail), True)
        self.assertEqual(isinstance(response.data['task_content'][0], ErrorDetail), True)
        self.assertEqual(isinstance(response.data['hint'][0], ErrorDetail), True)
        self.assertEqual(isinstance(response.data['solution'][0], ErrorDetail), True)
        self.assertEqual(len(response.data), 4)

    def test_create_exercise_with_attachment(self):
        client = Client()
        import base64
        filename = os.path.join(os.getcwd(), "obrazki/agh.jpg")
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(
                image_file.read())
        data = {
          "title": "Rownanie kwadratowe2",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "1",
          "policies": [
             "1"
          ],
          "attachments": [
            {
                "id": "2",
                "name": "Jakaś nazwa",
                "width": "0.10",
                "label": "attachmentPK10",
                "caption": "captionPK10",
                "image":
                    "data:image/jpg;base64," +
                    encoded_string.decode(),
                "attachment_type": "exercise_content"
            }
          ]
        }
        # "obrazki/agh.jpg"

        url = reverse('exercises-list')
        factory = APIRequestFactory()
        request = factory.post("/exercises/", data,
                               format='json')
        view = views.ExerciseView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        attachment_num_before = Attachment.objects.count()
        response = view(request)
        exercise_num_after = Exercise.objects.count()
        attachment_num_after = Attachment.objects.count()
        # code.interact(local=locals())
        self.assertEqual(attachment_num_after,
                         attachment_num_before+1)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exercise_num_after,
                         exercise_num_before+1)
        self.assertEqual(
            Exercise.objects.get(
                pk=response.data['id']).title,
            'Rownanie kwadratowe2')
        self.assertEqual(response.data['attachments'][0]
                                      ['image']
                                      ['base64_string'],
                         encoded_string.decode())

    def test_update_exercise(self):
        client = Client()
        import base64
        filename = os.path.join(os.getcwd(), "obrazki/agh.jpg")
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(
                image_file.read())
        data = {
          "title": "Rownanie kwadratoweupdate",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "1",
          "language": "2",
          "category": "3",
          "policies": [
             "2"
          ],
          "attachments": [
          ]
        }
        client = Client()
        factory = APIRequestFactory()
        request = factory.put("/exercises/", data,
                              format='json')
        view = views.ExerciseView.as_view({'put': 'update'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=9)
        exercise_num_after = Exercise.objects.count()
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(Exercise.objects.get(pk=9).title,
                         'Rownanie kwadratoweupdate')
        self.assertEqual(Exercise.objects.get(pk=9).language.id,
                         int(data['language']))
        self.assertEqual(Exercise.objects.get(pk=9).category.id,
                         int(data['category']))
        self.assertEqual(Exercise.objects.get(pk=9).policies.all()[0].id,
                         int(data['policies'][0]))

    def test_update_foreign_exercise(self):
        client = Client()
        import base64
        filename = os.path.join(os.getcwd(), "obrazki/agh.jpg")
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(
                image_file.read())
        data = {
          "title": "Rownanie kwadratoweupdate",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "1",
          "language": "2",
          "category": "3",
          "policies": [
             "2"
          ],
          "attachments": [
          ]
        }
        client = Client()
        factory = APIRequestFactory()
        request = factory.put("/exercises/", data,
                              format='json')
        view = views.ExerciseView.as_view({'put': 'update'})
        # code.interact(local=locals())
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=9)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_403_FORBIDDEN)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(response.data,
                         "is not you exercise")

    def test_update_owner_exercise(self):
        client = Client()
        import base64
        filename = os.path.join(os.getcwd(), "obrazki/agh.jpg")
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(
                image_file.read())
        data = {
          "title": "Rownanie kwadratoweupdate",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "3", # JanProfesor
          "language": "2",
          "category": "3",
          "policies": [
             "2"
          ],
          "attachments": [
          ]
        }
        client = Client()
        factory = APIRequestFactory()
        request = factory.put("/exercises/", data,
                              format='json')
        view = views.ExerciseView.as_view({'put': 'update'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=9)
        exercise_num_after = Exercise.objects.count()
        # code.interact(local=locals())
        self.assertEqual(Exercise.objects.get(pk=9).owner.username,
                         '3bartocha')
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(Exercise.objects.get(pk=9).title,
                         'Rownanie kwadratoweupdate')
        self.assertEqual(Exercise.objects.get(pk=9).language.id,
                         int(data['language']))
        self.assertEqual(Exercise.objects.get(pk=9).category.id,
                         int(data['category']))
        self.assertEqual(Exercise.objects.get(pk=9).policies.all()[0].id,
                         int(data['policies'][0]))

    def test_destroy_exercise(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.delete("/exercises/",
                                 format='json')
        view = views.ExerciseView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=1)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before-1)

    def test_destroy_exercises(self):
        factory = APIRequestFactory()
        request = factory.delete("/exercises/",
                                 format='json')
        view = views.ExerciseView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=None)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before-21)

    def test_destroy_exercise_with_attachment(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.delete("/exercises/",
                                 format='json')
        view = views.ExerciseView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        attachment_num_before = Attachment.objects.count()
        # code.interact(local=locals())
        response = view(request, pk=9)
        attachment_num_after = Attachment.objects.count()
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before-1)
        self.assertEqual(attachment_num_after,
                         attachment_num_before-2)

    def test_destroy_foreign_exercise(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.delete("/exercises/",
                                 format='json')
        view = views.ExerciseView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='john2')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=1)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_403_FORBIDDEN)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(response.data,
                         "is not you exercise")

    def test_find_exercise_conj(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get('/exercises/find/?title=Pole')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.filter(
            title__icontains='Pole').order_by("title")
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_conj_username(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?owner=3bartocha&category=1')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.filter(
            Q(id__gte=6) & Q(id__lte=20)).order_by("title")
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']), 15)
        self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_specific_items(self):
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?id=1&id=2&id=3')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.all()
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']), 3)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_conj_username_zero(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?owner=3bartocha&category=8')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.all()
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(response.data['exercises'], [])
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_alter_username_one(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?operation=or'
            '&owner=3bartocha&category=1')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.filter(
            Q(id__gte=6) & Q(id__lte=20)).order_by("title")
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        print("response.data['exercises']: {0}".format(
            len(response.data['exercises'])))
        print("serializer.data: {0}".format(
            len(serializer.data)))
        self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_alter_username_zero(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get('/exercises/find/?operation=or'
                              '&owner=3bartochaA&category=3')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.all()
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(response.data['exercises'], [])
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_alter_one(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get('/exercises/find/?language=1&title=Nikt&intro=Nikt&content=Nikt&hint=Nikt&solution=Nikt&operation=or')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.all()
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']), 1)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)


class ExerciseFindViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]
    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        for index in range(9):
            copyfile(os.path.join(os.getcwd(), "obrazki/agh.jpg"),
                    os.path.join(os.getcwd(), "pic_folder/agh{0}.jpg").format(index+1))
    def tearDown(self):
        print("E---------end test----------")

    def test_get_page_exercises(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.get('/exercises/?page=2&pagesize=4')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercises-list'))
        view = views.ExerciseView.as_view({'get': 'list'})
        response = view(request)
        # code.interact(local=locals())
        languages = Exercise.objects.filter(
            owner__username="3bartocha").order_by(
                'title')[3:7]
        serializer = serializers.ExerciseSerializer(
            languages, many=True)

        self.assertEqual(len(response.data['exercises']), 4)
        self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_get_last_page_exercises(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.get(
            '/exercises/?page=4&pagesize=6')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        # response = client.get(reverse('exercises-list'))
        view = views.ExerciseView.as_view({'get': 'list'})
        response = view(request)

        languages = Exercise.objects.filter(
            Q(owner__username="3bartocha") |
            Q(id='22')).order_by('title')[18::]
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']), 4)
        self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_conj_subcatogires_empty_category(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?owner=3bartocha&category=')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(len(response.data['exercises']), 21)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_conj_subcatogires(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?owner=3bartocha&category=4')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(len(response.data['exercises']), 4)
        # code.interact(local=locals())
        for exercise in response.data['exercises']:
            self.assertEqual(
                exercise['id'] >= 1 and (exercise['id'] <= 3 or exercise['id'] == 24),
                True)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_alter_subcatogires_zero(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?operation=or&'
            'intro=zadaniuA&title=Cpp&category=1')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(len(response.data['exercises']), 0)
        # code.interact(local=locals())
        # for exercise in response.data['exercises']:
        #     self.assertEqual(
        #          exercise['id'] >= 1 and exercise['id'] <= 3,
        #          True)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_pages_conj_language(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?page=1&pagesize=2&language=2')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(len(response.data['exercises']), 0)
        # code.interact(local=locals())
        # for exercise in response.data['exercises']:
        #     self.assertEqual(
        #          exercise['id'] >= 1 and exercise['id'] <= 3,
        #          True)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_alter_subcatogires(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?operation=or&'
            'intro=zadaniuA&title=Cpp&category=2')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(len(response.data['exercises']), 2)
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        for exercise in response.data['exercises']:
            self.assertEqual(
                exercise['id'] == 2 or exercise['id'] == 4,
                True)

    def test_find_only_pytajnik(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(len(response.data['exercises']), 0)


class ExerciseVisibilityPermissionTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        for index in range(9):
            copyfile(os.path.join(os.getcwd(), "obrazki/agh.jpg"),
                    os.path.join(os.getcwd(), "pic_folder/agh{0}.jpg").format(index+1))

    def tearDown(self):
        print("E---------end test----------")

    def test_get_exercises_from_matma_category_3bartocha(self):
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'list'})
        request = factory.get('/exercises/')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(len(response.data['exercises']), 22)
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_get_exercises_from_matma_category_JanProfesor(self):
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'list'})
        request = factory.get('/exercises/')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(len(response.data['exercises']), 4)
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_get_exercises_from_matma_category_john2(self):
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'list'})
        request = factory.get('/exercises/')
        user = User.objects.get(username='john2')
        force_authenticate(request, user=user)
        response = view(request)
        # code.interact(local=locals())
        self.assertEqual(len(response.data['exercises']), 2)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)


class IntroHintSolutionVisibilityTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        for index in range(9):
            copyfile(os.path.join(os.getcwd(), "obrazki/agh.jpg"),
                    os.path.join(os.getcwd(), "pic_folder/agh{0}.jpg").format(index+1))

    def tearDown(self):
        print("E---------end test----------")

    def test_get_non_fully_exercise_JanProfesor(self):
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view(
            {'get': 'retrieve'})
        request = factory.get('/exercises/')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, pk="20")
        exercise = Exercise.objects.get(id="20")
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        # code.interact(local=locals())
        self.assertEqual(response.data['solution'], None)
        self.assertIsInstance(exercise.solution, str)

class AdminExerciseViewTestCase(TestCase):
    fixtures = ["many_exercises.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        for index in range(9):
            copyfile(os.path.join(os.getcwd(), 'obrazki/agh.jpg'),
                     os.path.join(os.getcwd(), 'pic_folder/agh{0}.jpg').format(index+1))

    def tearDown(self):
        print("E---------end test----------")

    def test_get_not_admin_exercise(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.get('/exercises/21/')
        view = views.ExerciseView.as_view({'get': 'retrieve'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request, pk='21')
        languages = Exercise.objects.order_by(
            "title").get(pk=21)
        serializer = serializers.ExerciseSerializer(
            languages)
        self.assertEqual(dict(response.data),
                         serializer.data)
        self.assertEqual(response.data["owner"]["username"],
                         "JanProfesor")
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_create_exercise_for_other_user(self):
        client = Client()
        data = {
          "title": "Rownanie kwadratowe2",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "john2",
          "policies": [
             "8", "7"
          ],
          "attachments": [
          ]
        }
        url = reverse('exercises-list')
        factory = APIRequestFactory()
        request = factory.post("/exercises/", data,
                               format='json')
        view = views.ExerciseView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exercise_num_after,
                         exercise_num_before+1)
        self.assertEqual(
            Exercise.objects.get(
                pk=exercise_num_before+1).title,
            'Rownanie kwadratowe2')
        # code.interact(local=locals())
        self.assertEqual(
            Exercise.objects.get(
                pk=exercise_num_before+1).owner.username,
            'john2')

    def test_create_exercise_for_other_user_with_not_his_policy(self):
        client = Client()
        data = {
          "title": "Rownanie kwadratowe2",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "john2",
          "policies": [
             "1"
          ],
          "attachments": [
          ]
        }
        url = reverse('exercises-list')
        factory = APIRequestFactory()
        request = factory.post("/exercises/", data,
                               format='json')
        view = views.ExerciseView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        # code.interact(local=locals())
        self.assertEqual(str(response.data['policies'][0]),
                         "Wybrany użytkownik nie jest właścicielem profili "
                         "dostępu: for_students. Wybierz takie profile dostępu,"
                         " dla których jest on właścicielem.")


    def test_create_exercise_with_attachment_for_other_user(self):
        client = Client()
        import base64
        filename = os.path.join(os.getcwd(), "obrazki/agh.jpg")
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(
                image_file.read())
        data = {
          "title": "Rownanie kwadratowe2",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "john2", # john2
          "policies": [
             "7", "8"
          ],
          "attachments": [
            {
                "id": "2",
                "name": "Jakaś nazwa",
                "width": "0.10",
                "label": "attachmentPK10",
                "caption": "captionPK10",
                "image":
                    "data:image/jpg;base64," +
                    encoded_string.decode(),
                "attachment_type": "exercise_content"
            }
          ]
        }
        # "obrazki/agh.jpg"

        url = reverse('exercises-list')
        factory = APIRequestFactory()
        request = factory.post("/exercises/", data,
                               format='json')
        view = views.ExerciseView.as_view({'post': 'create'})
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        attachment_num_before = Attachment.objects.count()
        response = view(request)
        # code.interact(local=locals())
        exercise_num_after = Exercise.objects.count()
        attachment_num_after = Attachment.objects.count()
        # code.interact(local=locals())
        self.assertEqual(attachment_num_after,
                         attachment_num_before+1)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(exercise_num_after,
                         exercise_num_before+1)
        self.assertEqual(
            Exercise.objects.get(
                pk=response.data['id']).title,
            'Rownanie kwadratowe2')
        self.assertEqual(response.data['attachments'][0]
                                      ['image']
                                      ['base64_string'],
                         encoded_string.decode())
        # code.interact(local=locals())
        self.assertEqual(
            Exercise.objects.get(
                pk=response.data['id']).owner.username,
            'john2')

    def test_update_exercise_for_other_user_bad_policy(self):
        client = Client()
        import base64
        filename = os.path.join(os.getcwd(), "obrazki/agh.jpg")
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(
                image_file.read())
        data = {
          "title": "Rownanie kwadratoweupdate",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "john2", # zmieniamy z JanProfesor na john2
          "language": "2",
          "category": "3",
          "policies": [
             "2"
          ],
          "attachments": [
          ]
        }
        client = Client()
        factory = APIRequestFactory()
        request = factory.put("/exercises/", data,
                              format='json')
        view = views.ExerciseView.as_view({'put': 'update'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        self.assertEqual(
            Exercise.objects.get(
                pk=21).owner.username,
            'JanProfesor')
        response = view(request, pk=21)
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        # code.interact(local=locals())
        self.assertEqual(str(response.data['policies'][0]),
                         "Wybrany użytkownik nie jest właścicielem profili "
                         "dostępu: for_all. Wybierz takie profile dostępu,"
                         " dla których jest on właścicielem.")


    def test_update_exercise_for_other_user_good_policy(self):
        client = Client()
        import base64
        filename = os.path.join(os.getcwd(), "obrazki/agh.jpg")
        with open(filename, "rb") as image_file:
            encoded_string = base64.b64encode(
                image_file.read())
        data = {
          "title": "Rownanie kwadratoweupdate",
          "intro": "introstring",
          "task_content": "taskcontentstring",
          "hint": "hintstring",
          "solution": "solutionstring",
          "owner": "john2", # zmieniamy z JanProfesor na john2
          "language": "2",
          "category": "3",
          "policies": [
             "8"
          ],
          "attachments": [
          ]
        }
        client = Client()
        factory = APIRequestFactory()
        request = factory.put("/exercises/", data,
                              format='json')
        view = views.ExerciseView.as_view({'put': 'update'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        self.assertEqual(
            Exercise.objects.get(
                pk=21).owner.username,
            'JanProfesor')
        response = view(request, pk=21)
        # code.interact(local=locals())
        self.assertEqual(
            Exercise.objects.get(
                pk=21).owner.username,
            'john2')
        exercise_num_after = Exercise.objects.count()
        # code.interact(local=locals())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before)
        self.assertEqual(Exercise.objects.get(pk=21).title,
                         'Rownanie kwadratoweupdate')
        self.assertEqual(Exercise.objects.get(pk=21).language.id,
                         int(data['language']))
        self.assertEqual(Exercise.objects.get(pk=21).category.id,
                         int(data['category']))
        self.assertEqual(Exercise.objects.get(pk=21).policies.all()[0].id,
                         int(data['policies'][0]))
        # self.assertEqual(response.data['attachments'][0]['image']['base64_string'],
        #                  encoded_string.decode())

    def test_destroy_not_admin_exercise(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.delete("/exercises/",
                                 format='json')
        view = views.ExerciseView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        self.assertEqual(
            Exercise.objects.get(
                pk=21).owner.username,
            'JanProfesor')
        response = view(request, pk=21)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before-1)

    def test_destroy_all_exercises(self):
        factory = APIRequestFactory()
        request = factory.delete("/exercises/",
                                 format='json')
        view = views.ExerciseView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        response = view(request, pk=None)
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         0)

    def test_destroy_admin_exercise_with_attachment(self):
        client = Client()
        factory = APIRequestFactory()
        request = factory.delete("/exercises/",
                                 format='json')
        view = views.ExerciseView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        exercise_num_before = Exercise.objects.count()
        attachment_num_before = Attachment.objects.count()
        # code.interact(local=locals())
        response = view(request, pk=9)
        attachment_num_after = Attachment.objects.count()
        exercise_num_after = Exercise.objects.count()
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(exercise_num_after,
                         exercise_num_before-1)
        self.assertEqual(attachment_num_after,
                         attachment_num_before-2)

    def test_find_exercise_conj(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get('/exercises/find/?title=Pole')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.filter(
            title__icontains='Pole').order_by("title")
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_conj_username(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?owner=3bartocha&category=1')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.filter(
            Q(id__gte=6) & Q(id__lte=20)).order_by("title")
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']), 15)
        self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_specific_items(self):
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?id=1&id=2&id=3')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.all()
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']), 3)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_conj_username_zero(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?owner=3bartocha&category=8')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.all()
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(response.data['exercises'], [])
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_alter_username_one(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get(
            '/exercises/find/?operation=or'
            '&owner=3bartocha&category=1')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.filter(
            Q(id__gte=6) & Q(id__lte=20)).order_by("title")
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        print("response.data['exercises']: {0}".format(
            len(response.data['exercises'])))
        print("serializer.data: {0}".format(
            len(serializer.data)))
        self.assertEqual(response.data['exercises'], serializer.data)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_alter_username_zero(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get('/exercises/find/?operation=or'
                              '&owner=3bartochaA&category=3')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.all()
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(response.data['exercises'], [])
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)

    def test_find_exercise_alter_one_as_admin(self):
        client = Client()
        factory = APIRequestFactory()
        view = views.ExerciseView.as_view({'get': 'find'})
        request = factory.get('/exercises/find/?language=1&title=Nikt&intro=Nikt&content=Nikt&hint=Nikt&solution=Nikt&operation=or')
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        response = view(request)
        languages = Exercise.objects.all()
        serializer = serializers.ExerciseSerializer(
            languages, many=True)
        self.assertEqual(len(response.data['exercises']), 3)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
# Użytkownicy:
#     3bartocha   1
#     john2       2
#     JanProfesor 3
#
# Uprawnienia autora 3bartocha
#     for_students    1
#     for_all         2
#     for_all_without_solution    3
# Uprawnienia autora JanProfesor
#     for_doktorants_without_solution    4
#     for_radawydzialu_without_solution  5
#     for_all                            6

# 1 Matematyka                   14 Zadań 3bartocha: {6,7,...,19} for_students; dla 9. jest załącznik
#                                1  Zadań 3bartocha:           20 for_all_without_solution
#                                1  Zadanie JanProfesor        21 for_doktorants_without_solution for_allfalse
#                                1  Zadanie JanProfesor        22 for_radawydzialu_without_solution for_allfalse
#                                1  Zadanie JanProfesor        23 NULL

# 9    Algebra
# 10    Matematyka Dyskretna
# 11    Geometria
# 2 Informatyka                     Zadanie 3bartocha: 5 for_students
# 3    Algorytmy                    Zadanie 3bartocha: 4 for_students
# 4    Programowanie                Zadanie 3bartocha: 3 for_students
# 5        ProgramowanieCpp         Zadanie 3bartocha: 2 for_students
# 6            ProgramowanieProc    Zadanie 3bartocha: 1 for_students
# 7            ProgramowanieObiek
# 8        ProgramowanieJava
