from django.test import TestCase #, Client
from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate

from tex_resources.models.exercise import Exercise
from tex_resources.models.attachment import Attachment
from tex_resources import serializers
from tex_resources import views

import os
import code

import base64
from shutil import copyfile

def image_to_string(filename=os.path.join(os.getcwd(), "obrazki/agh.jpg")):
    with open(filename, "rb") as image_file:
        encoded_string = base64.b64encode(
            image_file.read())
    return encoded_string


class AttachmentVieDestroywTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def getPath(self, number):
        return os.path.join(os.getcwd(), "pic_folder/agh{0}.jpg").format(number)
    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        for index in range(9):
            copyfile(os.path.join(os.getcwd(), "obrazki/agh.jpg"),
                    os.path.join(os.getcwd(), "pic_folder/agh{0}.jpg").format(index+1))
    def tearDown(self):
        print("E---------end test----------")

    def test_destroy_attachments(self):
        # client = Client()
        factory = APIRequestFactory()
        request = factory.delete("exercise/20/attachments/",
                                 format='json')
        view = views.AttachmentView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        attachment_num_before = Attachment.objects.count()
        ex_attachment_num_before = len(
            Exercise.objects.get(id=20).attachments.all())
        for number in [4, 5, 6, 7]:
            self.assertEqual(os.path.exists(self.getPath(number)), True)
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        ex_attachment_num_after = len(
            Exercise.objects.get(id=20).attachments.all())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(attachment_num_after,
                         attachment_num_before-4)
        self.assertEqual(ex_attachment_num_after,
                         0)
        for number in [4, 5, 6, 7]:
            self.assertEqual(os.path.exists(self.getPath(number)), False)

    def test_destroy_attachment(self):
        # client = Client()
        factory = APIRequestFactory()
        request = factory.delete("exercise/20/attachments/",
                                 format='json')
        view = views.AttachmentView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='3bartocha')
        force_authenticate(request, user=user)
        ex_attachment_num_before = len(
            Exercise.objects.get(id=20).attachments.all())
        attachment_num_before = Attachment.objects.count()
        self.assertEqual(os.path.exists(self.getPath(4)), True)
        response = view(request, exercise_pk="20", pk="4")
        attachment_num_after = Attachment.objects.count()
        ex_attachment_num_after = len(
            Exercise.objects.get(id=20).attachments.all())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(attachment_num_after,
                         attachment_num_before-1)
        self.assertEqual(ex_attachment_num_after,
                         ex_attachment_num_before-1)
        self.assertEqual(os.path.exists(self.getPath(4)), False)

class AttachmentViewTestCase(TestCase):
    fixtures = ["many_exercises_without_admin.json", "sample_preambules.json"]

    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        self.path = os.path.join(os.getcwd(), 'pic_folder/agh.jpg')
        for index in range(9):
            copyfile(os.path.join(os.getcwd(), 'pic_folder/agh.jpg'),
                     os.path.join(os.getcwd(), 'pic_folder/agh{0}.jpg').format(index+1))
    def tearDown(self):
        print("E---------end test----------")

    def test_get_attachments_without_one(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'list'})
        request = factory.get('exercise/20/attachments')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        attachment_num_before = Attachment.objects.count()
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        self.assertEqual(attachment_num_after,
                         attachment_num_before)
        self.assertEqual(len(response.data), 3)

    def test_retrieve_attachment_exercise_doesnt_exist(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'retrieve'})
        request = factory.get('exercise/20/attachments/1')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20000", pk="1")
        self.assertEqual(response.data, {'detail': ErrorDetail(string='Nie znaleziono.', code='not_found')})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_attachment_doesnt_exist(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'retrieve'})
        request = factory.get('exercise/20/attachments/1')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="10000")
        self.assertEqual(response.data, {'detail': ErrorDetail(string='Nie znaleziono.', code='not_found')})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_attachment_vis(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'retrieve'})
        request = factory.get('exercise/20/attachments/1')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="4")
        self.assertEqual(response.data['id'], 4)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_attachment_hide(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'retrieve'})
        request = factory.get('exercise/20/attachments/2')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="7")
        self.assertEqual(response.data, "forbidden")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_attachment_for_non_my_exercise(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'post': 'create'})
        data = {
            "name": "Nowy obrazek",
            "width": "0.11",
            "label": "attachmentPK11",
            "caption": "captionPK11",
            "image": "data:image/jpg;base64," +
                     image_to_string().decode(),
            "attachment_type": "exercise_content"
        }
        request = factory.post('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='JanProfesor')
        attachment_num_before = Attachment.objects.count()
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        self.assertEqual(attachment_num_after,
                         attachment_num_before)
        self.assertEqual(response.data, "nie masz uprawnien")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_attachment(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'post': 'create'})
        data = [{
            "name": "Nowy obrazek",
            "width": "0.11",
            "label": "attachmentPK11",
            "caption": "captionPK11",
            "image": "data:image/jpg;base64," +
                     image_to_string().decode(),
            "attachment_type": "exercise_content"
        }]
        request = factory.post('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='3bartocha')
        attachment_num_before = Attachment.objects.count()
        ex_attachment_num_before = len(
            Exercise.objects.get(id=20).attachments.all())
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        ex_attachment_num_after = len(
            Exercise.objects.get(id=20).attachments.all())
        self.assertEqual(attachment_num_after,
                         attachment_num_before+1)
        self.assertEqual(ex_attachment_num_after,
                         ex_attachment_num_before+1)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(response.data[0]['name'], "Nowy obrazek")
        self.assertEqual(response.data[0]['image']
                                      ['base64_string'],
                         image_to_string().decode())

    def test_create_attachments(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'post': 'create'})
        data = [{
                    "name": "Nowy obrazek",
                    "width": "0.11",
                    "label": "attachmentPK11",
                    "caption": "captionPK11",
                    "image": "data:image/jpg;base64," +
                             image_to_string().decode(),
                    "attachment_type": "exercise_content"
                },
                {
                    "name": "Nowy obrazek2",
                    "width": "0.12",
                    "label": "attachmentPK12",
                    "caption": "captionPK12",
                    "image": "data:image/jpg;base64," +
                             image_to_string().decode(),
                    "attachment_type": "exercise_content"
                }]
        request = factory.post('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='3bartocha')
        attachment_num_before = Attachment.objects.count()
        ex_attachment_num_before = len(
            Exercise.objects.get(id=20).attachments.all())
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        ex_attachment_num_after = len(
            Exercise.objects.get(id=20).attachments.all())
        self.assertEqual(attachment_num_after,
                         attachment_num_before+2)
        self.assertEqual(ex_attachment_num_after,
                         ex_attachment_num_before+2)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        # self.assertEqual(response.data['name'], "Nowy obrazek")
        # self.assertEqual(response.data['image']
        #                               ['base64_string'],
        #                  image_to_string().decode())

    def test_update_attachment_for_non_my_exercise(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'put': 'update'})
        data = {
            "name": "Update obrazka",
            "width": "0.44",
            "label": "attachmentUpdatePK4",
            "caption": "captionUpdatePK4",
            "image": "data:image/jpg;base64," +
                     image_to_string().decode(),
            "attachment_type": "exercise_intro"
        }
        request = factory.put('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='JanProfesor')
        attachment_num_before = Attachment.objects.count()
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="4")
        attachment_num_after = Attachment.objects.count()
        self.assertEqual(attachment_num_after,
                         attachment_num_before)
        self.assertEqual(response.data, "nie masz uprawnien")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_attachment(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'put': 'update'})
        data = {
            "name": "Update obrazka",
            "width": "0.44",
            "label": "attachmentUpdatePK4",
            "caption": "captionUpdatePK4",
            "image": "data:image/jpg;base64," +
                     image_to_string().decode(),
            "attachment_type": "exercise_intro"
        }
        request = factory.put('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='3bartocha')
        attachment_num_before = Attachment.objects.count()
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="4")
        attachment_num_after = Attachment.objects.count()
        self.assertEqual(attachment_num_after,
                         attachment_num_before)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(response.data['name'], "Update obrazka")
        self.assertEqual(response.data['label'], "attachmentUpdatePK4")
        self.assertEqual(response.data['image']
                                      ['base64_string'],
                         image_to_string().decode())

    def test_destroy_attachment_not_my(self):
        # client = Client()
        factory = APIRequestFactory()
        request = factory.delete("exercise/20/attachments/",
                                 format='json')
        view = views.AttachmentView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        ex_attachment_num_before = len(
            Exercise.objects.get(id=20).attachments.all())
        attachment_num_before = Attachment.objects.count()
        self.assertEqual(os.path.exists(self.path), True)
        response = view(request, exercise_pk="20", pk="4")
        attachment_num_after = Attachment.objects.count()
        ex_attachment_num_after = len(
            Exercise.objects.get(id=20).attachments.all())
        self.assertEqual(response.status_code,
                         status.HTTP_403_FORBIDDEN)
        self.assertEqual(attachment_num_after,
                         attachment_num_before)
        self.assertEqual(ex_attachment_num_after,
                         ex_attachment_num_before)
        self.assertEqual(os.path.exists(self.path), True)


class AdminAttachmentViewTestCase(TestCase):
    fixtures = ["many_exercisesJanProfesorAdmin.json", "sample_preambules.json"]

    def getPath(self, number):
        return os.path.join(os.getcwd(), "pic_folder/agh{0}.jpg").format(number)
    def setUp(self):
        print("B---------{0}----------".format(
            self._testMethodName))
        self.path = os.path.join(os.getcwd(), 'pic_folder/agh.jpg')
        for index in range(9):
            copyfile(os.path.join(os.getcwd(), 'pic_folder/agh.jpg'),
                     os.path.join(os.getcwd(), 'pic_folder/agh{0}.jpg').format(index+1))
    def tearDown(self):
        print("E---------end test----------")

    def test_get_attachments_with_extra_one(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'list'})
        request = factory.get('exercise/20/attachments')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        attachment_num_before = Attachment.objects.count()
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        self.assertEqual(attachment_num_after,
                         attachment_num_before)
        self.assertEqual(len(response.data), 4)

    def test_retrieve_attachment_exercise_doesnt_exist(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'retrieve'})
        request = factory.get('exercise/20/attachments/1')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20000", pk="1")
        self.assertEqual(response.data, {'detail': ErrorDetail(string='Nie znaleziono.', code='not_found')})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_attachment_doesnt_exist(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'retrieve'})
        request = factory.get('exercise/20/attachments/1')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="10000")
        self.assertEqual(response.data, {'detail': ErrorDetail(string='Nie znaleziono.', code='not_found')})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_retrieve_attachment_vis(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'retrieve'})
        request = factory.get('exercise/20/attachments/1')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="4")
        self.assertEqual(response.data['id'], 4)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_attachment_hide(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'get': 'retrieve'})
        request = factory.get('exercise/20/attachments/2')
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="7")
        self.assertEqual(response.data['id'], 7)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_attachment_for_non_my_exercise(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'post': 'create'})
        data = [{
            "name": "Nowy obrazek",
            "width": "0.11",
            "label": "attachmentPK11",
            "caption": "captionPK11",
            "image": "data:image/jpg;base64," +
                     image_to_string().decode(),
            "attachment_type": "exercise_content"
        }]
        request = factory.post('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='JanProfesor')
        attachment_num_before = Attachment.objects.count()
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        # code.interact(local=locals())
        self.assertEqual(attachment_num_after,
                         attachment_num_before+1)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(response.data[0]['name'], "Nowy obrazek")
        self.assertEqual(response.data[0]['image']
                                      ['base64_string'],
                         image_to_string().decode())

    def test_create_attachment(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'post': 'create'})
        data = [{
            "name": "Nowy obrazek",
            "width": "0.11",
            "label": "attachmentPK11",
            "caption": "captionPK11",
            "image": "data:image/jpg;base64," +
                     image_to_string().decode(),
            "attachment_type": "exercise_content"
        }]
        request = factory.post('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='3bartocha')
        attachment_num_before = Attachment.objects.count()
        ex_attachment_num_before = len(
            Exercise.objects.get(id=20).attachments.all())
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        ex_attachment_num_after = len(
            Exercise.objects.get(id=20).attachments.all())
        self.assertEqual(attachment_num_after,
                         attachment_num_before+1)
        self.assertEqual(ex_attachment_num_after,
                         ex_attachment_num_before+1)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        self.assertEqual(response.data[0]['name'], "Nowy obrazek")
        self.assertEqual(response.data[0]['image']
                                      ['base64_string'],
                         image_to_string().decode())

    def test_create_attachments(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'post': 'create'})
        data = [{
                    "name": "Nowy obrazek",
                    "width": "0.11",
                    "label": "attachmentPK11",
                    "caption": "captionPK11",
                    "image": "data:image/jpg;base64," +
                             image_to_string().decode(),
                    "attachment_type": "exercise_content"
                },
                {
                    "name": "Nowy obrazek2",
                    "width": "0.12",
                    "label": "attachmentPK12",
                    "caption": "captionPK12",
                    "image": "data:image/jpg;base64," +
                             image_to_string().decode(),
                    "attachment_type": "exercise_content"
                }]
        request = factory.post('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='3bartocha')
        attachment_num_before = Attachment.objects.count()
        ex_attachment_num_before = len(
            Exercise.objects.get(id=20).attachments.all())
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20")
        attachment_num_after = Attachment.objects.count()
        ex_attachment_num_after = len(
            Exercise.objects.get(id=20).attachments.all())
        self.assertEqual(attachment_num_after,
                         attachment_num_before+2)
        self.assertEqual(ex_attachment_num_after,
                         ex_attachment_num_before+2)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED)
        # self.assertEqual(response.data['name'], "Nowy obrazek")
        # self.assertEqual(response.data['image']
        #                               ['base64_string'],
        #                  image_to_string().decode())

    def test_update_attachment_for_non_my_exercise(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'put': 'update'})
        data = {
            "name": "Update obrazka",
            "width": "0.44",
            "label": "attachmentUpdatePK4",
            "caption": "captionUpdatePK4",
            "image": "data:image/jpg;base64," +
                     image_to_string().decode(),
            "attachment_type": "exercise_intro"
        }
        request = factory.put('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='JanProfesor')
        attachment_num_before = Attachment.objects.count()
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="4")
        attachment_num_after = Attachment.objects.count()
        self.assertEqual(attachment_num_after,
                         attachment_num_before)
        self.assertEqual(response.data['name'], "Update obrazka")
        self.assertEqual(response.data['label'], "attachmentUpdatePK4")
        self.assertEqual(response.data['image']
                                      ['base64_string'],
                         image_to_string().decode())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_attachment(self):
        factory = APIRequestFactory()
        view = views.AttachmentView.as_view({'put': 'update'})
        data = {
            "name": "Update obrazka",
            "width": "0.44",
            "label": "attachmentUpdatePK4",
            "caption": "captionUpdatePK4",
            "image": "data:image/jpg;base64," +
                     image_to_string().decode(),
            "attachment_type": "exercise_intro"
        }
        request = factory.put('exercise/20/attachments/', data, format='json')
        user = User.objects.get(username='3bartocha')
        attachment_num_before = Attachment.objects.count()
        force_authenticate(request, user=user)
        response = view(request, exercise_pk="20", pk="4")
        attachment_num_after = Attachment.objects.count()
        self.assertEqual(attachment_num_after,
                         attachment_num_before)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(response.data['name'], "Update obrazka")
        self.assertEqual(response.data['label'], "attachmentUpdatePK4")
        self.assertEqual(response.data['image']
                                      ['base64_string'],
                         image_to_string().decode())

    def test_destroy_attachment_not_my(self):
        # client = Client()
        factory = APIRequestFactory()
        request = factory.delete("exercise/20/attachments/",
                                 format='json')
        view = views.AttachmentView.as_view(
            {'delete': 'destroy'})
        # code.interact(local=locals())
        user = User.objects.get(username='JanProfesor')
        force_authenticate(request, user=user)
        ex_attachment_num_before = len(
            Exercise.objects.get(id=20).attachments.all())
        attachment_num_before = Attachment.objects.count()
        self.assertEqual(os.path.exists(self.getPath(4)), True)
        response = view(request, exercise_pk="20", pk="4")
        attachment_num_after = Attachment.objects.count()
        ex_attachment_num_after = len(
            Exercise.objects.get(id=20).attachments.all())
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(attachment_num_after,
                         attachment_num_before-1)
        self.assertEqual(ex_attachment_num_after,
                         ex_attachment_num_before-1)
        self.assertEqual(os.path.exists(self.getPath(4)), False)
