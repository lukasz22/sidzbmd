from rest_framework import viewsets
from other.models.language import Language
from ..models.exercise import Exercise
from other.models.category import Category
from ..models.attachment import Attachment

from other.serializers.languageserializer import LanguageSerializer
from ..serializers import ExerciseSerializer
from ..serializers import ExerciseContainerSerializer

from ..serializers import AttachmentSerializer
from other.serializers.categoryserializer import CategorySerializer
from other.serializers.categoryserializer import CategoryTreeSerializer
from other.serializers.categoryserializer import CategoryFlatTreeSerializer


# from recipes.serializers import LatexExerciseSerializer
# from recipes.serializers import LatexDocumentSerializer

from recipes.latexlib.latexlib import LatexFile, LatexExercise

from rest_framework.authentication import SessionAuthentication
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework import status
from datetime import datetime
from accesses.permission import IsOwnerOrReadOnly, IsOwner
from accesses.permission import IsOwnerOrHavePolicyAccess, IsAdministrator
import os
import subprocess

import code
from functools import reduce
from django.db.models import Q

from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
import operator
from django.conf import settings

from ..models.attachment import AttachmentTypes

from ..models.lib import filter_exercises

from accesses.models import Userprofile

from ..models.preambule import Preambule

from ..serializers import PreambuleSerializer

class PreambuleView(viewsets.ViewSet):

    def list(self, request):
        queryset = Preambule.objects.all()
        serializer = PreambuleSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk):
        category = get_object_or_404(Preambule, pk=pk)
        serializer = PreambuleSerializer(category)
        return Response(serializer.data) # not list

    def update(self, request, pk, format='json'):
        if not IsAdministrator.has_permission(request):
            return Response("Nie posiadasz uprawnień do aktualizowania preambuły. Skontaktuj się z administratorem.",
                            status=status.HTTP_403_FORBIDDEN)
        # exercise = Preambule.objects.get(pk=pk)
        # code.interact(local=locals())
        exercise = get_object_or_404(Preambule, pk=pk)
        serializer = PreambuleSerializer(
            exercise,
            data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_200_OK)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)
