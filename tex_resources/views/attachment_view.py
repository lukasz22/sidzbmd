from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework import viewsets

from ..models.exercise import Exercise
from ..models.attachment import Attachment
from ..serializers import AttachmentSerializer
from ..models.lib import filter_exercises

from accesses.permission import IsOwner
from accesses.permission import IsOwnerOrHavePolicyAccess
from accesses.permission import IsAdministrator

from rest_framework import status
from datetime import datetime

import code


class AttachmentView(viewsets.ViewSet):

    def list(self, request, exercise_pk):
        queryset = get_object_or_404(Exercise, pk=exercise_pk)
        # code.interact(local=locals())
        mod_queryset = [
            exer for exer in filter_exercises(request.user,
                                              [queryset])]
        if len(mod_queryset) != 1:
            raise Exception("OJOJ")
        serializer = AttachmentSerializer(
            mod_queryset[0].attachments.all(), many=True)
        return Response(serializer.data)

    def retrieve(self, request, exercise_pk, pk):
        queryset = get_object_or_404(Exercise, pk=exercise_pk)
        obj = get_object_or_404(queryset.attachments, pk=pk)

        mod_queryset = [
            exer for exer in filter_exercises(request.user,
                                              [queryset])]
        if len(mod_queryset) != 1:
            raise Exception("OJOJ")
        try:
            serializer = AttachmentSerializer(
                mod_queryset[0].attachments.get(id=pk))
        except Attachment.DoesNotExist:
            return Response("forbidden", status=status.HTTP_403_FORBIDDEN)
        return Response(serializer.data) # not list

    def create(self, request, exercise_pk):
        queryset = get_object_or_404(Exercise, pk=exercise_pk)
        if IsOwnerOrHavePolicyAccess.has_object_permission(
                request, queryset) or IsAdministrator.has_permission(request):
            if not request.POST._mutable:
                request.POST._mutable = True
            # code.interact(local=locals())
            if isinstance(request.data, list):
                for index in range(len(request.data)):
                    request.data[index]['exercise'] = exercise_pk
            else:
                request.data['exercise'] = exercise_pk
            serializer = AttachmentSerializer(
                data=request.data, many=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    def update(self, request, exercise_pk, pk):
        queryset = get_object_or_404(Exercise, pk=exercise_pk)
        if IsOwnerOrHavePolicyAccess.has_object_permission(
                request, queryset) or IsAdministrator.has_permission(request):
            if not request.POST._mutable:
                request.POST._mutable = True
            request.data['exercise'] = exercise_pk
            attachment = get_object_or_404(queryset.attachments, pk=pk)
            serializer = AttachmentSerializer(
                attachment, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data,
                                status=status.HTTP_200_OK)
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, exercise_pk, pk=None):
        queryset = get_object_or_404(Exercise, pk=exercise_pk)
        if IsOwnerOrHavePolicyAccess.has_object_permission(
                request, queryset) or IsAdministrator.has_permission(request):
            if pk is None:
                ans = Attachment.objects.filter(
                    Q(exercise=queryset)).delete()
                return Response(ans, status=status.HTTP_200_OK)
            else:
                obj = get_object_or_404(Attachment, pk=pk)
                ans = Attachment.objects.filter(
                    Q(pk=pk) & Q(exercise=queryset)).delete()
                return Response(ans, status=status.HTTP_200_OK)
        else:
            return Response(data='nie masz uprawnien',
                            status=status.HTTP_403_FORBIDDEN)
