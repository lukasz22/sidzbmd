from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action

from ..models.exercise import Exercise
from other.models.category import Category
from ..serializers import ExerciseSerializer
from ..serializers import ExerciseContainerSerializer
from ..models.lib import filter_exercises
from accesses.models import Userprofile
from django.contrib.auth.models import User


from accesses.permission import IsOwner
from accesses.permission import IsAdministrator

from rest_framework import status
from datetime import datetime

from functools import reduce
import operator
import code


class ExerciseView(viewsets.ViewSet):

    def get_range(self, request, queryset):
        page_number = int(request.GET.get('page', 1))
        page_size = int(request.GET.get('pagesize',
                                        len(queryset)))
        first = (page_number-1)*page_size
        last = page_number*page_size
        return queryset[first:last]

    def get_subcategories_id(self, obj, it, result=[]):
        if it is obj.id:
            result = []
        result.append(obj.id)
        children = obj.children.filter(
            parent=obj.id).order_by('category_name')
        for child in children:
            for out in self.get_subcategories_id(child,
                                                 it, []):
                result.extend(out)
        yield result

    def list(self, request):
        queryset = Exercise.objects.all().order_by('title')

        mod_queryset = [
            exer for exer in filter_exercises(request.user,
                                              queryset)]

        page_number = request.GET.get('page', 1)
        page_size = request.GET.get('pagesize',
                                    len(mod_queryset))
        # serializer = ExerciseSerializer(
        #     self.get_range(request, mod_queryset),
        #     many=True)

        serializer = ExerciseSerializer(
            self.get_range(request, mod_queryset),
            many=True)
        serializer_wrapper = ExerciseContainerSerializer(
            {
             'total': len(mod_queryset),
             'exercises': serializer.data})
        return Response(serializer_wrapper.data)

    def retrieve(self, request, pk):
        queryset = get_object_or_404(Exercise, pk=pk)
        mod_queryset = [
            exer for exer in filter_exercises(request.user,
                                                   [queryset])]
        # code.interact(local=locals())
        if mod_queryset:
            serializer = ExerciseSerializer(mod_queryset,
                                            many=True)
            return Response(serializer.data[0]) # is list
        elif queryset:
            return Response(status=status.HTTP_403_FORBIDDEN)

    def create(self, request):
        if not request.POST._mutable:
            request.POST._mutable = True

        request.data['creation_date'] = datetime.now()
        request.data['modified_date'] = datetime.now()


        #if not str(Userprofile.objects.get(user=request.user).id) == request.data['owner']:
        #    if not str(Userprofile.objects.get(user=request.user).id) == str(Userprofile.objects.get(user__username=request.data['owner']).id):
        #        return Response(data='is not you exercise',
        #                        status=status.HTTP_403_FORBIDDEN)

        try:
            request = self.update_owner_in_request(request)
        except User.DoesNotExist as e:
             return Response({"owner":["Podany login użytkownika nie istnieje "
                             "lub użytkownik nie zalogował się ani razu do serwisu."]},
                             status=status.HTTP_400_BAD_REQUEST)
        # mykwargs = {"request": request},
        serializer = ExerciseSerializer(data=request.data, request=request)
        if serializer.is_valid():
            # if not IsOwner.has_object_permission(request,
            #                                      serializer.data):
            #     return Response(data='is not you exercise',
            #                     status=status.HTTP_403_FORBIDDEN)
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_201_CREATED)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk, format='json'):
        exercise = get_object_or_404(Exercise, pk=pk)
        if not IsOwner.has_object_permission(request,
                                             exercise) and not IsAdministrator.has_permission(request):
            return Response(data='is not you exercise',
                            status=status.HTTP_403_FORBIDDEN)
        if not request.POST._mutable:
            request.POST._mutable = True
        request.data['creation_date'] = exercise.creation_date
        request.data['modified_date'] = datetime.now()
        try:
            request = self.update_owner_in_request(request)
        except User.DoesNotExist as e:
             return Response({"owner":["Podany login użytkownika nie istnieje "
                             "lub użytkownik nie zalogował się ani razu do serwisu."]},
                             status=status.HTTP_400_BAD_REQUEST)
        # mykwargs = {"request": request}
        serializer = ExerciseSerializer(exercise,
                                        data=request.data,  request=request)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status=status.HTTP_200_OK)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        if pk is None:
            # code.interact(local=locals())
            if IsAdministrator.has_permission(request):
                obj = Exercise.objects.all().delete()
            else:
                obj = Exercise.objects.filter(owner=request.user.id).delete()
            return Response(obj, status=status.HTTP_200_OK)
        else:
            obj = get_object_or_404(Exercise, pk=pk)
            if IsOwner.has_object_permission(request, obj) or IsAdministrator.has_permission(request):
                ans = Exercise.objects.filter(pk=pk).delete()
                return Response(ans, status=status.HTTP_200_OK)
            return Response(data='is not you exercise',
                            status=status.HTTP_403_FORBIDDEN)

    @action(methods=['get'], detail=False)
    def find(self, request):
        queryset = Exercise.objects.all()
        ex_id_list = request.GET.getlist('id')
        # code.interact(local=locals())
        operation = request.GET.get('operation', 'and')
        title = request.GET.get('title', '')#
        intro = request.GET.get('intro', '')#
        task_content = request.GET.get('task-content', '')#
        hint = request.GET.get('hint', '')#
        solution = request.GET.get('solution', '')#
        owner = request.GET.get('owner', '')#
        category_id = request.GET.get('category', '')
        language = request.GET.get('language', '')
        if operation == 'and':
            def funkcja(x, y):
                return operator.and_(x, y)
        elif operation == 'or':
            def funkcja(x, y):
                return operator.or_(x, y)
        else:
            # code.interact(local=locals())
            raise Exception
        q_list = [Q(title__icontains=title),
                  Q(intro__icontains=intro),
                  Q(task_content__icontains=task_content),
                  Q(hint__icontains=hint),
                  Q(solution__icontains=solution),
                  Q(owner__username__icontains=owner)]
                  # Q(language__id=language)]
        q_list_no_empty = [
            q_el for q_el in q_list if q_el.children[0][1] != '']
        # code.interact(local=locals())
        q_reduce = reduce(funkcja,
                          q_list_no_empty) if q_list_no_empty else Q()

        if category_id and not category_id.isdigit():
            return Response(
                "category has to be a digit, not string",
                status=status.HTTP_400_BAD_REQUEST)
        if category_id:
            root_category = Category.objects.get(
                id=category_id)
            sub_cats_gen = self.get_subcategories_id(
                root_category, root_category.id)
            q_list_or = [
                Q(category__id=id)
                for id in next(sub_cats_gen)]
            q_list_or_no_empty = [
                q_el
                for q_el in q_list_or
                if q_el.children[0][1]]

            q_or_reduce = reduce(
                (lambda x, y: operator.or_(x, y)),
                q_list_or_no_empty)
        else:
            q_or_reduce = Q()
        if language:
            q_language_no_empty = [
                q_el
                for q_el in [Q(language__id=language)]
                if q_el.children[0][1]]
            q_language_reduce = reduce(
                (lambda x, y: operator.or_(x, y)),
                q_language_no_empty)
        else:
            q_language_reduce = Q()

        if ex_id_list:
            q_ex_id_no_empty = [
                q_el
                for q_el in [Q(id=pk) for pk in ex_id_list]
                if q_el.children[0][1]]
            q_ex_id_reduce = reduce(
                (lambda x, y: operator.or_(x, y)),
                q_ex_id_no_empty)
        else:
            q_ex_id_reduce = Q()
        # any_list = [len(q_param.__dict__['children']) for q_param in [q_reduce,
        #                                                               q_or_reduce,
        #                                                               q_language_reduce,
        #                                                               q_ex_id_reduce]]

        if len(request.GET) == 0:
            serializer_wrapper = ExerciseContainerSerializer(
                {
                 'total': 0,
                 'exercises': []})
            response = Response(serializer_wrapper.data)
            return response
        mod_queryset = queryset.filter(
            q_reduce & q_or_reduce & q_language_reduce & q_ex_id_reduce).order_by('title')

        mod_mod_queryset = [
            exer
            for exer in filter_exercises(request.user,
                                         mod_queryset)]
        serializer = ExerciseSerializer(
            self.get_range(request, mod_mod_queryset),
            many=True)
        serializer_wrapper = ExerciseContainerSerializer(
            {
             'total': len(mod_mod_queryset),
             'exercises': serializer.data})
        # code.interact(local=locals())
        # code.interact(local=locals())
        response = Response(serializer_wrapper.data)
        return response

    def update_owner_in_request(self, request):
        # code.interact(local=locals())
        if request.user.userprofile.is_administrator:
            owner_username = request.data.get('owner', request.user.username)
        else:
            owner_username = request.user.username
        request.data['owner'] = str(User.objects.get(username=owner_username).id)
        return request
