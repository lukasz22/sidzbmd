
rm -rf db.sqlite3
rm -rf ./tex_resources/migrations
rm -rf ./recipes/migrations
python manage.py flush --no-input
echo 'drop owned by lukasz;' | python manage.py dbshell
python manage.py makemigrations tex_resources
python manage.py makemigrations accesses
python manage.py makemigrations auth
python manage.py makemigrations recipes
python manage.py migrate --run-syncdb
python manage.py loaddata init_policy.json
python manage.py loaddata init_exercise_vis_perm.json
python manage.py loaddata init_3bartocha.json
python manage.py loaddata init_category.json
python manage.py loaddata init_language.json
python manage.py loaddata sample_preambules.json
python manage.py loaddata db_tex_resources.json
python manage.py loaddata db_recipes.json
# python manage.py dbshell
# \dt pokazuje tabele
# \d+ tex_resources_category
# python -m trace --count -C ./trace_dir/ manage.py test recipes.tests.DocumentGeneratorViewTestCase.test_example

# drop owned by lukasz;

# ssh -N -f -L 9999:orion.fis.agh.edu.pl:636 3bartocha@taurus.fis.agh.edu.pl
# lista ssh tunnels
# ps aux | grep ssh

# 1) gdy podajemy kategorie A, to ma wyszukiwać także dla podkategorii

# aplikacja która wyświetlałaby tekst dla danej piiosenki
# tail -f /var/log/apache2/access.log
# /etc/apache2/sites-available/default
# http://91.188.125.140/sidzbmd/client/

# ssh root@91.188.125.140 -p 57185

# npm WARN deprecated minimatch@2.0.10: Please update to minimatch 3.0.2 or higher to avoid a RegExp DoS issue
# npm WARN deprecated minimatch@0.3.0: Please update to minimatch 3.0.2 or higher to avoid a RegExp DoS issue
# npm WARN deprecated minimatch@1.0.0: Please update to minimatch 3.0.2 or higher to avoid a RegExp DoS issue
# npm WARN deprecated graceful-fs@3.0.11: please upgrade to graceful-fs 4 for compatibility with current and future versions of Node.js
# npm WARN deprecated node-uuid@1.4.8: Use uuid module instead
# npm WARN deprecated hoek@0.9.1: The major version is no longer supported. Please update to 4.x or newer
# npm WARN deprecated babel-preset-es2015@6.24.1: 🙌  Thanks for using Babel: we recommend using babel-preset-env now: please read babeljs.io/env to update!
# npm WARN deprecated cross-spawn-async@2.2.5: cross-spawn no longer requires a build toolchain, use it instead
# loadDep:yargs → resolveWi ▀ ╢██████████████████████████████████████░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░╟
# npm WARN prefer global jshint@2.5.11 should be installed with -g
# npm WARN prefer global node-gyp@3.8.0 should be installed with -g
#
# > node-sass@4.11.0 install /home/lukasz/Projekty/sidzbmd/frontend/node_modules/node-sass
# > node scripts/install.js
#
# Downloading binary from https://github.com/sass/node-sass/releases/download/v4.11.0/linux-x64-57_binding.node
# Download complete..] - :
# Binary saved to /home/lukasz/Projekty/sidzbmd/frontend/node_modules/node-sass/vendor/linux-x64-57/binding.node
# Caching binary to /home/lukasz/.npm/node-sass/4.11.0/linux-x64-57_binding.node
#
# > node-sass@4.11.0 postinstall /home/lukasz/Projekty/sidzbmd/frontend/node_modules/node-sass
# > node scripts/build.js
#
# Binary found at /home/lukasz/Projekty/sidzbmd/frontend/node_modules/node-sass/vendor/linux-x64-57/binding.node
# Testing binary
# Binary is fine
# sidzbmd@1.0.0 /home/lukasz/Projekty/sidzbmd/frontend
# ├── angular@1.7.7
# ├── angular-animate@1.7.7
# ├── angular-bootstrap-toggle@0.1.2
# ├── angular-highlightjs@0.7.1
# ├── angular-route@1.7.7
# ├── angular-sanitize@1.7.7
# ├── angular-sessionstorage@1.1.5
# ├── angular-touch@1.7.7
# ├── angular-ui-bootstrap@2.5.6
# ├── angular-ui-tree@2.22.6
# ├─┬ babel-core@6.26.3
# │ ├─┬ babel-code-frame@6.26.0
# │ │ └── js-tokens@3.0.2
# │ ├─┬ babel-generator@6.26.1
# │ │ ├─┬ detect-indent@4.0.0
# │ │ │ └─┬ repeating@2.0.1
# │ │ │   └── is-finite@1.0.2
# │ │ ├── jsesc@1.3.0
# │ │ ├── source-map@0.5.7
# │ │ └── trim-right@1.0.1
# │ ├── babel-helpers@6.24.1
# │ ├── babel-messages@6.23.0
# │ ├─┬ babel-register@6.26.0
# │ │ ├── core-js@2.6.5
# │ │ ├─┬ home-or-tmp@2.0.0
# │ │ │ ├── os-homedir@1.0.2
# │ │ │ └── os-tmpdir@1.0.2
# │ │ └─┬ source-map-support@0.4.18
# │ │   └── source-map@0.5.7
# │ ├─┬ babel-runtime@6.26.0
# │ │ └── regenerator-runtime@0.11.1
# │ ├── babel-template@6.26.0
# │ ├─┬ babel-traverse@6.26.0
# │ │ ├── debug@2.6.9
# │ │ ├── globals@9.18.0
# │ │ └─┬ invariant@2.2.4
# │ │   └── loose-envify@1.4.0
# │ ├─┬ babel-types@6.26.0
# │ │ └── to-fast-properties@1.0.3
# │ ├── babylon@6.18.0
# │ ├─┬ convert-source-map@1.6.0
# │ │ └── safe-buffer@5.1.2
# │ ├─┬ debug@2.6.9
# │ │ └── ms@2.0.0
# │ ├── json5@0.5.1
# │ ├── lodash@4.17.11
# │ ├─┬ minimatch@3.0.4
# │ │ └─┬ brace-expansion@1.1.11
# │ │   ├── balanced-match@1.0.0
# │ │   └── concat-map@0.0.1
# │ ├── path-is-absolute@1.0.1
# │ ├── private@0.1.8
# │ ├── slash@1.0.0
# │ └── source-map@0.5.7
# ├─┬ babel-loader@7.1.5
# │ ├─┬ find-cache-dir@1.0.0
# │ │ ├── commondir@1.0.1
# │ │ ├── make-dir@1.3.0
# │ │ └─┬ pkg-dir@2.0.0
# │ │   └─┬ find-up@2.1.0
# │ │     └─┬ locate-path@2.0.0
# │ │       └─┬ p-locate@2.0.0
# │ │         └─┬ p-limit@1.3.0
# │ │           └── p-try@1.0.0
# │ ├─┬ loader-utils@1.2.3
# │ │ ├── big.js@5.2.2
# │ │ ├── emojis-list@2.1.0
# │ │ └─┬ json5@1.0.1
# │ │   └── minimist@1.2.0
# │ └─┬ mkdirp@0.5.1
# │   └── minimist@0.0.8
# ├─┬ babel-plugin-angularjs-annotate@0.10.0
# │ ├─┬ @babel/code-frame@7.0.0
# │ │ └─┬ @babel/highlight@7.0.0
# │ │   ├─┬ chalk@2.4.2
# │ │   │ ├── ansi-styles@3.2.1
# │ │   │ └── supports-color@5.5.0
# │ │   └── js-tokens@4.0.0
# │ ├─┬ @babel/types@7.3.4
# │ │ └── to-fast-properties@2.0.0
# │ └── simple-is@0.2.0
# ├─┬ babel-preset-es2015@6.24.1
# │ ├── babel-plugin-check-es2015-constants@6.22.0
# │ ├── babel-plugin-transform-es2015-arrow-functions@6.22.0
# │ ├── babel-plugin-transform-es2015-block-scoped-functions@6.22.0
# │ ├── babel-plugin-transform-es2015-block-scoping@6.26.0
# │ ├─┬ babel-plugin-transform-es2015-classes@6.24.1
# │ │ ├── babel-helper-define-map@6.26.0
# │ │ ├── babel-helper-function-name@6.24.1
# │ │ ├── babel-helper-optimise-call-expression@6.24.1
# │ │ └── babel-helper-replace-supers@6.24.1
# │ ├── babel-plugin-transform-es2015-computed-properties@6.24.1
# │ ├── babel-plugin-transform-es2015-destructuring@6.23.0
# │ ├── babel-plugin-transform-es2015-duplicate-keys@6.24.1
# │ ├── babel-plugin-transform-es2015-for-of@6.23.0
# │ ├── babel-plugin-transform-es2015-function-name@6.24.1
# │ ├── babel-plugin-transform-es2015-literals@6.22.0
# │ ├── babel-plugin-transform-es2015-modules-amd@6.24.1
# │ ├─┬ babel-plugin-transform-es2015-modules-commonjs@6.26.2
# │ │ └── babel-plugin-transform-strict-mode@6.24.1
# │ ├─┬ babel-plugin-transform-es2015-modules-systemjs@6.24.1
# │ │ └── babel-helper-hoist-variables@6.24.1
# │ ├── babel-plugin-transform-es2015-modules-umd@6.24.1
# │ ├── babel-plugin-transform-es2015-object-super@6.24.1
# │ ├─┬ babel-plugin-transform-es2015-parameters@6.24.1
# │ │ ├── babel-helper-call-delegate@6.24.1
# │ │ └── babel-helper-get-function-arity@6.24.1
# │ ├── babel-plugin-transform-es2015-shorthand-properties@6.24.1
# │ ├── babel-plugin-transform-es2015-spread@6.22.0
# │ ├─┬ babel-plugin-transform-es2015-sticky-regex@6.24.1
# │ │ └── babel-helper-regex@6.26.0
# │ ├── babel-plugin-transform-es2015-template-literals@6.22.0
# │ ├── babel-plugin-transform-es2015-typeof-symbol@6.23.0
# │ ├─┬ babel-plugin-transform-es2015-unicode-regex@6.24.1
# │ │ └─┬ regexpu-core@2.0.0
# │ │   ├── regenerate@1.4.0
# │ │   ├── regjsgen@0.2.0
# │ │   └─┬ regjsparser@0.1.5
# │ │     └── jsesc@0.5.0
# │ └─┬ babel-plugin-transform-regenerator@6.26.0
# │   └── regenerator-transform@0.10.1
# ├── bootstrap@3.4.1
# ├─┬ clean-webpack-plugin@1.0.1
# │ └── rimraf@2.6.3
# ├─┬ css-loader@2.1.0
# │ ├── icss-utils@4.1.0
# │ ├─┬ loader-utils@1.2.3
# │ │ ├── big.js@5.2.2
# │ │ └─┬ json5@1.0.1
# │ │   └── minimist@1.2.0
# │ ├─┬ postcss@7.0.14
# │ │ ├─┬ chalk@2.4.2
# │ │ │ ├── ansi-styles@3.2.1
# │ │ │ └── supports-color@5.5.0
# │ │ ├── source-map@0.6.1
# │ │ └── supports-color@6.1.0
# │ ├── postcss-modules-extract-imports@2.0.0
# │ ├─┬ postcss-modules-local-by-default@2.0.5
# │ │ └─┬ css-selector-tokenizer@0.7.1
# │ │   ├── cssesc@0.1.0
# │ │   └── regexpu-core@1.0.0
# │ ├── postcss-modules-scope@2.0.1
# │ ├─┬ postcss-modules-values@2.0.0
# │ │ └── icss-replace-symbols@1.1.0
# │ ├── postcss-value-parser@3.3.1
# │ └─┬ schema-utils@1.0.0
# │   └── ajv-errors@1.0.1
# ├─┬ eslint@5.14.1
# │ ├─┬ ajv@6.9.2
# │ │ ├── fast-deep-equal@2.0.1
# │ │ ├── fast-json-stable-stringify@2.0.0
# │ │ ├── json-schema-traverse@0.4.1
# │ │ └─┬ uri-js@4.2.2
# │ │   └── punycode@2.1.1
# │ ├─┬ chalk@2.4.2
# │ │ ├─┬ ansi-styles@3.2.1
# │ │ │ └─┬ color-convert@1.9.3
# │ │ │   └── color-name@1.1.3
# │ │ ├── escape-string-regexp@1.0.5
# │ │ └── supports-color@5.5.0
# │ ├─┬ cross-spawn@6.0.5
# │ │ ├── nice-try@1.0.5
# │ │ ├── path-key@2.0.1
# │ │ ├─┬ shebang-command@1.2.0
# │ │ │ └── shebang-regex@1.0.0
# │ │ └─┬ which@1.3.1
# │ │   └── isexe@2.0.0
# │ ├─┬ debug@4.1.1
# │ │ └── ms@2.1.1
# │ ├── doctrine@3.0.0
# │ ├─┬ eslint-scope@4.0.0
# │ │ ├── esrecurse@4.2.1
# │ │ └── estraverse@4.2.0
# │ ├── eslint-utils@1.3.1
# │ ├── eslint-visitor-keys@1.0.0
# │ ├─┬ espree@5.0.1
# │ │ ├── UNMET PEER DEPENDENCY acorn@6.1.0
# │ │ └── acorn-jsx@5.0.1
# │ ├── esquery@1.0.1
# │ ├── esutils@2.0.2
# │ ├─┬ file-entry-cache@5.0.1
# │ │ └─┬ flat-cache@2.0.1
# │ │   ├── flatted@2.0.0
# │ │   └── write@1.0.3
# │ ├── functional-red-black-tree@1.0.1
# │ ├─┬ glob@7.1.3
# │ │ ├── fs.realpath@1.0.0
# │ │ ├─┬ inflight@1.0.6
# │ │ │ └── wrappy@1.0.2
# │ │ ├── inherits@2.0.3
# │ │ └── once@1.4.0
# │ ├── globals@11.11.0
# │ ├── ignore@4.0.6
# │ ├─┬ import-fresh@3.0.0
# │ │ ├─┬ parent-module@1.0.0
# │ │ │ └── callsites@3.0.0
# │ │ └── resolve-from@4.0.0
# │ ├── imurmurhash@0.1.4
# │ ├─┬ inquirer@6.2.2
# │ │ ├── ansi-escapes@3.2.0
# │ │ ├─┬ chalk@2.4.2
# │ │ │ ├── ansi-styles@3.2.1
# │ │ │ └── supports-color@5.5.0
# │ │ ├─┬ cli-cursor@2.1.0
# │ │ │ └─┬ restore-cursor@2.0.0
# │ │ │   └── onetime@2.0.1
# │ │ ├── cli-width@2.2.0
# │ │ ├─┬ external-editor@3.0.3
# │ │ │ ├── chardet@0.7.0
# │ │ │ └── tmp@0.0.33
# │ │ ├── figures@2.0.0
# │ │ ├── mute-stream@0.0.7
# │ │ ├─┬ run-async@2.3.0
# │ │ │ └── is-promise@2.1.0
# │ │ ├── rxjs@6.4.0
# │ │ ├─┬ string-width@2.1.1
# │ │ │ ├── is-fullwidth-code-point@2.0.0
# │ │ │ └─┬ strip-ansi@4.0.0
# │ │ │   └── ansi-regex@3.0.0
# │ │ ├─┬ strip-ansi@5.0.0
# │ │ │ └── ansi-regex@4.0.0
# │ │ └── through@2.3.8
# │ ├─┬ js-yaml@3.12.2
# │ │ ├─┬ argparse@1.0.10
# │ │ │ └── sprintf-js@1.0.3
# │ │ └── esprima@4.0.1
# │ ├── json-stable-stringify-without-jsonify@1.0.1
# │ ├─┬ levn@0.3.0
# │ │ ├── prelude-ls@1.1.2
# │ │ └── type-check@0.3.2
# │ ├── natural-compare@1.4.0
# │ ├─┬ optionator@0.8.2
# │ │ ├── deep-is@0.1.3
# │ │ ├── fast-levenshtein@2.0.6
# │ │ └── wordwrap@1.0.0
# │ ├── path-is-inside@1.0.2
# │ ├── progress@2.0.3
# │ ├── regexpp@2.0.1
# │ ├── semver@5.6.0
# │ ├─┬ strip-ansi@4.0.0
# │ │ └── ansi-regex@3.0.0
# │ ├── strip-json-comments@2.0.1
# │ ├─┬ table@5.2.3
# │ │ ├─┬ slice-ansi@2.1.0
# │ │ │ ├── ansi-styles@3.2.1
# │ │ │ └── astral-regex@1.0.0
# │ │ └─┬ string-width@3.0.0
# │ │   ├── emoji-regex@7.0.3
# │ │   └─┬ strip-ansi@5.0.0
# │ │     └── ansi-regex@4.0.0
# │ └── text-table@0.2.0
# ├── eslint-config-google@0.9.1
# ├─┬ file-loader@3.0.1
# │ └─┬ loader-utils@1.2.3
# │   ├── big.js@5.2.2
# │   └─┬ json5@1.0.1
# │     └── minimist@1.2.0
# ├─┬ highlight.js@9.15.5
# │ ├── bluebird@3.5.3
# │ ├── commander@2.19.0
# │ ├─┬ del@3.0.0
# │ │ ├─┬ globby@6.1.0
# │ │ │ └── pify@2.3.0
# │ │ ├── is-path-cwd@1.0.0
# │ │ ├─┬ is-path-in-cwd@1.0.1
# │ │ │ └── is-path-inside@1.0.1
# │ │ └── p-map@1.2.0
# │ ├─┬ gear@0.9.7
# │ │ ├── async@0.8.0
# │ │ ├─┬ liftoff@2.0.3
# │ │ │ ├── extend@2.0.2
# │ │ │ ├─┬ findup-sync@0.2.1
# │ │ │ │ └─┬ glob@4.3.5
# │ │ │ │   └── minimatch@2.0.10
# │ │ │ ├── flagged-respawn@0.3.2
# │ │ │ ├── minimist@1.1.3
# │ │ │ └── resolve@1.1.7
# │ │ └── minimist@0.1.0
# │ ├─┬ gear-lib@0.9.2
# │ │ ├─┬ csslint@0.10.0
# │ │ │ └── parserlib@0.2.5
# │ │ ├─┬ glob@3.2.11
# │ │ │ └─┬ minimatch@0.3.0
# │ │ │   └── lru-cache@2.7.3
# │ │ ├─┬ handlebars@2.0.0
# │ │ │ ├─┬ optimist@0.3.7
# │ │ │ │ └── wordwrap@0.0.3
# │ │ │ └─┬ uglify-js@2.3.6
# │ │ │   └── async@0.2.10
# │ │ ├─┬ jshint@2.5.11
# │ │ │ ├─┬ cli@0.6.6
# │ │ │ │ └─┬ glob@3.2.11
# │ │ │ │   └── minimatch@0.3.0
# │ │ │ ├── exit@0.1.2
# │ │ │ ├─┬ htmlparser2@3.8.3
# │ │ │ │ ├── domelementtype@1.3.1
# │ │ │ │ ├── domhandler@2.3.0
# │ │ │ │ ├─┬ domutils@1.5.1
# │ │ │ │ │ └─┬ dom-serializer@0.1.1
# │ │ │ │ │   └── entities@1.1.2
# │ │ │ │ ├── entities@1.0.0
# │ │ │ │ └─┬ readable-stream@1.1.14
# │ │ │ │   ├── isarray@0.0.1
# │ │ │ │   └── string_decoder@0.10.31
# │ │ │ ├── minimatch@1.0.0
# │ │ │ ├── shelljs@0.3.0
# │ │ │ ├── strip-json-comments@1.0.4
# │ │ │ └── underscore@1.6.0
# │ │ ├─┬ jslint@0.3.4
# │ │ │ ├─┬ glob@3.2.11
# │ │ │ │ └── minimatch@0.3.0
# │ │ │ └── nopt@1.0.10
# │ │ ├─┬ knox@0.8.10
# │ │ │ ├── debug@0.7.4
# │ │ │ ├─┬ stream-counter@0.1.0
# │ │ │ │ └── readable-stream@1.0.34
# │ │ │ └─┬ xml2js@0.2.8
# │ │ │   └── sax@0.5.8
# │ │ ├─┬ less@1.7.5
# │ │ │ ├─┬ clean-css@2.2.23
# │ │ │ │ └── commander@2.2.0
# │ │ │ ├─┬ graceful-fs@3.0.11
# │ │ │ │ └── natives@1.1.6
# │ │ │ └─┬ request@2.40.0
# │ │ │   ├── aws-sign2@0.5.0
# │ │ │   ├── forever-agent@0.5.2
# │ │ │   ├─┬ form-data@0.1.4
# │ │ │   │ ├── async@0.9.2
# │ │ │   │ └─┬ combined-stream@0.0.7
# │ │ │   │   └── delayed-stream@0.0.5
# │ │ │   ├─┬ hawk@1.1.1
# │ │ │   │ ├── boom@0.4.2
# │ │ │   │ ├── cryptiles@0.2.2
# │ │ │   │ ├── hoek@0.9.1
# │ │ │   │ └── sntp@0.2.4
# │ │ │   ├─┬ http-signature@0.10.1
# │ │ │   │ ├── asn1@0.1.11
# │ │ │   │ ├── assert-plus@0.1.5
# │ │ │   │ └── ctype@0.5.3
# │ │ │   ├── mime-types@1.0.2
# │ │ │   ├── node-uuid@1.4.8
# │ │ │   ├── oauth-sign@0.3.0
# │ │ │   ├── qs@1.0.2
# │ │ │   ├── stringstream@0.0.6
# │ │ │   ├── tough-cookie@3.0.1
# │ │ │   └── tunnel-agent@0.4.3
# │ │ ├── mime@1.2.11
# │ │ └─┬ uglify-js@2.4.24
# │ │   ├── async@0.2.10
# │ │   ├── source-map@0.1.34
# │ │   ├── uglify-to-browserify@1.0.2
# │ │   └─┬ yargs@3.5.4
# │ │     ├── camelcase@1.2.1
# │ │     ├── window-size@0.1.0
# │ │     └── wordwrap@0.0.2
# │ ├─┬ js-beautify@1.8.9
# │ │ ├─┬ config-chain@1.1.12
# │ │ │ ├── ini@1.3.5
# │ │ │ └── proto-list@1.2.4
# │ │ ├─┬ editorconfig@0.15.2
# │ │ │ ├── @types/node@10.12.27
# │ │ │ ├── @types/semver@5.5.0
# │ │ │ ├── lru-cache@4.1.5
# │ │ │ └── sigmund@1.0.1
# │ │ └─┬ nopt@4.0.1
# │ │   └── abbrev@1.1.1
# │ ├─┬ jsdom@9.2.1
# │ │ ├── abab@1.0.4
# │ │ ├── acorn@2.7.0
# │ │ ├── acorn-globals@1.0.9
# │ │ ├── array-equal@1.0.0
# │ │ ├── cssom@0.3.6
# │ │ ├── cssstyle@0.2.37
# │ │ ├─┬ escodegen@1.11.1
# │ │ │ ├── esprima@3.1.3
# │ │ │ └── source-map@0.6.1
# │ │ ├─┬ iconv-lite@0.4.24
# │ │ │ └── safer-buffer@2.1.2
# │ │ ├── nwmatcher@1.4.4
# │ │ ├── parse5@1.5.1
# │ │ ├─┬ request@2.88.0
# │ │ │ ├── aws-sign2@0.7.0
# │ │ │ ├─┬ combined-stream@1.0.7
# │ │ │ │ └── delayed-stream@1.0.0
# │ │ │ ├── extend@3.0.2
# │ │ │ ├── forever-agent@0.6.1
# │ │ │ ├── form-data@2.3.3
# │ │ │ ├─┬ http-signature@1.2.0
# │ │ │ │ └── assert-plus@1.0.0
# │ │ │ ├── mime-types@2.1.22
# │ │ │ ├── oauth-sign@0.9.0
# │ │ │ ├── qs@6.5.2
# │ │ │ ├─┬ tough-cookie@2.4.3
# │ │ │ │ └── punycode@1.4.1
# │ │ │ ├── tunnel-agent@0.6.0
# │ │ │ └── uuid@3.3.2
# │ │ ├── sax@1.2.4
# │ │ ├── symbol-tree@3.2.2
# │ │ ├─┬ tough-cookie@2.5.0
# │ │ │ ├── psl@1.1.31
# │ │ │ └── punycode@2.1.1
# │ │ ├── webidl-conversions@3.0.1
# │ │ ├─┬ whatwg-url@3.1.0
# │ │ │ └── tr46@0.0.3
# │ │ └── xml-name-validator@2.0.1
# │ └── tiny-worker@2.1.2
# ├── highlightjs@9.12.0
# ├─┬ html-loader@0.5.5
# │ ├─┬ es6-templates@0.2.3
# │ │ └─┬ recast@0.11.23
# │ │   ├── ast-types@0.9.6
# │ │   └── source-map@0.5.7
# │ ├── fastparse@1.1.2
# │ ├─┬ html-minifier@3.5.21
# │ │ ├─┬ camel-case@3.0.0
# │ │ │ ├─┬ no-case@2.3.2
# │ │ │ │ └── lower-case@1.1.4
# │ │ │ └── upper-case@1.1.3
# │ │ ├─┬ clean-css@4.2.1
# │ │ │ └── source-map@0.6.1
# │ │ ├── commander@2.17.1
# │ │ ├── he@1.2.0
# │ │ ├── param-case@2.1.1
# │ │ ├── relateurl@0.2.7
# │ │ └── uglify-js@3.4.9
# │ ├─┬ loader-utils@1.2.3
# │ │ ├── big.js@5.2.2
# │ │ └─┬ json5@1.0.1
# │ │   └── minimist@1.2.0
# │ └── object-assign@4.1.1
# ├─┬ html-webpack-plugin@3.2.0
# │ ├─┬ loader-utils@0.2.17
# │ │ └── big.js@3.2.0
# │ ├─┬ pretty-error@2.1.1
# │ │ ├─┬ renderkid@2.0.3
# │ │ │ ├─┬ css-select@1.2.0
# │ │ │ │ ├── boolbase@1.0.0
# │ │ │ │ ├── css-what@2.1.3
# │ │ │ │ └── nth-check@1.0.2
# │ │ │ └── dom-converter@0.2.0
# │ │ └── utila@0.4.0
# │ ├── tapable@1.1.1
# │ ├── toposort@1.0.7
# │ └─┬ util.promisify@1.0.0
# │   ├─┬ define-properties@1.1.3
# │   │ └── object-keys@1.1.0
# │   └─┬ object.getownpropertydescriptors@2.0.3
# │     └─┬ es-abstract@1.13.0
# │       ├─┬ es-to-primitive@1.2.0
# │       │ ├── is-date-object@1.0.1
# │       │ └─┬ is-symbol@1.0.2
# │       │   └── has-symbols@1.0.0
# │       ├── function-bind@1.1.1
# │       ├── has@1.0.3
# │       ├── is-callable@1.1.4
# │       └── is-regex@1.0.4
# ├── js-base64@2.5.1
# ├─┬ node-sass@4.11.0
# │ ├── async-foreach@0.1.3
# │ ├─┬ chalk@1.1.3
# │ │ ├── ansi-styles@2.2.1
# │ │ ├── has-ansi@2.0.0
# │ │ └── supports-color@2.0.0
# │ ├─┬ cross-spawn@3.0.1
# │ │ └─┬ lru-cache@4.1.5
# │ │   ├── pseudomap@1.0.2
# │ │   └── yallist@2.1.2
# │ ├─┬ gaze@1.1.3
# │ │ └── globule@1.2.1
# │ ├── get-stdin@4.0.1
# │ ├── in-publish@2.0.0
# │ ├── lodash.assign@4.2.0
# │ ├── lodash.clonedeep@4.5.0
# │ ├── lodash.mergewith@4.6.1
# │ ├─┬ meow@3.7.0
# │ │ ├─┬ camelcase-keys@2.1.0
# │ │ │ └── camelcase@2.1.1
# │ │ ├── decamelize@1.2.0
# │ │ ├─┬ loud-rejection@1.6.0
# │ │ │ ├─┬ currently-unhandled@0.4.1
# │ │ │ │ └── array-find-index@1.0.2
# │ │ │ └── signal-exit@3.0.2
# │ │ ├── map-obj@1.0.1
# │ │ ├── minimist@1.2.0
# │ │ ├─┬ normalize-package-data@2.5.0
# │ │ │ ├── hosted-git-info@2.7.1
# │ │ │ ├─┬ resolve@1.10.0
# │ │ │ │ └── path-parse@1.0.6
# │ │ │ └─┬ validate-npm-package-license@3.0.4
# │ │ │   ├─┬ spdx-correct@3.1.0
# │ │ │   │ └── spdx-license-ids@3.0.3
# │ │ │   └─┬ spdx-expression-parse@3.0.0
# │ │ │     └── spdx-exceptions@2.2.0
# │ │ ├─┬ read-pkg-up@1.0.1
# │ │ │ ├─┬ find-up@1.1.2
# │ │ │ │ └── path-exists@2.1.0
# │ │ │ └─┬ read-pkg@1.1.0
# │ │ │   ├─┬ load-json-file@1.1.0
# │ │ │   │ ├── graceful-fs@4.1.15
# │ │ │   │ ├─┬ parse-json@2.2.0
# │ │ │   │ │ └─┬ error-ex@1.3.2
# │ │ │   │ │   └── is-arrayish@0.2.1
# │ │ │   │ ├── pify@2.3.0
# │ │ │   │ └─┬ strip-bom@2.0.0
# │ │ │   │   └── is-utf8@0.2.1
# │ │ │   └─┬ path-type@1.1.0
# │ │ │     ├── graceful-fs@4.1.15
# │ │ │     └── pify@2.3.0
# │ │ ├─┬ redent@1.0.0
# │ │ │ ├── indent-string@2.1.0
# │ │ │ └── strip-indent@1.0.1
# │ │ └── trim-newlines@1.0.0
# │ ├── nan@2.12.1
# │ ├─┬ node-gyp@3.8.0
# │ │ ├─┬ fstream@1.0.11
# │ │ │ └── graceful-fs@4.1.15
# │ │ ├── graceful-fs@4.1.15
# │ │ ├── nopt@3.0.6
# │ │ ├── osenv@0.1.5
# │ │ ├─┬ request@2.88.0
# │ │ │ ├── aws-sign2@0.7.0
# │ │ │ ├─┬ combined-stream@1.0.7
# │ │ │ │ └── delayed-stream@1.0.0
# │ │ │ ├── extend@3.0.2
# │ │ │ ├── forever-agent@0.6.1
# │ │ │ ├── form-data@2.3.3
# │ │ │ ├─┬ http-signature@1.2.0
# │ │ │ │ └── assert-plus@1.0.0
# │ │ │ ├── mime-types@2.1.22
# │ │ │ ├── oauth-sign@0.9.0
# │ │ │ ├── qs@6.5.2
# │ │ │ ├─┬ tough-cookie@2.4.3
# │ │ │ │ └── punycode@1.4.1
# │ │ │ ├── tunnel-agent@0.6.0
# │ │ │ └── uuid@3.3.2
# │ │ ├── semver@5.3.0
# │ │ └─┬ tar@2.2.1
# │ │   └── block-stream@0.0.9
# │ ├─┬ npmlog@4.1.2
# │ │ ├─┬ are-we-there-yet@1.1.5
# │ │ │ ├── delegates@1.0.0
# │ │ │ └─┬ readable-stream@2.3.6
# │ │ │   ├── isarray@1.0.0
# │ │ │   └── string_decoder@1.1.1
# │ │ ├── console-control-strings@1.1.0
# │ │ ├─┬ gauge@2.7.4
# │ │ │ ├── aproba@1.2.0
# │ │ │ ├── has-unicode@2.0.1
# │ │ │ ├─┬ string-width@1.0.2
# │ │ │ │ └── is-fullwidth-code-point@1.0.0
# │ │ │ └── wide-align@1.1.3
# │ │ └── set-blocking@2.0.0
# │ ├─┬ request@2.88.0
# │ │ ├── aws-sign2@0.7.0
# │ │ ├── aws4@1.8.0
# │ │ ├── caseless@0.12.0
# │ │ ├─┬ combined-stream@1.0.7
# │ │ │ └── delayed-stream@1.0.0
# │ │ ├── extend@3.0.2
# │ │ ├── forever-agent@0.6.1
# │ │ ├─┬ form-data@2.3.3
# │ │ │ └── asynckit@0.4.0
# │ │ ├─┬ har-validator@5.1.3
# │ │ │ └── har-schema@2.0.0
# │ │ ├─┬ http-signature@1.2.0
# │ │ │ ├── assert-plus@1.0.0
# │ │ │ ├─┬ jsprim@1.4.1
# │ │ │ │ ├── assert-plus@1.0.0
# │ │ │ │ ├── extsprintf@1.3.0
# │ │ │ │ ├── json-schema@0.2.3
# │ │ │ │ └─┬ verror@1.10.0
# │ │ │ │   └── assert-plus@1.0.0
# │ │ │ └─┬ sshpk@1.16.1
# │ │ │   ├── asn1@0.2.4
# │ │ │   ├── assert-plus@1.0.0
# │ │ │   ├── bcrypt-pbkdf@1.0.2
# │ │ │   ├─┬ dashdash@1.14.1
# │ │ │   │ └── assert-plus@1.0.0
# │ │ │   ├── ecc-jsbn@0.1.2
# │ │ │   ├─┬ getpass@0.1.7
# │ │ │   │ └── assert-plus@1.0.0
# │ │ │   ├── jsbn@0.1.1
# │ │ │   └── tweetnacl@0.14.5
# │ │ ├── is-typedarray@1.0.0
# │ │ ├── isstream@0.1.2
# │ │ ├── json-stringify-safe@5.0.1
# │ │ ├─┬ mime-types@2.1.22
# │ │ │ └── mime-db@1.38.0
# │ │ ├── oauth-sign@0.9.0
# │ │ ├── performance-now@2.1.0
# │ │ ├── qs@6.5.2
# │ │ ├─┬ tough-cookie@2.4.3
# │ │ │ └── punycode@1.4.1
# │ │ ├── tunnel-agent@0.6.0
# │ │ └── uuid@3.3.2
# │ ├─┬ sass-graph@2.2.4
# │ │ ├─┬ scss-tokenizer@0.2.3
# │ │ │ └── source-map@0.4.4
# │ │ └─┬ yargs@7.1.0
# │ │   ├── camelcase@3.0.0
# │ │   ├─┬ cliui@3.2.0
# │ │   │ └─┬ string-width@1.0.2
# │ │   │   └── is-fullwidth-code-point@1.0.0
# │ │   ├─┬ os-locale@1.4.0
# │ │   │ └─┬ lcid@1.0.0
# │ │   │   └── invert-kv@1.0.0
# │ │   ├─┬ string-width@1.0.2
# │ │   │ └── is-fullwidth-code-point@1.0.0
# │ │   ├── which-module@1.0.0
# │ │   └─┬ yargs-parser@5.0.0
# │ │     └── camelcase@3.0.0
# │ ├─┬ stdout-stream@1.4.1
# │ │ └─┬ readable-stream@2.3.6
# │ │   ├── core-util-is@1.0.2
# │ │   ├── isarray@1.0.0
# │ │   ├── process-nextick-args@2.0.0
# │ │   ├── string_decoder@1.1.1
# │ │   └── util-deprecate@1.0.2
# │ └── true-case-path@1.0.3
# ├─┬ npm-check@5.9.0
# │ ├─┬ callsite-record@3.2.2
# │ │ ├── callsite@1.0.0
# │ │ ├─┬ error-stack-parser@1.3.6
# │ │ │ └── stackframe@0.3.1
# │ │ ├─┬ highlight-es@1.0.3
# │ │ │ ├─┬ chalk@2.4.2
# │ │ │ │ ├── ansi-styles@3.2.1
# │ │ │ │ └── supports-color@5.5.0
# │ │ │ └── is-es2016-keyword@1.0.0
# │ │ └─┬ pinkie-promise@2.0.1
# │ │   └── pinkie@2.0.4
# │ ├── co@4.6.0
# │ ├─┬ depcheck@0.6.11
# │ │ ├── builtin-modules@1.1.1
# │ │ ├── deprecate@1.1.0
# │ │ ├── deps-regex@0.1.4
# │ │ ├── require-package-name@2.0.1
# │ │ ├── walkdir@0.0.11
# │ │ └─┬ yargs@8.0.2
# │ │   ├── camelcase@4.1.0
# │ │   ├─┬ os-locale@2.1.0
# │ │   │ ├─┬ execa@0.7.0
# │ │   │ │ └─┬ cross-spawn@5.1.0
# │ │   │ │   └── lru-cache@4.1.5
# │ │   │ └── mem@1.1.0
# │ │   ├─┬ read-pkg-up@2.0.0
# │ │   │ └─┬ read-pkg@2.0.0
# │ │   │   ├─┬ load-json-file@2.0.0
# │ │   │   │ ├── graceful-fs@4.1.15
# │ │   │   │ ├── pify@2.3.0
# │ │   │   │ └── strip-bom@3.0.0
# │ │   │   └── path-type@2.0.0
# │ │   ├── which-module@2.0.0
# │ │   └── yargs-parser@7.0.0
# │ ├─┬ execa@0.2.2
# │ │ ├─┬ cross-spawn-async@2.2.5
# │ │ │ └── lru-cache@4.1.5
# │ │ ├── npm-run-path@1.0.0
# │ │ ├── path-key@1.0.0
# │ │ └── strip-eof@1.0.0
# │ ├── giturl@1.0.1
# │ ├─┬ global-modules@1.0.0
# │ │ ├─┬ global-prefix@1.0.2
# │ │ │ ├── expand-tilde@2.0.2
# │ │ │ └─┬ homedir-polyfill@1.0.3
# │ │ │   └── parse-passwd@1.0.0
# │ │ ├── is-windows@1.0.2
# │ │ └── resolve-dir@1.0.1
# │ ├─┬ globby@4.1.0
# │ │ ├─┬ array-union@1.0.2
# │ │ │ └── array-uniq@1.0.3
# │ │ ├── arrify@1.0.1
# │ │ ├── glob@6.0.4
# │ │ └── pify@2.3.0
# │ ├─┬ inquirer@0.12.0
# │ │ ├── ansi-escapes@1.4.0
# │ │ ├── ansi-regex@2.1.1
# │ │ ├─┬ cli-cursor@1.0.2
# │ │ │ └─┬ restore-cursor@1.0.1
# │ │ │   ├── exit-hook@1.1.1
# │ │ │   └── onetime@1.1.0
# │ │ ├── figures@1.7.0
# │ │ ├─┬ readline2@1.0.1
# │ │ │ ├── is-fullwidth-code-point@1.0.0
# │ │ │ └── mute-stream@0.0.5
# │ │ ├── run-async@0.1.0
# │ │ ├── rx-lite@3.1.2
# │ │ └─┬ string-width@1.0.2
# │ │   ├── code-point-at@1.1.0
# │ │   └─┬ is-fullwidth-code-point@1.0.0
# │ │     └── number-is-nan@1.0.1
# │ ├─┬ is-ci@1.2.1
# │ │ └── ci-info@1.6.0
# │ ├─┬ node-emoji@1.10.0
# │ │ └── lodash.toarray@4.4.0
# │ ├─┬ ora@0.2.3
# │ │ ├─┬ cli-cursor@1.0.2
# │ │ │ └─┬ restore-cursor@1.0.1
# │ │ │   └── onetime@1.1.0
# │ │ └── cli-spinners@0.1.2
# │ ├─┬ package-json@4.0.1
# │ │ ├─┬ got@6.7.1
# │ │ │ ├─┬ create-error-class@3.0.2
# │ │ │ │ └── capture-stack-trace@1.0.1
# │ │ │ ├── duplexer3@0.1.4
# │ │ │ ├── get-stream@3.0.0
# │ │ │ ├── is-redirect@1.0.0
# │ │ │ ├── is-retry-allowed@1.1.0
# │ │ │ ├── is-stream@1.1.0
# │ │ │ ├── lowercase-keys@1.0.1
# │ │ │ ├── timed-out@4.0.1
# │ │ │ ├── unzip-response@2.0.1
# │ │ │ └─┬ url-parse-lax@1.0.0
# │ │ │   └── prepend-http@1.0.4
# │ │ ├─┬ registry-auth-token@3.3.2
# │ │ │ └─┬ rc@1.2.8
# │ │ │   ├── deep-extend@0.6.0
# │ │ │   ├── minimist@1.2.0
# │ │ │   └── strip-json-comments@2.0.1
# │ │ └── registry-url@3.1.0
# │ ├── path-exists@2.1.0
# │ ├─┬ pkg-dir@1.0.0
# │ │ └── find-up@1.1.2
# │ ├─┬ preferred-pm@1.0.1
# │ │ ├── path-exists@3.0.0
# │ │ └─┬ which-pm@1.1.0
# │ │   └─┬ load-yaml-file@0.1.0
# │ │     ├── graceful-fs@4.1.15
# │ │     ├── pify@2.3.0
# │ │     └── strip-bom@3.0.0
# │ ├── semver-diff@2.1.0
# │ ├── throat@2.0.2
# │ ├─┬ update-notifier@2.5.0
# │ │ ├─┬ boxen@1.3.0
# │ │ │ ├── ansi-align@2.0.0
# │ │ │ ├── camelcase@4.1.0
# │ │ │ ├─┬ chalk@2.4.2
# │ │ │ │ ├── ansi-styles@3.2.1
# │ │ │ │ └── supports-color@5.5.0
# │ │ │ ├── cli-boxes@1.0.0
# │ │ │ ├─┬ term-size@1.2.0
# │ │ │ │ └─┬ execa@0.7.0
# │ │ │ │   └─┬ cross-spawn@5.1.0
# │ │ │ │     └── lru-cache@4.1.5
# │ │ │ └── widest-line@2.0.1
# │ │ ├─┬ chalk@2.4.2
# │ │ │ ├── ansi-styles@3.2.1
# │ │ │ └── supports-color@5.5.0
# │ │ ├─┬ configstore@3.1.2
# │ │ │ ├─┬ dot-prop@4.2.0
# │ │ │ │ └── is-obj@1.0.1
# │ │ │ ├── graceful-fs@4.1.15
# │ │ │ ├─┬ unique-string@1.0.0
# │ │ │ │ └── crypto-random-string@1.0.0
# │ │ │ └─┬ write-file-atomic@2.4.2
# │ │ │   └── graceful-fs@4.1.15
# │ │ ├── import-lazy@2.1.0
# │ │ ├─┬ is-installed-globally@0.1.0
# │ │ │ └── global-dirs@0.1.1
# │ │ ├── is-npm@1.0.0
# │ │ ├── latest-version@3.1.0
# │ │ └── xdg-basedir@3.0.0
# │ └── xtend@4.0.1
# ├─┬ required-loader@1.3.16
# │ ├── glob@6.0.4
# │ ├─┬ imports-loader@0.6.5
# │ │ └─┬ source-map@0.1.43
# │ │   └── amdefine@1.0.1
# │ └─┬ loader-utils@1.2.3
# │   ├── big.js@5.2.2
# │   └─┬ json5@1.0.1
# │     └── minimist@1.2.0
# ├─┬ sass-loader@7.1.0
# │ ├─┬ clone-deep@2.0.2
# │ │ ├─┬ for-own@1.0.0
# │ │ │ └── for-in@1.0.2
# │ │ ├─┬ is-plain-object@2.0.4
# │ │ │ └── isobject@3.0.1
# │ │ ├── kind-of@6.0.2
# │ │ └─┬ shallow-clone@1.0.0
# │ │   ├── is-extendable@0.1.1
# │ │   ├── kind-of@5.1.0
# │ │   └─┬ mixin-object@2.0.1
# │ │     └── for-in@0.1.8
# │ ├─┬ loader-utils@1.2.3
# │ │ ├── big.js@5.2.2
# │ │ └─┬ json5@1.0.1
# │ │   └── minimist@1.2.0
# │ ├── lodash.tail@4.1.1
# │ ├── neo-async@2.6.0
# │ └── pify@3.0.0
# ├─┬ source-map-loader@0.2.4
# │ ├── async@2.6.2
# │ └─┬ loader-utils@1.2.3
# │   ├── big.js@5.2.2
# │   └─┬ json5@1.0.1
# │     └── minimist@1.2.0
# ├─┬ style-loader@0.18.2
# │ ├─┬ loader-utils@1.2.3
# │ │ ├── big.js@5.2.2
# │ │ └─┬ json5@1.0.1
# │ │   └── minimist@1.2.0
# │ └─┬ schema-utils@0.3.0
# │   └─┬ ajv@5.5.2
# │     ├── fast-deep-equal@1.1.0
# │     └── json-schema-traverse@0.3.1
# ├─┬ terser@3.16.1
# │ ├── commander@2.17.1
# │ ├── source-map@0.6.1
# │ └─┬ source-map-support@0.5.10
# │   └── buffer-from@1.1.1
# ├─┬ url-loader@1.1.2
# │ ├─┬ loader-utils@1.2.3
# │ │ ├── big.js@5.2.2
# │ │ └─┬ json5@1.0.1
# │ │   └── minimist@1.2.0
# │ └── mime@2.4.0
# ├─┬ webpack@4.29.5
# │ ├─┬ @webassemblyjs/ast@1.8.3
# │ │ ├── @webassemblyjs/helper-wasm-bytecode@1.8.3
# │ │ └─┬ @webassemblyjs/wast-parser@1.8.3
# │ │   ├── @webassemblyjs/floating-point-hex-parser@1.8.3
# │ │   ├── @webassemblyjs/helper-code-frame@1.8.3
# │ │   ├── @webassemblyjs/helper-fsm@1.8.3
# │ │   └── @xtuc/long@4.2.2
# │ ├─┬ @webassemblyjs/helper-module-context@1.8.3
# │ │ └── mamacro@0.0.3
# │ ├─┬ @webassemblyjs/wasm-edit@1.8.3
# │ │ ├── @webassemblyjs/helper-buffer@1.8.3
# │ │ ├── @webassemblyjs/helper-wasm-section@1.8.3
# │ │ ├── @webassemblyjs/wasm-gen@1.8.3
# │ │ ├── @webassemblyjs/wasm-opt@1.8.3
# │ │ └── @webassemblyjs/wast-printer@1.8.3
# │ ├─┬ @webassemblyjs/wasm-parser@1.8.3
# │ │ ├── @webassemblyjs/helper-api-error@1.8.3
# │ │ ├─┬ @webassemblyjs/ieee754@1.8.3
# │ │ │ └── @xtuc/ieee754@1.2.0
# │ │ ├── @webassemblyjs/leb128@1.8.3
# │ │ └── @webassemblyjs/utf8@1.8.3
# │ ├── UNMET PEER DEPENDENCY acorn@6.1.0
# │ ├── acorn-dynamic-import@4.0.0
# │ ├── ajv-keywords@3.4.0
# │ ├─┬ chrome-trace-event@1.0.0
# │ │ └── tslib@1.9.3
# │ ├─┬ enhanced-resolve@4.1.0
# │ │ └── graceful-fs@4.1.15
# │ ├── json-parse-better-errors@1.0.2
# │ ├── loader-runner@2.4.0
# │ ├─┬ loader-utils@1.2.3
# │ │ ├── big.js@5.2.2
# │ │ └─┬ json5@1.0.1
# │ │   └── minimist@1.2.0
# │ ├─┬ memory-fs@0.4.1
# │ │ ├─┬ errno@0.1.7
# │ │ │ └── prr@1.0.1
# │ │ └─┬ readable-stream@2.3.6
# │ │   ├── isarray@1.0.0
# │ │   └── string_decoder@1.1.1
# │ ├─┬ micromatch@3.1.10
# │ │ ├── arr-diff@4.0.0
# │ │ ├── array-unique@0.3.2
# │ │ ├─┬ braces@2.3.2
# │ │ │ ├── arr-flatten@1.1.0
# │ │ │ ├── extend-shallow@2.0.1
# │ │ │ ├─┬ fill-range@4.0.0
# │ │ │ │ ├── extend-shallow@2.0.1
# │ │ │ │ ├─┬ is-number@3.0.0
# │ │ │ │ │ └─┬ kind-of@3.2.2
# │ │ │ │ │   └── is-buffer@1.1.6
# │ │ │ │ ├── repeat-string@1.6.1
# │ │ │ │ └── to-regex-range@2.1.1
# │ │ │ ├── repeat-element@1.1.3
# │ │ │ ├─┬ snapdragon-node@2.1.1
# │ │ │ │ ├─┬ define-property@1.0.0
# │ │ │ │ │ └─┬ is-descriptor@1.0.2
# │ │ │ │ │   ├── is-accessor-descriptor@1.0.0
# │ │ │ │ │   └── is-data-descriptor@1.0.0
# │ │ │ │ └─┬ snapdragon-util@3.0.1
# │ │ │ │   └── kind-of@3.2.2
# │ │ │ └── split-string@3.1.0
# │ │ ├─┬ define-property@2.0.2
# │ │ │ └─┬ is-descriptor@1.0.2
# │ │ │   ├── is-accessor-descriptor@1.0.0
# │ │ │   └── is-data-descriptor@1.0.0
# │ │ ├─┬ extend-shallow@3.0.2
# │ │ │ ├── assign-symbols@1.0.0
# │ │ │ └── is-extendable@1.0.1
# │ │ ├─┬ extglob@2.0.4
# │ │ │ ├─┬ define-property@1.0.0
# │ │ │ │ └─┬ is-descriptor@1.0.2
# │ │ │ │   ├── is-accessor-descriptor@1.0.0
# │ │ │ │   └── is-data-descriptor@1.0.0
# │ │ │ ├─┬ expand-brackets@2.1.4
# │ │ │ │ ├── debug@2.6.9
# │ │ │ │ ├── define-property@0.2.5
# │ │ │ │ ├── extend-shallow@2.0.1
# │ │ │ │ └── posix-character-classes@0.1.1
# │ │ │ └── extend-shallow@2.0.1
# │ │ ├─┬ fragment-cache@0.2.1
# │ │ │ └── map-cache@0.2.2
# │ │ ├── nanomatch@1.2.13
# │ │ ├── object.pick@1.3.0
# │ │ ├─┬ regex-not@1.0.2
# │ │ │ └─┬ safe-regex@1.1.0
# │ │ │   └── ret@0.1.15
# │ │ ├─┬ snapdragon@0.8.2
# │ │ │ ├─┬ base@0.11.2
# │ │ │ │ ├─┬ cache-base@1.0.1
# │ │ │ │ │ ├─┬ collection-visit@1.0.0
# │ │ │ │ │ │ ├── map-visit@1.0.0
# │ │ │ │ │ │ └── object-visit@1.0.1
# │ │ │ │ │ ├── get-value@2.0.6
# │ │ │ │ │ ├─┬ has-value@1.0.0
# │ │ │ │ │ │ └─┬ has-values@1.0.0
# │ │ │ │ │ │   └── kind-of@4.0.0
# │ │ │ │ │ ├─┬ set-value@2.0.0
# │ │ │ │ │ │ └── extend-shallow@2.0.1
# │ │ │ │ │ ├─┬ to-object-path@0.3.0
# │ │ │ │ │ │ └── kind-of@3.2.2
# │ │ │ │ │ ├─┬ union-value@1.0.0
# │ │ │ │ │ │ └─┬ set-value@0.4.3
# │ │ │ │ │ │   └── extend-shallow@2.0.1
# │ │ │ │ │ └─┬ unset-value@1.0.0
# │ │ │ │ │   └─┬ has-value@0.3.1
# │ │ │ │ │     ├── has-values@0.1.4
# │ │ │ │ │     └─┬ isobject@2.1.0
# │ │ │ │ │       └── isarray@1.0.0
# │ │ │ │ ├─┬ class-utils@0.3.6
# │ │ │ │ │ ├── arr-union@3.1.0
# │ │ │ │ │ ├── define-property@0.2.5
# │ │ │ │ │ └─┬ static-extend@0.1.2
# │ │ │ │ │   ├── define-property@0.2.5
# │ │ │ │ │   └─┬ object-copy@0.1.0
# │ │ │ │ │     ├── copy-descriptor@0.1.1
# │ │ │ │ │     ├── define-property@0.2.5
# │ │ │ │ │     └── kind-of@3.2.2
# │ │ │ │ ├── component-emitter@1.2.1
# │ │ │ │ ├─┬ define-property@1.0.0
# │ │ │ │ │ └─┬ is-descriptor@1.0.2
# │ │ │ │ │   ├── is-accessor-descriptor@1.0.0
# │ │ │ │ │   └── is-data-descriptor@1.0.0
# │ │ │ │ ├─┬ mixin-deep@1.3.1
# │ │ │ │ │ └── is-extendable@1.0.1
# │ │ │ │ └── pascalcase@0.1.1
# │ │ │ ├── debug@2.6.9
# │ │ │ ├─┬ define-property@0.2.5
# │ │ │ │ └─┬ is-descriptor@0.1.6
# │ │ │ │   ├─┬ is-accessor-descriptor@0.1.6
# │ │ │ │   │ └── kind-of@3.2.2
# │ │ │ │   ├─┬ is-data-descriptor@0.1.4
# │ │ │ │   │ └── kind-of@3.2.2
# │ │ │ │   └── kind-of@5.1.0
# │ │ │ ├── extend-shallow@2.0.1
# │ │ │ ├── source-map@0.5.7
# │ │ │ ├─┬ source-map-resolve@0.5.2
# │ │ │ │ ├── atob@2.1.2
# │ │ │ │ ├── decode-uri-component@0.2.0
# │ │ │ │ ├── resolve-url@0.2.1
# │ │ │ │ ├── source-map-url@0.4.0
# │ │ │ │ └── urix@0.1.0
# │ │ │ └── use@3.1.1
# │ │ └── to-regex@3.0.2
# │ ├─┬ node-libs-browser@2.2.0
# │ │ ├─┬ assert@1.4.1
# │ │ │ └─┬ util@0.10.3
# │ │ │   └── inherits@2.0.1
# │ │ ├─┬ browserify-zlib@0.2.0
# │ │ │ └── pako@1.0.8
# │ │ ├─┬ buffer@4.9.1
# │ │ │ ├── base64-js@1.3.0
# │ │ │ ├── ieee754@1.1.12
# │ │ │ └── isarray@1.0.0
# │ │ ├─┬ console-browserify@1.1.0
# │ │ │ └── date-now@0.1.4
# │ │ ├── constants-browserify@1.0.0
# │ │ ├─┬ crypto-browserify@3.12.0
# │ │ │ ├─┬ browserify-cipher@1.0.1
# │ │ │ │ ├─┬ browserify-aes@1.2.0
# │ │ │ │ │ └── buffer-xor@1.0.3
# │ │ │ │ ├─┬ browserify-des@1.0.2
# │ │ │ │ │ └── des.js@1.0.0
# │ │ │ │ └── evp_bytestokey@1.0.3
# │ │ │ ├─┬ browserify-sign@4.0.4
# │ │ │ │ ├── bn.js@4.11.8
# │ │ │ │ ├── browserify-rsa@4.0.1
# │ │ │ │ ├─┬ elliptic@6.4.1
# │ │ │ │ │ ├── brorand@1.1.0
# │ │ │ │ │ ├── hash.js@1.1.7
# │ │ │ │ │ ├── hmac-drbg@1.0.1
# │ │ │ │ │ └── minimalistic-crypto-utils@1.0.1
# │ │ │ │ └─┬ parse-asn1@5.1.4
# │ │ │ │   └── asn1.js@4.10.1
# │ │ │ ├── create-ecdh@4.0.3
# │ │ │ ├─┬ create-hash@1.2.0
# │ │ │ │ ├── cipher-base@1.0.4
# │ │ │ │ ├─┬ md5.js@1.3.5
# │ │ │ │ │ └── hash-base@3.0.4
# │ │ │ │ ├── ripemd160@2.0.2
# │ │ │ │ └── sha.js@2.4.11
# │ │ │ ├── create-hmac@1.1.7
# │ │ │ ├─┬ diffie-hellman@5.0.3
# │ │ │ │ └── miller-rabin@4.0.1
# │ │ │ ├── pbkdf2@3.0.17
# │ │ │ ├── public-encrypt@4.0.3
# │ │ │ ├── randombytes@2.1.0
# │ │ │ └── randomfill@1.0.4
# │ │ ├── domain-browser@1.2.0
# │ │ ├── events@3.0.0
# │ │ ├── https-browserify@1.0.0
# │ │ ├── os-browserify@0.3.0
# │ │ ├── path-browserify@0.0.0
# │ │ ├── process@0.11.10
# │ │ ├── punycode@1.4.1
# │ │ ├── querystring-es3@0.2.1
# │ │ ├─┬ readable-stream@2.3.6
# │ │ │ ├── isarray@1.0.0
# │ │ │ └── string_decoder@1.1.1
# │ │ ├─┬ stream-browserify@2.0.2
# │ │ │ └─┬ readable-stream@2.3.6
# │ │ │   ├── isarray@1.0.0
# │ │ │   └── string_decoder@1.1.1
# │ │ ├─┬ stream-http@2.8.3
# │ │ │ ├── builtin-status-codes@3.0.0
# │ │ │ ├─┬ readable-stream@2.3.6
# │ │ │ │ ├── isarray@1.0.0
# │ │ │ │ └── string_decoder@1.1.1
# │ │ │ └── to-arraybuffer@1.0.1
# │ │ ├── string_decoder@1.2.0
# │ │ ├─┬ timers-browserify@2.0.10
# │ │ │ └── setimmediate@1.0.5
# │ │ ├── tty-browserify@0.0.0
# │ │ ├── util@0.11.1
# │ │ └─┬ vm-browserify@0.0.4
# │ │   └── indexof@0.0.1
# │ ├─┬ terser-webpack-plugin@1.2.3
# │ │ ├─┬ cacache@11.3.2
# │ │ │ ├── chownr@1.1.1
# │ │ │ ├── figgy-pudding@3.5.1
# │ │ │ ├── graceful-fs@4.1.15
# │ │ │ ├─┬ lru-cache@5.1.1
# │ │ │ │ └── yallist@3.0.3
# │ │ │ ├─┬ mississippi@3.0.0
# │ │ │ │ ├─┬ concat-stream@1.6.2
# │ │ │ │ │ ├─┬ readable-stream@2.3.6
# │ │ │ │ │ │ ├── isarray@1.0.0
# │ │ │ │ │ │ └── string_decoder@1.1.1
# │ │ │ │ │ └── typedarray@0.0.6
# │ │ │ │ ├─┬ duplexify@3.7.1
# │ │ │ │ │ ├─┬ readable-stream@2.3.6
# │ │ │ │ │ │ ├── isarray@1.0.0
# │ │ │ │ │ │ └── string_decoder@1.1.1
# │ │ │ │ │ └── stream-shift@1.0.0
# │ │ │ │ ├── end-of-stream@1.4.1
# │ │ │ │ ├─┬ flush-write-stream@1.1.1
# │ │ │ │ │ └─┬ readable-stream@2.3.6
# │ │ │ │ │   ├── isarray@1.0.0
# │ │ │ │ │   └── string_decoder@1.1.1
# │ │ │ │ ├─┬ from2@2.3.0
# │ │ │ │ │ └─┬ readable-stream@2.3.6
# │ │ │ │ │   ├── isarray@1.0.0
# │ │ │ │ │   └── string_decoder@1.1.1
# │ │ │ │ ├─┬ parallel-transform@1.1.0
# │ │ │ │ │ ├── cyclist@0.2.2
# │ │ │ │ │ └─┬ readable-stream@2.3.6
# │ │ │ │ │   ├── isarray@1.0.0
# │ │ │ │ │   └── string_decoder@1.1.1
# │ │ │ │ ├── pump@3.0.0
# │ │ │ │ ├─┬ pumpify@1.5.1
# │ │ │ │ │ └── pump@2.0.1
# │ │ │ │ ├── stream-each@1.2.3
# │ │ │ │ └─┬ through2@2.0.5
# │ │ │ │   └─┬ readable-stream@2.3.6
# │ │ │ │     ├── isarray@1.0.0
# │ │ │ │     └── string_decoder@1.1.1
# │ │ │ ├─┬ move-concurrently@1.0.1
# │ │ │ │ ├─┬ copy-concurrently@1.0.5
# │ │ │ │ │ └── iferr@0.1.5
# │ │ │ │ ├─┬ fs-write-stream-atomic@1.0.10
# │ │ │ │ │ └── graceful-fs@4.1.15
# │ │ │ │ └── run-queue@1.0.3
# │ │ │ ├── promise-inflight@1.0.1
# │ │ │ ├── ssri@6.0.1
# │ │ │ ├─┬ unique-filename@1.1.1
# │ │ │ │ └── unique-slug@2.0.1
# │ │ │ └── y18n@4.0.0
# │ │ ├─┬ find-cache-dir@2.0.0
# │ │ │ └─┬ pkg-dir@3.0.0
# │ │ │   └─┬ find-up@3.0.0
# │ │ │     └─┬ locate-path@3.0.0
# │ │ │       └─┬ p-locate@3.0.0
# │ │ │         └─┬ p-limit@2.1.0
# │ │ │           └── p-try@2.0.0
# │ │ ├── serialize-javascript@1.6.1
# │ │ ├── source-map@0.6.1
# │ │ └── worker-farm@1.6.0
# │ ├─┬ watchpack@1.6.0
# │ │ └── graceful-fs@4.1.15
# │ └─┬ webpack-sources@1.3.0
# │   ├── source-list-map@2.0.1
# │   └── source-map@0.6.1
# ├─┬ webpack-cli@3.2.3
# │ ├─┬ chalk@2.4.2
# │ │ └── ansi-styles@3.2.1
# │ ├─┬ findup-sync@2.0.0
# │ │ ├── detect-file@1.0.0
# │ │ └─┬ is-glob@3.1.0
# │ │   └── is-extglob@2.1.1
# │ ├─┬ import-local@2.0.0
# │ │ ├─┬ pkg-dir@3.0.0
# │ │ │ └─┬ find-up@3.0.0
# │ │ │   └─┬ locate-path@3.0.0
# │ │ │     └─┬ p-locate@3.0.0
# │ │ │       └─┬ p-limit@2.1.0
# │ │ │         └── p-try@2.0.0
# │ │ └─┬ resolve-cwd@2.0.0
# │ │   └── resolve-from@3.0.0
# │ ├── interpret@1.2.0
# │ ├─┬ loader-utils@1.2.3
# │ │ ├── big.js@5.2.2
# │ │ └─┬ json5@1.0.1
# │ │   └── minimist@1.2.0
# │ ├─┬ supports-color@5.5.0
# │ │ └── has-flag@3.0.0
# │ ├── v8-compile-cache@2.0.2
# │ └─┬ yargs@12.0.5
# │   ├─┬ cliui@4.1.0
# │   │ ├─┬ strip-ansi@4.0.0
# │   │ │ └── ansi-regex@3.0.0
# │   │ └─┬ wrap-ansi@2.1.0
# │   │   └─┬ string-width@1.0.2
# │   │     └── is-fullwidth-code-point@1.0.0
# │   ├─┬ find-up@3.0.0
# │   │ └─┬ locate-path@3.0.0
# │   │   └─┬ p-locate@3.0.0
# │   │     └─┬ p-limit@2.1.0
# │   │       └── p-try@2.0.0
# │   ├── get-caller-file@1.0.3
# │   ├─┬ os-locale@3.1.0
# │   │ ├─┬ execa@1.0.0
# │   │ │ ├── get-stream@4.1.0
# │   │ │ ├── npm-run-path@2.0.2
# │   │ │ └── p-finally@1.0.0
# │   │ ├─┬ lcid@2.0.0
# │   │ │ └── invert-kv@2.0.0
# │   │ └─┬ mem@4.1.0
# │   │   ├─┬ map-age-cleaner@0.1.3
# │   │   │ └── p-defer@1.0.0
# │   │   ├── mimic-fn@1.2.0
# │   │   └── p-is-promise@2.0.0
# │   ├── require-directory@2.1.1
# │   ├── require-main-filename@1.0.1
# │   ├── which-module@2.0.0
# │   ├── y18n@3.2.1
# │   └─┬ yargs-parser@11.1.1
# │     └── camelcase@5.0.0
# ├─┬ webpack-dev-server@3.2.1
# │ ├── ansi-html@0.0.7
# │ ├─┬ bonjour@3.5.0
# │ │ ├── array-flatten@2.1.2
# │ │ ├── deep-equal@1.0.1
# │ │ ├── dns-equal@1.0.0
# │ │ ├─┬ dns-txt@2.0.2
# │ │ │ └── buffer-indexof@1.1.1
# │ │ ├─┬ multicast-dns@6.2.3
# │ │ │ ├── dns-packet@1.3.1
# │ │ │ └── thunky@1.0.3
# │ │ └── multicast-dns-service-types@1.1.0
# │ ├─┬ chokidar@2.1.2
# │ │ ├─┬ anymatch@2.0.0
# │ │ │ └─┬ normalize-path@2.1.1
# │ │ │   └── remove-trailing-separator@1.1.0
# │ │ ├── async-each@1.0.1
# │ │ ├─┬ glob-parent@3.1.0
# │ │ │ ├── is-glob@3.1.0
# │ │ │ └── path-dirname@1.0.2
# │ │ ├─┬ is-binary-path@1.0.1
# │ │ │ └── binary-extensions@1.13.0
# │ │ ├── is-glob@4.0.0
# │ │ ├── normalize-path@3.0.0
# │ │ ├─┬ readdirp@2.2.1
# │ │ │ ├── graceful-fs@4.1.15
# │ │ │ └─┬ readable-stream@2.3.6
# │ │ │   ├── isarray@1.0.0
# │ │ │   └── string_decoder@1.1.1
# │ │ └── upath@1.1.0
# │ ├─┬ compression@1.7.3
# │ │ ├─┬ accepts@1.3.5
# │ │ │ ├── mime-types@2.1.22
# │ │ │ └── negotiator@0.6.1
# │ │ ├── bytes@3.0.0
# │ │ ├── compressible@2.0.16
# │ │ ├── debug@2.6.9
# │ │ ├── on-headers@1.0.2
# │ │ └── vary@1.1.2
# │ ├── connect-history-api-fallback@1.6.0
# │ ├─┬ debug@4.1.1
# │ │ └── ms@2.1.1
# │ ├─┬ express@4.16.4
# │ │ ├── array-flatten@1.1.1
# │ │ ├─┬ body-parser@1.18.3
# │ │ │ ├── debug@2.6.9
# │ │ │ ├── iconv-lite@0.4.23
# │ │ │ ├── qs@6.5.2
# │ │ │ └─┬ raw-body@2.3.3
# │ │ │   └── iconv-lite@0.4.23
# │ │ ├── content-disposition@0.5.2
# │ │ ├── content-type@1.0.4
# │ │ ├── cookie@0.3.1
# │ │ ├── cookie-signature@1.0.6
# │ │ ├── debug@2.6.9
# │ │ ├── depd@1.1.2
# │ │ ├── encodeurl@1.0.2
# │ │ ├── escape-html@1.0.3
# │ │ ├── etag@1.8.1
# │ │ ├─┬ finalhandler@1.1.1
# │ │ │ ├── debug@2.6.9
# │ │ │ └── unpipe@1.0.0
# │ │ ├── fresh@0.5.2
# │ │ ├── merge-descriptors@1.0.1
# │ │ ├── methods@1.1.2
# │ │ ├─┬ on-finished@2.3.0
# │ │ │ └── ee-first@1.1.1
# │ │ ├── parseurl@1.3.2
# │ │ ├── path-to-regexp@0.1.7
# │ │ ├─┬ proxy-addr@2.0.4
# │ │ │ ├── forwarded@0.1.2
# │ │ │ └── ipaddr.js@1.8.0
# │ │ ├── qs@6.5.2
# │ │ ├── range-parser@1.2.0
# │ │ ├─┬ send@0.16.2
# │ │ │ ├── debug@2.6.9
# │ │ │ ├── destroy@1.0.4
# │ │ │ └── mime@1.4.1
# │ │ ├── serve-static@1.13.2
# │ │ ├── setprototypeof@1.1.0
# │ │ ├── statuses@1.4.0
# │ │ ├─┬ type-is@1.6.16
# │ │ │ ├── media-typer@0.3.0
# │ │ │ └── mime-types@2.1.22
# │ │ └── utils-merge@1.0.1
# │ ├── html-entities@1.2.1
# │ ├─┬ http-proxy-middleware@0.19.1
# │ │ └─┬ http-proxy@1.17.0
# │ │   ├── eventemitter3@3.1.0
# │ │   ├─┬ follow-redirects@1.7.0
# │ │   │ └─┬ debug@3.2.6
# │ │   │   └── ms@2.1.1
# │ │   └── requires-port@1.0.0
# │ ├─┬ internal-ip@4.2.0
# │ │ ├─┬ default-gateway@4.1.2
# │ │ │ ├─┬ execa@1.0.0
# │ │ │ │ └── get-stream@4.1.0
# │ │ │ └── ip-regex@2.1.0
# │ │ └── ipaddr.js@1.9.0
# │ ├── ip@1.1.5
# │ ├── killable@1.0.1
# │ ├── loglevel@1.6.1
# │ ├─┬ opn@5.4.0
# │ │ └── is-wsl@1.1.0
# │ ├─┬ portfinder@1.0.20
# │ │ ├── async@1.5.2
# │ │ └── debug@2.6.9
# │ ├─┬ selfsigned@1.10.4
# │ │ └── node-forge@0.7.5
# │ ├─┬ serve-index@1.9.1
# │ │ ├── batch@0.6.1
# │ │ ├── debug@2.6.9
# │ │ ├── http-errors@1.6.3
# │ │ └── mime-types@2.1.22
# │ ├─┬ sockjs@0.3.19
# │ │ ├─┬ faye-websocket@0.10.0
# │ │ │ └─┬ websocket-driver@0.7.0
# │ │ │   ├── http-parser-js@0.5.0
# │ │ │   └── websocket-extensions@0.1.3
# │ │ └── uuid@3.3.2
# │ ├─┬ sockjs-client@1.3.0
# │ │ ├─┬ debug@3.2.6
# │ │ │ └── ms@2.1.1
# │ │ ├─┬ eventsource@1.0.7
# │ │ │ └── original@1.0.2
# │ │ ├── faye-websocket@0.11.1
# │ │ ├── json3@3.3.2
# │ │ └─┬ url-parse@1.4.4
# │ │   └── querystringify@2.1.0
# │ ├─┬ spdy@4.0.0
# │ │ ├─┬ debug@4.1.1
# │ │ │ └── ms@2.1.1
# │ │ ├── handle-thing@2.0.0
# │ │ ├── http-deceiver@1.2.7
# │ │ ├── select-hose@2.0.0
# │ │ └─┬ spdy-transport@3.0.0
# │ │   ├─┬ debug@4.1.1
# │ │   │ └── ms@2.1.1
# │ │   ├── detect-node@2.0.4
# │ │   ├─┬ hpack.js@2.1.6
# │ │   │ └─┬ readable-stream@2.3.6
# │ │   │   ├── isarray@1.0.0
# │ │   │   └── string_decoder@1.1.1
# │ │   ├── obuf@1.1.2
# │ │   ├─┬ readable-stream@3.1.1
# │ │   │ └── string_decoder@1.2.0
# │ │   └─┬ wbuf@1.7.3
# │ │     └── minimalistic-assert@1.0.1
# │ ├── strip-ansi@3.0.1
# │ ├── supports-color@6.1.0
# │ ├─┬ url@0.11.0
# │ │ ├── punycode@1.3.2
# │ │ └── querystring@0.2.0
# │ ├─┬ webpack-dev-middleware@3.6.0
# │ │ └── mime@2.4.0
# │ ├─┬ webpack-log@2.0.0
# │ │ ├── ansi-colors@3.2.3
# │ │ └── uuid@3.3.2
# │ └─┬ yargs@12.0.2
# │   ├─┬ cliui@4.1.0
# │   │ └─┬ strip-ansi@4.0.0
# │   │   └── ansi-regex@3.0.0
# │   ├─┬ decamelize@2.0.0
# │   │ └── xregexp@4.0.0
# │   ├─┬ find-up@3.0.0
# │   │ └─┬ locate-path@3.0.0
# │   │   └─┬ p-locate@3.0.0
# │   │     └─┬ p-limit@2.1.0
# │   │       └── p-try@2.0.0
# │   ├─┬ os-locale@3.1.0
# │   │ ├─┬ execa@1.0.0
# │   │ │ └── get-stream@4.1.0
# │   │ ├─┬ lcid@2.0.0
# │   │ │ └── invert-kv@2.0.0
# │   │ └── mem@4.1.0
# │   ├── which-module@2.0.0
# │   └─┬ yargs-parser@10.1.0
# │     └── camelcase@4.1.0
# └── webpack-merge@4.2.1
#
# npm WARN optional Skipping failed optional dependency /chokidar/fsevents:
# npm WARN notsup Not compatible with your operating system or architecture: fsevents@1.2.7
# npm WARN acorn-jsx@5.0.1 requires a peer of acorn@^6.0.0 but none was installed.
# npm WARN acorn-dynamic-import@4.0.0 requires a peer of acorn@^6.0.0 but none was installed.
# npm WARN sidzbmd@1.0.0 No description
# npm WARN sidzbmd@1.0.0 No repository field.

# ssh -D 12345 3bartocha@taurus.fis.agh.edu.pl
