"""sidzbmd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from django.conf.urls import url, include
from rest_framework import routers
from tex_resources import views
from other import views as other_view

from accesses import views as views_accesses
from recipes.views import views as views_pdf

#from rest_framework_jwt.views import obtain_jwt_token
from accesses.views import throttle_obtain_jwt_token
import code

from rest_framework_jwt.views import refresh_jwt_token

router = routers.SimpleRouter()
router.register(r'sidzbmd/api/languages', other_view.LanguageViewSet)
router.register(r'sidzbmd/api/exercises', views.ExerciseView,
                base_name='exercises')
router.register(r'sidzbmd/api/documents', views_pdf.DocumentGeneratorView,
                base_name='documents')
router.register(r'sidzbmd/api/user', views_accesses.UserView,
                base_name='user')
router.register(r'sidzbmd/api/groups', views_accesses.GroupView,
                base_name='groups')
router.register(r'sidzbmd/api/categories', other_view.CategoryView,
                base_name='categories')
router.register(r'sidzbmd/api/user/exercises/permissions',
                views_accesses.ExVisPermView,
                base_name='expermissions')
router.register(r'sidzbmd/api/preambles', views.PreambuleView,
                base_name='preambles')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    # url(r'^sidzbmd/api/admin/', admin.site.urls),
    # url(r'^', include(router.urls)),
    url(r'^sidzbmd/api/api-token-auth/', throttle_obtain_jwt_token),
    url(r'^sidzbmd/api/api-token-refresh/', refresh_jwt_token),


    # attachment list
    url(r'^sidzbmd/api/documents/(?P<document_pk>[^/.]+)/recipes/$',
        views_pdf.ExerciseRecipeView.as_view({'get': 'list',
                                      'post': 'create',
                                      'put': 'update',
                                      'delete': 'destroy'})),
    url(r'^sidzbmd/api/documents/(?P<document_pk>[^/.]+)\.(?P<format>[a-z0-9]+)/recipes/?$',
        views_pdf.ExerciseRecipeView.as_view({'get': 'list',
                                      'post': 'create',
                                      'put': 'update',
                                      'delete': 'destroy'})),
    # attachment detail

    url(r'^sidzbmd/api/documents/(?P<document_pk>[^/.]+)/recipes/(?P<pk>[^/.]+)/$',
        views_pdf.ExerciseRecipeView.as_view({'get': 'retrieve',
                                      'put': 'update',
                                      'delete': 'destroy'})),
    url(r'^sidzbmd/api/documents/(?P<document_pk>[^/.]+)\.(?P<exercise_format>[a-z0-9]+)/recipes/(?P<pk>[^/.]+)\.(?P<format>[a-z0-9]+)/?$',
        views_pdf.ExerciseRecipeView.as_view({'get': 'retrieve',
                                      'put': 'update',
                                      'delete': 'destroy'})),

    # attachment list
    url(r'^sidzbmd/api/exercise/(?P<exercise_pk>[^/.]+)/attachments/$',
        views.AttachmentView.as_view({'get': 'list',
                                      'post': 'create',
                                      'delete': 'destroy'})),
    url(r'^sidzbmd/api/exercise/(?P<exercise_pk>[^/.]+)\.(?P<format>[a-z0-9]+)/attachments/?$',
        views.AttachmentView.as_view({'get': 'list',
                                      'post': 'create',
                                      'delete': 'destroy'})),

    # attachment detail

    url(r'^sidzbmd/api/exercise/(?P<exercise_pk>[^/.]+)/attachments/(?P<pk>[^/.]+)/$',
        views.AttachmentView.as_view({'get': 'retrieve',
                                      'put': 'update',
                                      'delete': 'destroy'})),
    url(r'^sidzbmd/api/exercise/(?P<exercise_pk>[^/.]+)\.(?P<exercise_format>[a-z0-9]+)/attachments/(?P<pk>[^/.]+)\.(?P<format>[a-z0-9]+)/?$',
        views.AttachmentView.as_view({'get': 'retrieve',
                                      'put': 'update',
                                      'delete': 'destroy'})),

]
urlpatterns += router.urls
# print(urlpatterns)
# code.interact(local=locals())

# python manage.py drf_create_token -r lukasz
# curl -X GET http://127.0.0.1:8000/exercise/ -H 'Authorization: Token 15f383cd2358cee9ee4b2593d45d519a2de0767e'

# from pprint import pprint
# pprint(vars(your_object))

# ssh -N -f -L 9999:orion.fis.agh.edu.pl:636 3bartocha@taurus.fis.agh.edu.pl
