from django.conf import settings
import os
import subprocess
from distutils.dir_util import copy_tree


def run(*args):
    # print(args)
    print(settings.BASE_DIR)
    frontend_dir = os.path.join(settings.BASE_DIR, "frontend")
    os.chdir(frontend_dir)
    subprocess.run("npm run-script build", shell=True, check=True)
    copy_tree(os.path.join(frontend_dir, "dist"), settings.STATIC_ROOT)
    os.chdir(settings.BASE_DIR)
    # subprocess.run("python manage.py collectstatic", shell=True, check=True)
    print(os.getcwd())
