#!/bin/python3
import os
from django.core.management import execute_from_command_line
import time


if __name__ == '__main__':

    database = "db.sqlite3"
    BASE_DIR = os.path.dirname(os.path.dirname(
        os.path.abspath(__file__)))
    migracje_tex_resources = os.path.join(BASE_DIR, 'tex_resources',
                                 'migrations')
    print(BASE_DIR)
    try:
        os.remove(database)
        os.remove(migracje_tex_resources)
    except FileNotFoundError:
        pass

    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "sidzbmd.settings")
    # execute_from_command_line(['manage.py', 'makemigrations'])
    time.sleep(5)
    execute_from_command_line(argv=["manage.py", "migrate",
                                    "--run-syncdb"])
    execute_from_command_line(['manage.py',
                               'loaddata',
                               'user_fixtures.json'])
