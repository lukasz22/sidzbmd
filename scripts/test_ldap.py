#!/usr/bin/env python3
# lista ssh tunnels
# ps aux | grep ssh
import ldap

ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, False)
ldap.set_option(ldap.OPT_REFERRALS, False)
#ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, "/home/lukasz/Dokumenty/IIstopien/magisterka/sidzbmd/ldapserver_lukasz.pem")
ldap.set_option(ldap.OPT_DEBUG_LEVEL, 4095)
ldap.protocol_version = ldap.VERSION3
import code

## first you must open a connection to the server
try:
    l = ldap.initialize('ldaps://localhost:9999')
    #l.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
    #ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
    #l = ldap.open("127.0.0.1", port=636)
    ## searching doesn't require a bind in LDAP V3.  If you're using LDAP v2, set the next line appropriately
    ## and do a bind as shown in the above example.
    # you can also set this to ldap.VERSION2 if you're using a v2 directory
    # you should  set the next option to ldap.VERSION2 if you're using a v2 directory
    #l.protocol_version = ldap.VERSION3
    l.simple_bind_s('cn=3bartocha,ou=2013,o=fis', 'zmienioneHaslo22')
except ldap.LDAPError as e:
    print(e)
    print("---------------------")
    raise
    # handle error however you like


## The next lines will also need to be changed to support your search requirements and directory
baseDN = "o=fis"
searchScope = ldap.SCOPE_SUBTREE
## retrieve all attributes - again adjust to your needs - see documentation for more options
retrieveAttributes = None
# searchFilter = "cn=*art*"
searchFilter = "(|(objectClass=organizationalUnit)(objectClass=Group))"

try:
    ldap_result_id = l.search(baseDN, searchScope,
                              searchFilter,
                              retrieveAttributes)
    result_set = []
    while 1:
        result_type, result_data = l.result(
            ldap_result_id, 0)
        if (result_data == []):
            break
        else:
            ## here you don't have to append to a list
            ## you could do whatever you want with the individual entry
            ## The appending to list is just for illustration.
            if result_type == ldap.RES_SEARCH_ENTRY:
                result_set.append(result_data[0][0])
    print(result_set)
    # code.interact(local=locals())
except ldap.LDAPError as e:
    print(e)
