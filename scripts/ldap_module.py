#!/usr/bin/env python3
import subprocess
import json

# ssh -N -f -L 9999:orion.fis.agh.edu.pl:636 3bartocha@taurus.fis.agh.edu.pl
# ldapsearch -x -h orion.fis.agh.edu.pl -LLL '(mail=lukasz.bartocha@fis.agh.edu.pl)'
# ldapsearch -x -h localhost -p 9999 -LLL '(mail=lukasz.bartocha@fis.agh.edu.pl)'


def search(port="636",
           command="ldapsearch -x -h orion.fis.agh.edu.pl -LLL",
           email="lukasz.bartocha@fis.agh.edu.pl"):
    full_command = "{0} '(mail={1})'".format(command, email)
    full_command =\
        "{0} '(&(group-name=*)(uid=3bartocha))'".format(
            command)
    print(full_command)
    arguments = ["ssh", "3bartocha@taurus.fis.agh.edu.pl",
                 full_command]
    ssh = subprocess.Popen(arguments,
                           shell=False,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    person_data = ssh.stdout.readlines()
    for item in person_data:
        print("{0}".format(item))
    return person_data

# JSON Validator function


def json_validator(data):
    try:
        with open(os.path.join(os.getcwd(), 'tex_resources/fixtures/user_fixtures.json'),
                  "r") as file:
            json.loads(file.read())
            return True
    except ValueError as error:
        print("invalid json: %s" % error)
        return False


if __name__ == "__main__":
    out = search()
    print("----------")
    print("----------")
    search(email="maciej.woloszyn@fis.agh.edu.pl")
