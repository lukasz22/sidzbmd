const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    historyApiFallback: {
      index: '/sidzbmd/client/',
      contentBase: './dist'
    },
    proxy: {
      '/sidzbmd/api/': {
        target: 'http://127.0.0.1:8000/'
      }
    }
  }
});
