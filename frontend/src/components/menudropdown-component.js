import Angular from 'angular';
import AngularBootstrap from 'angular-ui-bootstrap';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import ngTouch from 'angular-touch';

import MenuDropdownView from '../templates/menudropdown-view.html';
import CategoryService from '../services/categories.js'
// import TreeController from '../controllers/menudropdown-controller.js'



let app = Angular.module('menudropdown.component',
                         [AngularBootstrap, ngAnimate,
                          ngSanitize, ngTouch, CategoryService])
  .component('menudropdowncomponent', {template: MenuDropdownView})

export default app.name;
