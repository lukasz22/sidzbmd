import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';
import DocumentService from '../services/document.js';


let app = Angular.module('document.recipe.modal.create', [ExerciseService, CategoryService, UserService, DocumentService]).component('modalDocumentRecipeComponentCreate', {
  template: require('../templates/document-modal.html'),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', '$scope', 'document', '$location', function ($rootScope, UserService, CategoryService, ExerciseService, $scope, DocumentService, $location) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      // $ctrl.items = $ctrl.resolve.items;
      $ctrl.items = {};
      let recipe = {
        "exercise": $ctrl.resolve.items,
        "is_title": true,
        "is_intro": true,
        "is_hint": true,
        "is_solution": true
      };
      $ctrl.modal_title = "Dodawanie zadania dla szablonu dokumentu"
      $ctrl.send_button_name = "Dodaj";
      $ctrl.items.exercise_recipes = [recipe];
      $ctrl.attachments_visible = false;
      $ctrl.document_visible = false;
      $ctrl.document_recipe_visible = true;
      $ctrl.document_recipe_ref_visible = true;
      $ctrl.backendvalid = [{"exercise_recipes": []}]
      // $ctrl.backendvalid[0].exercise_recipes[$index]
      $ctrl.getDocuments();
      // $ctrl.items.exercise_recipes = [];
    };
    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      objCopy.exercise_recipes[0].document = objCopy.exercise_recipes[0].selectedType.id;
      DocumentService.createDocumentRecipe("Document.createDocumentRecipe.modal", [objCopy.exercise_recipes[0]]);
    };

    let registerScope1 = $rootScope.$on('Document.createDocumentRecipe.modal', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
        $ctrl.backendvalid[0].exercise_recipes[0] = data.msg
      }

      });
      $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

    var forms = [
      "form1.tpl.html",
      "form2.tpl.html",
      "form3.tpl.html",
    ];
    $ctrl.displayedForms = [];

    $ctrl.addForm = function(formIndex) {
      $ctrl.displayedForms.push(forms[formIndex]);
    }

    $ctrl.removeForm = function(formIndex) {
      // $ctrl.displayedForms.push(forms[formIndex]);
      // $ctrl.displayedForms.splice(formIndex, 1);
      $ctrl.items.exercise_recipes.splice(formIndex, 1);
    }

    $ctrl.getDocuments = function(){
      DocumentService.getAllDocuments('Document.createDocumentRecipe.modal.getDocuments');
    };

    let registerScope2 = $rootScope.$on('Document.createDocumentRecipe.modal.getDocuments', function(event, data) {
      if (data.type === 'success') {
        $ctrl.items.exercise_recipes[0].document = data.msg;
        $ctrl.items.exercise_recipes[0].selectedType = $ctrl.items.exercise_recipes[0].document[0];

        $ctrl.updatePositionList()
      }
      });
    $scope.$on('$destroy', registerScope2);

    $ctrl.updatePositionList = function(){
      $ctrl.items.exercise_recipes[0].position_number = 1;
      $ctrl.position_list =
        [...Array($ctrl.items.exercise_recipes[0].selectedType.exercise_recipes.length+1).keys()].map(x => ++x)
    }

    $ctrl.goToSingleExercisePage = function(index){
      window.open("exercises/"+$ctrl.items.exercise_recipes[index].exercise.id, '_blank');
      // $location.path("/exercises/"+$ctrl.items.exercise_recipes[index].exercise.id);
    }

    $ctrl.goToSingleDocumentPage = function(document_id){
      window.open("documents/"+document_id, '_blank');
      // $location.path("/exercises/"+$ctrl.items.exercise_recipes[index].exercise.id);
    }
  }]
});

export default app.name;
