import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';


let app = Angular.module('attachment.modal.update', [ExerciseService, CategoryService, UserService ]).component('modalAttachmentComponentUpdate', {
  template: require("../templates/attachment-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', '$scope', '$window', function ($rootScope, UserService, CategoryService, ExerciseService, $scope, $window) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.attachmentRequired=false;
      $ctrl.attachments_visible = false;
      $ctrl.deleteSingleAttVisible = false;
      $ctrl.addForm(0);
      $ctrl.attachment_modal_title = "Edycja załącznika";
      $ctrl.send_button_name = "Edytuj";
      $ctrl.backendvalid = {}
      if (!$ctrl.items.attachments[0].image.base64_string.startsWith("data:image/")) {
        $ctrl.items.attachments[0].image.base64_string = 'data:image/png;base64, '+$ctrl.items.attachments[0].image.base64_string;
      }
      let tmpObj = [
        { "id": "Główna treść zadania",
          "name": "exercise_content"
        },
        { "id": "Wprowadzenie do zadania",
          "name": "exercise_intro"
        },
        { "id": "Wskazówka do zadania",
          "name": "exercise_hint"
        },
        { "id": "Rozwiązanie zadania",
          "name": "exercise_solution"
        }
      ];
      if (!Array.isArray($ctrl.items.attachments[0].attachment_type)) {
        $ctrl.items.attachments[0].selectedType=tmpObj.filter(group => group.name == $ctrl.items.attachments[0].attachment_type)[0];
        $ctrl.items.attachments[0].attachment_type=tmpObj;
      }
    };
    $ctrl.ok = function () {
      $ctrl.items.owner = UserService.getUsername()
      let objCopy = angular.copy($ctrl.items);
      ExerciseService.updateAttachment("Attachment.updateAttachment.modal",
                                        objCopy.attachments[0].exercise,
                                        objCopy.attachments[0])
    };

    let registerScope1 = $rootScope.$on('Attachment.updateAttachment.modal', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
        $ctrl.backendvalid.attachments = [data.msg]
      }

      });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

    $ctrl.loadImageFileAsURL = function(index){
      var fileToLoad = $ctrl.items.attachments[index].file;
      var fileReader = new FileReader();
      fileReader.onload = function (fileLoadedEvent) {
        $ctrl.items.attachments[index].image.base64_string = fileLoadedEvent.target.result;
        $scope.$apply();
      };
      fileReader.readAsDataURL(fileToLoad);
    }

    var forms = [
      "form1.tpl.html",
      "form2.tpl.html",
      "form3.tpl.html",
    ];

    $ctrl.example="haha";
    $ctrl.displayedForms = [];

    $ctrl.addForm = function(formIndex) {
      $ctrl.displayedForms.push(forms[formIndex]);
    }

    $ctrl.removeForm = function(formIndex) {
      // $ctrl.displayedForms.push(forms[formIndex]);
      $ctrl.displayedForms.splice(formIndex, 1);
      $ctrl.items.attachments.splice(formIndex, 1);
    }

    $ctrl.openInNewWindow = function(index) {
      let image = $ctrl.items.attachments[index].image.base64_string
      $window.open(image)
    }

  }]
});

export default app.name;
