import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';

let app = Angular.module('controllers.category.update', [UserService, CategoryService]).component('modalCategoryComponentUpdate', {
  template: require("../templates/category-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', '$scope', 'category', function ($rootScope, UserService, $scope, CategoryService) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.modal_title = "Edycja kategorii";
      $ctrl.send_button_name = "Edytuj";
      $ctrl.backendvalid = {
        "category_name" : "",
        "parent": ""
      }

      CategoryService.getAllCategories('category.getAllCategories');
    };

    let registerScope3 = $rootScope.$on('category.getAllCategories', function(event, data) {
      if (data.type === 'success') {
        data.msg=[{id: 'all', name: 'Wszystkie', full_name: "--Żadna--"}, ...data.msg] ;
        let parent_id = data.msg.filter(category => category.id === $ctrl.items.id)[0].parent.id;
        if (parent_id === undefined) {
          $ctrl.items.parent = data.msg[0]
        }
        else {
          $ctrl.items.parent = data.msg.filter(category => category.id === parent_id)[0];
        }
        $ctrl.items.category_list=data.msg;
      }
    });
    $scope.$on('$destroy', registerScope3);

    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      CategoryService.updateCategory("CategoryService.updateCategory", objCopy)

    };

    let registerScope1 = $rootScope.$on('CategoryService.updateCategory', function(event, data) {
        if (data.type === 'success' ) {
          $ctrl.close({$value: 'success'});
        }
        else if (data.type === 'novalid') { $ctrl.backendtype = data.type
          if (data.msg.hasOwnProperty("parent")){
            $ctrl.backendvalid.parent = data.msg.parent
          }
          else {
            $ctrl.backendvalid.parent = ""
          }
          if (data.msg.hasOwnProperty("category_name")){
            $ctrl.backendvalid.category_name = data.msg.category_name
          }
          else {
            $ctrl.backendvalid.category_name = ""
          }

        }
        else if (data.type === 'danger') {
          $ctrl.close({$value: 'failure'});
        }
    });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };
  }]
});

export default app.name;
