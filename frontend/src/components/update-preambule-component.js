import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';
import PreambuleService from '../services/preambule.js';


let app = Angular.module('preambule.modal.update', [ExerciseService, CategoryService, UserService, PreambuleService]).component('modalPreambuleComponent', {
  template: require("../templates/preambule-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', 'preambule', '$scope', function ($rootScope, UserService, CategoryService, ExerciseService, PreambuleService, $scope) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      $ctrl.attachments_visible = false;
      $ctrl.deleteSingleAttVisible = false;
      $ctrl.modal_title = "Edycja preambuły";
      $ctrl.send_button_name = "Edytuj";
      $ctrl.backendvalid = {
        "preambule_title" : "",
        "content": ""
      }
    };
    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      PreambuleService.updatePreambule("Preambule.updatePreambule.modal", objCopy)
    };

    let registerScope1 = $rootScope.$on('Preambule.updatePreambule.modal', function(event, data) {
        if (data.type === 'success' ) {
          $ctrl.close({$value: data});
        }
        else if (data.type === 'novalid') { $ctrl.backendtype = data.type
          $ctrl.backendvalid = data.msg
        }


      });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

  }]
});

export default app.name;
