import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';
import DocumentService from '../services/document.js';
import AngularDragula from 'angularjs-dragula'

import AutoScroll from 'dom-autoscroller'


let app = Angular.module('document.modal.create', [ExerciseService, CategoryService, UserService, DocumentService, AngularDragula(Angular)]).component('modalDocumentComponentCreate', {
  template: require("../templates/document-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', '$scope', 'document', '$location', 'dragulaService', function ($rootScope, UserService, CategoryService, ExerciseService, $scope, DocumentService, $location, dragulaService) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.isDragged  = false
      let container = document.querySelector('#list-container');

      let scroll = AutoScroll([
        container
      ],{
        margin: 50,
        maxSpeed: 7,
        scrollWhenOutside: true,
        autoScroll: function() {
          return $ctrl.isDragged
        }
      })

      $ctrl.items = $ctrl.resolve.items;
      let onlyExerciesRecipes = $ctrl.resolve.onlyExerciesRecipes;
      $ctrl.onlyExRecipesMode = onlyExerciesRecipes
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      if ($ctrl.onlyExRecipesMode) {
        $ctrl.modal_title = "Dodawanie zadań dla szablonu dokumentu"
        $ctrl.items.positions = [{id: 1,
                                  name: "Na początku"},
                                 {id: 2,
                                  name: "Na końcu"}]
        $ctrl.items.selectedPosition = $ctrl.items.positions[0];
        $ctrl.getDocuments();
      }
      else {
        $ctrl.modal_title = "Nowy szablon dokumentu"
      }

      $ctrl.send_button_name = "Utwórz";
      $ctrl.attachments_visible = false;

      $ctrl.document_visible = !$ctrl.onlyExRecipesMode;
      $ctrl.document_recipe_visible = true;
      $ctrl.document_recipe_ref_visible = false;
      $ctrl.items.exercise_recipes = [];
      $ctrl.items.is_title_part = true;

      $ctrl.selectVisibity = {
        'is_title': [true, "Widoczność wszystkich tytułów zadań"],
        'is_intro': [false, "Widoczność wszystkich wprowadzeń zadań"],
        'is_hint': [false, "Widoczność wszystkich wskazówek zadań"],
        'is_solution': [false, "Widoczność wszystkich rozwiązań zadań"]
      }
      let id_list = [];
      $ctrl.backendvalid = {}
      ExerciseService.getSelectedExercises('document.create.getSelectedExercises');
    };

    let registerScope1 = $rootScope.$on('document.create.getSelectedExercises', function(event, data) {
      if (data.type === 'success') {
        for (var index in data.msg.exercises) {
          if (data.msg.exercises.hasOwnProperty(index)) {
            $ctrl.items.exercise_recipes.push({"exercise": data.msg.exercises[index],
                                                "is_title": $ctrl.selectVisibity.is_title[0],
                                                "is_intro": $ctrl.selectVisibity.is_intro[0],
                                                "is_hint": $ctrl.selectVisibity.is_hint[0],
                                                "is_solution": $ctrl.selectVisibity.is_solution[0]});
          }
        }
      }
    });
    $scope.$on('$destroy', registerScope1);

    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      objCopy.attachments = []; // temporary
      if ($ctrl.onlyExRecipesMode) {
        for (var index in objCopy.exercise_recipes) {
          if (objCopy.exercise_recipes.hasOwnProperty(index)) {
            objCopy.exercise_recipes[index].document = $ctrl.items.selectedType.id;
            if ($ctrl.items.selectedPosition.id == 1) {
              objCopy.exercise_recipes[index].position_number = 1
            }

          }
        }
        if ($ctrl.items.selectedPosition.id == 1) {
          objCopy.exercise_recipes = objCopy.exercise_recipes.reverse()
        }
        DocumentService.createDocumentRecipe("Document.createDocumentRecipe.modal", objCopy.exercise_recipes);
      }
      else {
        DocumentService.createDocument("Document.createDocument.modal", objCopy)
      }
    };

    let registerScope2 = $rootScope.$on('Document.createDocument.modal', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
        $ctrl.backendvalid = [data.msg]
      }
      });
    $scope.$on('$destroy', registerScope2);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

    var forms = [
      "form1.tpl.html",
      "form2.tpl.html",
      "form3.tpl.html",
    ];
    // $ctrl.single_form = "document.exercise_recipes.tpl.html";
    $ctrl.example="haha";
    $ctrl.displayedForms = [];

    $ctrl.addForm = function(formIndex) {
      $ctrl.displayedForms.push(forms[formIndex]);
    }

    $ctrl.removeForm = function(formIndex) {
      // $ctrl.displayedForms.push(forms[formIndex]);
      // $ctrl.displayedForms.splice(formIndex, 1);
      $ctrl.items.exercise_recipes.splice(formIndex, 1);
    }

    $ctrl.goToSingleExercisePage = function(index){
      window.open("exercises/"+$ctrl.items.exercise_recipes[index].exercise.id, '_blank');
      // $location.path("/exercises/"+$ctrl.items.exercise_recipes[index].exercise.id);
    }
    $scope.$on('exercise-table2.drag', function (event, element) {

      $ctrl.isDragged = true
      let dragulatests = document.querySelectorAll('.dragulatest');
      dragulatests.forEach(function (onediv, index) {
        onediv.style.display = "none"
      });

    });

    $scope.$on('exercise-table2.dragend', function (event, element) {
      $ctrl.isDragged = false
      let dragulatests = document.querySelectorAll('.dragulatest');
      dragulatests.forEach(function (onediv, index) {
        onediv.style.display = "block"
      });
    });
    dragulaService.options($scope, 'exercise-table2', {
      moves: function (el, container, handle) {
        return handle.closest('.exerciseheaderhandler');
      }
    });

    $ctrl.getDocuments = function(){
      DocumentService.getAllDocuments('Document.createDocumentComponent.modal.getDocuments');
    };

    $ctrl.selectAllIntro = function(key){
      $ctrl.items.exercise_recipes.forEach(function (onediv, index) {
        if ($ctrl.selectVisibity[key][0] === true || $ctrl.selectVisibity[key][0] === false) {
          onediv[key] = $ctrl.selectVisibity[key][0];
        }
      });
    };

    let registerScope3 = $rootScope.$on('Document.createDocumentComponent.modal.getDocuments', function(event, data) {
      if (data.type === 'success') {
        $ctrl.items.document = data.msg;
        $ctrl.items.selectedType = $ctrl.items.document[0];

        // $ctrl.updatePositionList()
      }
      });
    $scope.$on('$destroy', registerScope3);

    let registerScope4 = $rootScope.$on('Document.createDocumentRecipe.modal', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
        $ctrl.backendvalid = [data.msg]
      }
      });
    $scope.$on('$destroy', registerScope4);

  }]
});

export default app.name;
