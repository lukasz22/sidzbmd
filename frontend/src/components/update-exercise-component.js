import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';


let app = Angular.module('exercise.modal.update', [ExerciseService, CategoryService, UserService ]).component('modalExerciseComponent', {
  template: require("../templates/exercise-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', '$scope', function ($rootScope, UserService, CategoryService, ExerciseService, $scope) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      $ctrl.attachments_visible = false;
      $ctrl.deleteSingleAttVisible = false;
      $ctrl.modal_title = "Edycja zadania";
      $ctrl.send_button_name = "Edytuj";
      $ctrl.isAdmin = UserService.isAdministrator();
      $ctrl.backendvalid = {
        "title" : "",
        "intro": "",
        "task_content" : "",
        "hint": "",
        "solution" : "",
        "owner" : ""
      }
    };
    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      objCopy.attachments = []; // temporary
      ExerciseService.updateExercise("Exercise.updateExercise.modal", objCopy,
                                     $ctrl.selectedLanguage, $ctrl.selectedCategory,
                                     $ctrl.selectedPolicy)
    };

    let registerScope1 = $rootScope.$on('Exercise.updateExercise.modal', function(event, data) {
        if (data.type === 'success' ) {
          $ctrl.items.category = $ctrl.selectedCategory;
          $ctrl.items.language = $ctrl.selectedLanguage;
          $ctrl.close({$value: 'success'});
        }
        else if (data.type === 'novalid') { $ctrl.backendtype = data.type
          $ctrl.backendvalid = data.msg
        }


      });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

    $ctrl.getAccessGroupsProfile = function(){
      UserService.getAccessGroupsProfile('user.getAccessGroupsProfile.modal');
    };

    let registerScope2 = $rootScope.$on('user.getAccessGroupsProfile.modal', function(event, data) {
      if (data.type === 'success') {
        $ctrl.accessgroup_list=data.msg;
        $ctrl.selectedPolicy=[]
        for (var policy of $ctrl.items.policies) {
          let result = $ctrl.accessgroup_list.filter(group => group.id == policy.id)[0];
          $ctrl.selectedPolicy.push(result)
        }
      }
      });
    $scope.$on('$destroy', registerScope2);

    $ctrl.getLanguages = function(){
      CategoryService.getAllLanguages('exercise.getAllLanguages.modal');
    };

    let registerScope3 = $rootScope.$on('exercise.getAllLanguages.modal', function(event, data) {
      if (data.type === 'success') {
        $ctrl.language_list=data.msg;
        $ctrl.selectedLanguage=$ctrl.language_list.filter(group => group.id == $ctrl.items.language.id)[0];
      }
      });
    $scope.$on('$destroy', registerScope3);

    $ctrl.getCategories = function(){
      CategoryService.getAllCategories('exercise.getAllCategoriesTree.modal');
    };

    let registerScope4 = $rootScope.$on('exercise.getAllCategoriesTree.modal', function(event, data) {
      if (data.type === 'success') {
        data.msg=[{id: 'all', name: 'Wszystkie', full_name: "--Żadna--"}, ...data.msg] ;
        $ctrl.category_list=data.msg;
        if ($ctrl.items.category.id === undefined) {
          $ctrl.selectedCategory = data.msg[0]
        }
        else {
        $ctrl.selectedCategory=$ctrl.category_list.filter(group => group.id == $ctrl.items.category.id)[0];
        }

      }
      });
    $scope.$on('$destroy', registerScope4);

    $ctrl.getLanguages();
    $ctrl.getCategories();
    $ctrl.getAccessGroupsProfile();

    var forms = [
      "form1.tpl.html",
      "form2.tpl.html",
      "form3.tpl.html",
    ];

    $ctrl.example="haha";
    $ctrl.displayedForms = [];

    $ctrl.addForm = function(formIndex) {
      $ctrl.displayedForms.push(forms[formIndex]);
    }

    $ctrl.removeForm = function(formIndex) {
      // $ctrl.displayedForms.push(forms[formIndex]);
      $ctrl.displayedForms.splice(formIndex, 1);
    }

    $ctrl.goToSingleUserPermissionPage = function(id){
      window.open("user/accessprofiles/"+id, '_blank');
      // $location.path("/exercises/"+$ctrl.items.exercise_recipes[index].exercise.id);
    }

  }]
});

export default app.name;
