import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';

let app = Angular.module('controllers.category.create', [UserService, CategoryService]).component('modalCategoryComponentCreate', {
  template: require("../templates/category-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', '$scope', 'category', function ($rootScope, UserService, $scope, CategoryService) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.modal_title = "Nowa kategoria";
      $ctrl.send_button_name = "Utwórz";
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      $ctrl.backendvalid = {}

      CategoryService.getAllCategories('category.getAllCategories');
    };

    let registerScope3 = $rootScope.$on('category.getAllCategories', function(event, data) {
      if (data.type === 'success') {
        data.msg=[{id: 'all', name: 'Wszystkie', full_name: "--Żadna--"}, ...data.msg] ;
        let parent_id = data.msg.filter(category => category.id === $ctrl.items.id)[0];
        $ctrl.items.parent =parent_id;
        $ctrl.items.category_name="";
        $ctrl.items.category_list=data.msg;
        $ctrl.backendvalid = {
          "name" : "",
          "parent": ""
        }
      }
    });
    $scope.$on('$destroy', registerScope3);

    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      CategoryService.addCategory("CategoryService.createCategory", objCopy)
    };

    let registerScope1 = $rootScope.$on('CategoryService.createCategory', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') {
        $ctrl.backendvalid = data.msg
        $ctrl.backendtype = data.type
      }
      else if (data.type === 'danger') {
        $ctrl.close({$value: 'failure'});
      }
    });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };
  }]
});

export default app.name;
