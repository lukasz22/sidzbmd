import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';


let app = Angular.module('attachment.modal.create', [ExerciseService, CategoryService, UserService ]).component('modalAttachmentComponentCreate', {
  template: require("../templates/attachment-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', '$scope', '$window', function ($rootScope, UserService, CategoryService, ExerciseService, $scope, $window) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.exercise_id = $ctrl.resolve.exercise_id;
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      $ctrl.attachmentRequired=true;
      $ctrl.items.attachments = [];
      $ctrl.attachments_visible = false;
      $ctrl.addForm(0);
      $ctrl.attachment_modal_title = "Nowy załącznik";
      $ctrl.send_button_name = "Utwórz";
      $ctrl.items.attachments[0].selectedType = $ctrl.items.attachments[0].attachment_type[0]
      $ctrl.backendvalid = {}
    };
    $ctrl.ok = function () {
      // $ctrl.items.attachments = []; // temporary
      $ctrl.items.owner = UserService.getUsername()
      let objCopy = angular.copy($ctrl.items);
      ExerciseService.createAttachments("Attachment.createAttachments.modal",
                                        $ctrl.exercise_id,
                                        objCopy.attachments)
    };
    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

    let registerScope1 = $rootScope.$on('Attachment.createAttachments.modal', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
        $ctrl.backendvalid.attachments = data.msg
      }

      });
    $scope.$on('$destroy', registerScope1);

    $ctrl.loadImageFileAsURL = function(index){
      var fileToLoad = $ctrl.items.attachments[index].file;
      var fileReader = new FileReader();
      fileReader.onload = function (fileLoadedEvent) {
          $ctrl.items.attachments[index].image.base64_string = fileLoadedEvent.target.result;
            $ctrl.items.attachments[index].image.base64_string =
                $ctrl.items.attachments[index].image.base64_string.replace("data:application/octet-stream;base64", 'data:image/png;base64')
          $scope.$apply();
      };
      fileReader.readAsDataURL(fileToLoad);
    }

    var forms = [
      "form1.tpl.html",
      "form2.tpl.html",
      "form3.tpl.html",
    ];

    $ctrl.displayedForms = [];

    $ctrl.addForm = function(formIndex) {
      $ctrl.displayedForms.push(forms[formIndex]);
      var obj = {};
      obj.image={};
      obj.attachment_type=[
        { "id": "Główna treść zadania",
          "name": "exercise_content"
        },
        { "id": "Wprowadzenie do zadania",
          "name": "exercise_intro"
        },
        { "id": "Wskazówka do zadania",
          "name": "exercise_hint"
        },
        { "id": "Rozwiązanie zadania",
          "name": "exercise_solution"
        }
      ];
      $ctrl.items.attachments.splice(formIndex, 0, obj);
    }

    $ctrl.removeForm = function(formIndex) {
      // $ctrl.displayedForms.push(forms[formIndex]);
      $ctrl.displayedForms.splice(formIndex, 1);
      $ctrl.items.attachments.splice(formIndex, 1);
    }

    $ctrl.openInNewWindow = function(index) {
      let image = $ctrl.items.attachments[index].image.base64_string
      if (image) {
        $window.open(image)
      }
    }
  }]
});

export default app.name;
