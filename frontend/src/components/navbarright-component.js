import Angular from 'angular';
import AngularBootstrap from 'angular-ui-bootstrap';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import ngTouch from 'angular-touch';

import NavbarRightView from '../templates/navbarright-view.html';



let app = Angular.module('nav.navbarRightComponent', [AngularBootstrap, ngAnimate, ngSanitize, ngTouch])
  .component('navbarright', {template: NavbarRightView})

export default app.name;
