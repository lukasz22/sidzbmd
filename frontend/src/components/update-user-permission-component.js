import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';

let app = Angular.module('controllers.userpermgroupprofile.update', [UserService]).component('modalComponent', {
  template: require("../templates/user-permission-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', '$scope', function ($rootScope, UserService, $scope) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.modal_title = "Edycja profilu dostępu";
      $ctrl.send_button_name = "Edytuj";
      $ctrl.isAdmin = UserService.isAdministrator();
      $ctrl.backendvalid = {}
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
    };
    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      UserService.updateAccessGroupsProfile("UserService.updateAccessGroupsProfile", objCopy, $ctrl.selected)
    };

    let registerScope1 = $rootScope.$on('UserService.updateAccessGroupsProfile', function(event, data) {
        if (data.type === 'success' ) {
          $ctrl.items.group = $ctrl.selected;
          $ctrl.close({$value: 'success'});
        }
        else if (data.type === 'novalid') { $ctrl.backendtype = data.type
          $ctrl.backendvalid = data.msg
        }
      });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };
    $ctrl.getUserGroups = function(){
      UserService.getAllGroups('user.getAllGroups.modal');
    };

    let registerScope2 = $rootScope.$on('user.getAllGroups.modal', function(event, data) {
      if (data.type === 'success') {
        $ctrl.group_list=data.msg;
        $ctrl.selected=$ctrl.group_list.filter(group => group.id == $ctrl.items.group.id)[0];
      }
    });
    $scope.$on('$destroy', registerScope2);

    $ctrl.getUserGroups();
  }]
});

export default app.name;
