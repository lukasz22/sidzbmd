import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';
import DocumentService from '../services/document.js';
import AngularDragula from 'angularjs-dragula'

import AutoScroll from 'dom-autoscroller'

let app = Angular.module('document.modal.update', [ExerciseService, CategoryService, UserService, DocumentService, AngularDragula(Angular)]).component('modalDocumentComponentUpdate', {
  template: require("../templates/document-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', 'category','exercise', '$scope', 'document', '$location', 'dragulaService', function ($rootScope, UserService, CategoryService, ExerciseService, $scope, DocumentService, $location, dragulaService) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.isDragged  = false
      let container = document.querySelector('#list-container');
      let scroll = AutoScroll([
        container
      ],{
        margin: 50,
        maxSpeed: 7,
        scrollWhenOutside: true,
        autoScroll: function() {
          return $ctrl.isDragged
        }
      })

      $ctrl.items = $ctrl.resolve.items;
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      $ctrl.modal_title = "Edycja szablonu dokumentu"
      $ctrl.send_button_name = "Edytuj";
      $ctrl.attachments_visible = false;

      $ctrl.document_visible = true;
      $ctrl.document_recipe_visible = true;
      $ctrl.document_recipe_ref_visible = false;
      $ctrl.backendvalid = {}

      $ctrl.selectVisibity = {
        'is_title': [null, "Widoczność wszystkich tytułów zadań"],
        'is_intro': [null, "Widoczność wszystkich wprowadzeń zadań"],
        'is_hint': [null, "Widoczność wszystkich wskazówek zadań"],
        'is_solution': [null, "Widoczność wszystkich rozwiązań zadań"]
      }

    };
    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      objCopy.attachments = []; // temporary
      DocumentService.updateDocument("Document.updateDocument.modal", objCopy)
    };

    let registerScope1 = $rootScope.$on('Document.updateDocument.modal', function(event, data) {
      if (data.type === 'success' ) {
        $ctrl.close({$value: 'success'});
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
        $ctrl.backendvalid = data.msg
      }
      });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };

    var forms = [
      "form1.tpl.html",
      "form2.tpl.html",
      "form3.tpl.html",
    ];
    // $ctrl.single_form = "document.exercise_recipes.tpl.html";
    $ctrl.example="haha";
    $ctrl.displayedForms = [];

    $ctrl.addForm = function(formIndex) {
      $ctrl.displayedForms.push(forms[formIndex]);
    }

    $ctrl.removeForm = function(formIndex) {
      // $ctrl.displayedForms.push(forms[formIndex]);
      // $ctrl.displayedForms.splice(formIndex, 1);
      $ctrl.items.exercise_recipes.splice(formIndex, 1);
    }

    $ctrl.goToSingleExercisePage = function(index){
      window.open("exercises/"+$ctrl.items.exercise_recipes[index].exercise.id, '_blank');
      // $location.path("/exercises/"+$ctrl.items.exercise_recipes[index].exercise.id);
    }

    $scope.$on('exercise-table2.drag', function (event, element) {

      $ctrl.isDragged = true
      let dragulatests = document.querySelectorAll('.dragulatest');
      dragulatests.forEach(function (onediv, index) {
        onediv.style.display = "none"
      });

    });

    $scope.$on('exercise-table2.dragend', function (event, element) {
      $ctrl.isDragged = false
      let dragulatests = document.querySelectorAll('.dragulatest');
      dragulatests.forEach(function (onediv, index) {
        onediv.style.display = "block"
      });
    });

    dragulaService.options($scope, 'exercise-table2', {
      moves: function (el, container, handle) {
        return handle.closest('.exerciseheaderhandler');
      }
    });

    $ctrl.selectAllIntro = function(key){
      $ctrl.items.exercise_recipes.forEach(function (onediv, index) {
        if ($ctrl.selectVisibity[key][0] === true || $ctrl.selectVisibity[key][0] === false) {
          onediv[key] = $ctrl.selectVisibity[key][0];
        }
      });
    };

  }]
});

export default app.name;
