import Angular from 'angular';
import ExerciseService from '../services/exercise.js';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';

let app = Angular.module('controllers.userpermgroupprofile.create', [UserService]).component('modalComponentCreate', {
  template: require("../templates/user-permission-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope','user', '$scope', function ($rootScope, UserService, $scope) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.modal_title = "Nowy profil dostępu";
      $ctrl.send_button_name = "Utwórz";
      $ctrl.isAdmin = UserService.isAdministrator();
      $ctrl.backendvalid = {}
      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
    };
    $ctrl.ok = function () {
      let objCopy = angular.copy($ctrl.items);
      UserService.createAccessGroupsProfile("UserService.createAccessGroupsProfile", objCopy, $ctrl.selected)
    };

    let registerScope1 = $rootScope.$on('UserService.createAccessGroupsProfile', function(event, data) {
        if (data.type === 'success' ) {
          $ctrl.items.group = $ctrl.selected;
          $ctrl.close({$value: 'success'});
        }
        else if (data.type === 'novalid') { $ctrl.backendtype = data.type
          $ctrl.backendvalid = data.msg
        }
    });
    $scope.$on('$destroy', registerScope1);

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };
    $ctrl.getUserGroups = function(){
      UserService.getAllGroups('user.getAllGroups.modal');
    };

    let registerScope2 = $rootScope.$on('user.getAllGroups.modal', function(event, data) {
      if (data.type === 'success') {
        $ctrl.group_list=data.msg;
        $ctrl.selected=$ctrl.group_list[0];
      }
      });
    $scope.$on('$destroy', registerScope2);

    $ctrl.getUserGroups();

    $ctrl.updateOtherButtons = function(){
      if ($ctrl.items.exercise_vis === false) {
        $ctrl.items.intro_vis = false;
        $ctrl.items.hint_vis = false;
        $ctrl.items.solution_vis = false;
      }
    };

    $ctrl.changeFirstButtons = function(){
      $ctrl.items.exercise_vis = true;
    };

  }]
});

export default app.name;
