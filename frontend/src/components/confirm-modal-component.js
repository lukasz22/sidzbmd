import Angular from 'angular';

let app = Angular.module('controllers.confirm.modal', []).component('modalComponentConfirm', {
  template: require("../templates/confirm-modal.html"),
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: ['$rootScope', '$scope', '$timeout', function ($rootScope, $scope, $timeout) {
    var $ctrl = this;
    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.modal_title = $ctrl.items.header;
      $ctrl.body = $ctrl.items.body
      $ctrl.send_button_name = "Tak";

      // $ctrl.selected = {
      //   item: $ctrl.items.name
      // };
      $ctrl.backendvalid = {}

      if ($ctrl.items.exp) {
        $ctrl.is_showed = 0;
        $ctrl.timer_value = $ctrl.items.exp - (new Date()).getTime() / 1000|0;;
        $ctrl.body = $ctrl.items.body + ' ' + $ctrl.timer_value + 's. Czy chcesz go odświeżyć? Brak reakcji spowoduje wylogowanie z serwisu.'
        $timeout(function() {$ctrl.timer($ctrl)}, 1000);
      }
      else {
        $ctrl.is_showed = 1;
      }

      $ctrl.timer = function($ctrl){
        $ctrl.timer_value--;
        $ctrl.body = $ctrl.items.body + ' ' + $ctrl.timer_value + 's. Czy chcesz go odświeżyć? Brak reakcji spowoduje wylogowanie z serwisu.'
        if ($ctrl.timer_value <= 0) {
          $ctrl.dismiss({$value: 'cancel'});
        }
        else {
          $timeout(function() {$ctrl.timer($ctrl)}, 1000);
        }
      }

    };

    $ctrl.ok = function () {
      $ctrl.close({$value: 'success'});
    };

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };
  }]
});

export default app.name;
