import Angular from 'angular';
import AngularRouter from 'angular-route';

import Routing from './app.config';

import Domain from './domain.config';

import NavApp from './apps/nav/nav-app.js'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap-theme.css'

// import 'highlight.js/styles/atom-one-dark.css'
import 'highlight.js/styles/gruvbox-light.css'

// gruvbox-light.css
import 'angular-ui-tree/dist/angular-ui-tree.min.css'
import 'angular-ui-tree/dist/angular-ui-tree.js'

import 'highlightjs/highlight.pack.min.js'
import 'angular-highlightjs/angular-highlightjs.min.js'

require('./css/exercise.css')
require('./css/validation-style.css')

import HomeHTML from './templates/home.html'
import ExerciseViewHTML from './templates/exercise-view.html'

import DocumentViewHTML from './templates/document-view.html'
import PreambuleViewHTML from './templates/preambule-view.html'
import UserGroupsViewHTML from './templates/usergroups-view.html'
import UserPermGroupProfileViewHTML from './templates/userPermGroupProfile-view.html'

import AboutViewHTML from './templates/about-view.html'

import PageNotFoundHTML from './templates/pageNotFound.html'
import AlertController from './controllers/alert.js'
import MenuDropDownComponent from './components/menudropdown-component.js'
import TreeController from './controllers/menudropdown-controller.js'
import ExerciseController from './controllers/exercise-controller.js'
import DocumentController from './controllers/document-controller.js'
import UserGroupsController from './controllers/usergroups-controller.js'
import PreambuleController from './controllers/preambule-controller.js'
import HomeController from './controllers/home-controller.js'

import UserPermGroupProfileController from './controllers/userpermgroupprofile-controller.js'

import ExerciseService from './services/exercise.js';
import FileDirective from './directives/file-directive.js';
import AttachmentCreateComponent from './components/create-attachment-component.js';
import ExerciseCreateComponent from './components/create-exercise-component.js';
import ExerciseUpdateComponent from './components/update-exercise-component.js';
import AttachmentUpdateComponent from './components/update-attachment-component.js';
import DocumentUpdateComponent from './components/update-document-component.js';
import DocumentRecipeCreateComponent from './components/create-document-recipe-component.js';
import DocumentCreateComponent from './components/create-document-component.js';
import UserPermissionCreateComponent from './components/create-user-permission-component.js';
import UserPermissionUpdateComponent from './components/update-user-permission-component.js';
import CategoryCreateComponent from './components/create-category-component.js';
import CategoryUpdateComponent from './components/update-category-component.js';
import ConfirmModalComponent from './components/confirm-modal-component.js';
import PreambuleModalComponent from './components/update-preambule-component.js';


// import CategoryService from './services/categories.js'
let AngularUiTree = 'ui.tree'

let AngularDragula = require('angularjs-dragula');

let app = Angular.module('index', [
	AngularDragula(Angular),
	AngularRouter,
	'ui.bootstrap',
	NavApp,
	AlertController,
	AngularUiTree,
	MenuDropDownComponent,
	TreeController,
	ExerciseController,
	DocumentController,
	ExerciseService,
	UserGroupsController,
	UserPermGroupProfileController,
	HomeController,
	FileDirective,
	AttachmentCreateComponent,
	ExerciseCreateComponent,
	ExerciseUpdateComponent,
	AttachmentUpdateComponent,
	DocumentUpdateComponent,
	DocumentRecipeCreateComponent,
	DocumentCreateComponent,
	UserPermissionCreateComponent,
	UserPermissionUpdateComponent,
	CategoryCreateComponent,
	CategoryUpdateComponent,
	ConfirmModalComponent,
	'hljs',
	PreambuleController,
	PreambuleModalComponent
	// CategoryService

]).config(Routing);
app.value('$domainName', Domain.name);

app.config(function($routeProvider){
	$routeProvider
	.when("/", {
		template : HomeHTML
	})
	.when("/home", {
		template : HomeHTML
	})
	.when("/user/groups", {
		template : UserGroupsViewHTML
	})
	.when("/user/accessprofiles", {
		template : UserPermGroupProfileViewHTML
	})
	.when("/user/accessprofiles/:userPermissionId", {
		template : UserPermGroupProfileViewHTML
	})
	.when("/exercises", {
		template : ExerciseViewHTML
	})
	.when("/exercises/:exerciseId", {
		template : ExerciseViewHTML
	})
	.when("/documents", {
		template : DocumentViewHTML
	})
	.when("/documents/:documentId", {
		template : DocumentViewHTML
	})
	.when("/preambles", {
		template : PreambuleViewHTML
	})
	.when("/about", {
		template : AboutViewHTML
	})
	.when("/pageNotFound", {
		template : PageNotFoundHTML
	}).
	otherwise({template: PageNotFoundHTML});
});

app.run(function(uibPaginationConfig){
   // uibPaginationConfig.firstText='MY FIRST';
   // uibPaginationConfig.previousText='YOUR TEXT';
	 uibPaginationConfig.firstText='Na początek';
	 uibPaginationConfig.previousText='Poprzednia';
	 uibPaginationConfig.nextText='Następna';
	 uibPaginationConfig.lastText='Na koniec';

})

app.config(function (hljsServiceProvider) {
  hljsServiceProvider.setOptions({
    // replace tab with 4 spaces
		// useBR: true,
    tabReplace: '    '

  });
});
