import Angular from 'angular';
import UserService from './user.js';


let app = Angular.module('service.preambule', []);

app.service('preambule',
  ["$domainName","$http","$location","$sessionStorage","$rootScope", '$log', 'user',
  function($domainName,$http, $location, $sessionStorage, $rootScope, $log, UserService) {
    let DOMAIN_NAME=$domainName;
    let documents = this;

    this.getPreambule = function (message, preambule_id) {
      $log.debug(message);

      // let intGetPreambule = function(message, preambule_id) {
        $http.get(DOMAIN_NAME+'preambles/'+preambule_id+'/',{
          headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
        }).then(function(response){
          // 'user.getLoggedUserData'
            $rootScope.$emit(message, {type: 'success', msg: response.data});
        }, function() {
          $rootScope.$emit(message+'.notfound', {type: 'danger', msg: "Dokument nie istnieje"});
        });
      // };

      // let opaqueFun = function(){
      //   intGetPreambule(message, preambule_id);
      // };
      // UserService.check_token_expiration(opaqueFun);

    };

    this.updatePreambule = function (message, preambule) {
      $rootScope.$emit('LOAD')
      $http.put(DOMAIN_NAME+'preambles/'+preambule.id+'/', preambule, {

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      },
      function(response){
        $rootScope.$emit('UNLOAD')
        documents.validate_400_response(message, response)
      });
    };

    this.validate_400_response = function(message, response){
      if (response.status === 400) {
        $rootScope.$emit(message, {type: 'novalid', msg: response.data});
      }
      else {
        console.log('inny kod bledu');
      }
    }

}]);


export default app.name;
