import Angular from 'angular';

let app = Angular.module('service.exercise', []);

app.service('exercise',
  ["$domainName","$http","$location","$sessionStorage","$rootScope", '$log', '$window',
  function($domainName, $http, $location, $sessionStorage, $rootScope, $log, $window) {
    let DOMAIN_NAME=$domainName;
    let exercises = this;
    this.updateExerciseTable = function(index, value){
      // $sessionStorage.put('user_token', token);
      // $sessionStorage.remove('user_token');
      // $sessionStorage.get('user_token');
        let obj = JSON.parse($window.localStorage.getItem('exercise_table'));
        if (!obj) {
          obj = {};
        }
        obj[index] = value;
        $window.localStorage.setItem('exercise_table', JSON.stringify(obj));
        // $rootScope.$emit('ExerciseService.updateExerciseTable', {type: 'success', msg: 'success'});
    }

    this.clearExerciseTable = function(){
        $window.localStorage.setItem('exercise_table', JSON.stringify({}));
    }


    this.getExerciseTable = function(){
        let obj = JSON.parse($window.localStorage.getItem('exercise_table'));
        if (!obj) {
          obj = {};
        }
        return obj;
    }

    this.getExercises = function (message, query) {
      $http.get(DOMAIN_NAME+'exercises/find/'+query,{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.getExercise = function (message, exercise_id) {
      $http.get(DOMAIN_NAME+'exercises/'+exercise_id+'/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function() {
        $rootScope.$emit(message+'.notfound', {type: 'danger', msg: "Zadanie nie istnieje"});
    });
    };
    this.getNumSelectedExercise = function(){
      let id_list = this.getExerciseTable();
      let query='';
      let counter = 0;
      for (var index in id_list) {
        if (id_list.hasOwnProperty(index)  && id_list[index] === true) {
          counter++
        }
      }
      return counter
    }
    this.getSelectedExercises = function (message) {
      let id_list = this.getExerciseTable();
      let query='';
      for (var index in id_list) {
        if (id_list.hasOwnProperty(index)  && id_list[index] === true) {
          query=query+'&id='+index;
        }
      }
      $http.get(DOMAIN_NAME+'exercises/find/?'+query.substring(1),{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.updateExercise = function(message, exercise, selected_language,
                                   selected_category, selected_policies) {
      // profile.group = selected_group.id;
      exercise.owner=exercise.owner.username;
      if (selected_category.id === 'all') {
        exercise.category = null;
      }
      else {
        exercise.category = selected_category.id;
      }
      exercise.language = selected_language.id;

      let selected_policy_id=[];
      for (var policy of selected_policies) {
        selected_policy_id.push(policy.id)
      }
      exercise.policies = selected_policy_id;
      $rootScope.$emit('LOAD')
      $http.put(DOMAIN_NAME+'exercises/'+exercise.id+'/', exercise, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        exercises.validate_400_response(message, response)
      });
    }

    this.deleteExercise = function(message, exercise) {
      // profile.group = selected_group.id;
      $http.delete(DOMAIN_NAME+'exercises/'+exercise.id+'/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.createExercise = function(message, exercise, selected_language,
                                   selected_category, selected_policies) {
      // profile.group = selected_group.id;
      exercise.owner=exercise.owner.username;
      if (selected_category.id === 'all') {
        exercise.category = null;
      }
      else {
        exercise.category = selected_category.id;
      }
      exercise.language = selected_language.id;

      for (var index of Object.keys(exercise.attachments)) {
        exercise.attachments[index].image = exercise.attachments[index].image.base64_string;
        exercise.attachments[index].attachment_type = exercise.attachments[index].selectedType.name;
      }

      let selected_policy_id=[];
      for (var policy of selected_policies) {
        selected_policy_id.push(policy.id)
      }
      exercise.policies = selected_policy_id;
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'exercises/', exercise, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        exercises.validate_400_response(message, response)
      });
    }

    this.validate_400_response = function(message, response){
      if (response.status === 400) {
        $rootScope.$emit(message, {type: 'novalid', msg: response.data});
      }
      else {
        console.log('inny kod bledu');
      }
    }


    this.createAttachments = function(message, exercise_id, attachments) {
      for (var index of Object.keys(attachments)) {
        attachments[index].image = attachments[index].image.base64_string;
        attachments[index].attachment_type = attachments[index].selectedType.name;
      }
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'exercises/'+exercise_id+'/attachments/', attachments, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        exercises.validate_400_response(message, response)
      });
    }

    this.updateAttachment = function(message, exercise_id, attachment) {
     attachment.image = attachment.image.base64_string;
     attachment.attachment_type = attachment.selectedType.name;
      $rootScope.$emit('LOAD')
      $http.put(DOMAIN_NAME+'exercises/'+exercise_id+'/attachments/'+attachment.id+'/', attachment, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        exercises.validate_400_response(message, response)
      });
    }

    this.deleteAttachments = function(message, exercise_id) {

      $http.delete(DOMAIN_NAME+'exercises/'+exercise_id+'/attachments/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }
    this.deleteAttachment = function(message, exercise_id, attachment_id) {

      $http.delete(DOMAIN_NAME+'exercises/'+exercise_id+'/attachments/'+attachment_id+'/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }
}]);


export default app.name;
