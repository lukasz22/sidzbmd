import Angular from 'angular';

let app = Angular.module('shared.categories', []);

app.service('category',
  ["$domainName","$http","$location","$sessionStorage","$rootScope",
  function($domainName, $http, $location, $sessionStorage, $rootScope) {
    let DOMAIN_NAME=$domainName;
    let categories = this;

    this.getAllCategoriesTree = function (message) {
      $http.get(DOMAIN_NAME+'categories/treeall/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.getAllCategories = function (message) {
      $http.get(DOMAIN_NAME+'categories/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.getAllLanguages = function (message) {
      $http.get(DOMAIN_NAME+'languages/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };


    this.deleteSingleCategory = function (message, category_id) {
      $http.delete(DOMAIN_NAME+'categories/'+category_id+'/',{

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      },
      function(response){
        categories.validate_400_response(message, response)
      });
    };

    this.deleteSubCategories = function (message, category_id) {
      let url = ""
      if (category_id === 'all') {
        url = DOMAIN_NAME+'categories/treeall/'
      } else {
        url = DOMAIN_NAME+'categories/'+category_id+'/tree/'
      }
      $http.delete(url,{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      },
      function(response){
        categories.validate_400_response(message, response)
      });
    };

    this.addCategory = function (message, category) {
      category.children = null;
      category.full_name = null;
      if (category.parent.id === 'all') {
        category.parent = null;
      }
      else {
          category.parent = category.parent.id
      }
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'categories/', category, {

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      },
      function(response){
        $rootScope.$emit('UNLOAD')
        categories.validate_400_response(message, response)
      });
    };


    this.updateCategory = function (message, category) {
      category.children = null;
      category.full_name = null;
      if (category.parent.id === 'all') {
        category.parent = null;
      }
      else {
          category.parent = category.parent.id
      }
      $rootScope.$emit('LOAD')
      $http.put(DOMAIN_NAME+'categories/'+category.id+'/', category, {

        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      },
      function(response){
        $rootScope.$emit('UNLOAD')
        categories.validate_400_response(message, response)
      });
    };

    this.validate_400_response = function(message, response){
      if (response.status === 400) {
          if (response.data.hasOwnProperty("parent") &&
            response.data.parent[0] === "parent error") {
              response.data.parent = "Podałeś niewłaściwą kategorię nadrzędną"
          }
          if (response.data.hasOwnProperty("category_name") &&
            response.data.category_name[0] === "category with this category name already exists.") {
              response.data.category_name = "Podana kategoria już istnieje"
          }
        $rootScope.$emit(message, {type: 'novalid', msg: response.data});
      }
      else if (response.status === 403) {
        $rootScope.$emit(message, {type: 'danger', msg: response.data});
      }
      else {
        console.log('inny kod bledu');
      }
    }

}]);


export default app.name;
