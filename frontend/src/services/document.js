import Angular from 'angular';

let app = Angular.module('service.document', []);

app.service('document',
  ["$domainName","$http","$location","$sessionStorage","$rootScope", '$log',
  function($domainName,$http, $location, $sessionStorage, $rootScope, $log) {
    let DOMAIN_NAME=$domainName;
    let documents = this;
    this.getAllDocuments = function (message) {
      $http.get(DOMAIN_NAME+'documents/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };
    this.getDocuments = function (message, query) {
      $log.debug(message);
      $http.get(DOMAIN_NAME+'documents/find/'+query,{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.getDocument = function (message, document_id) {
      $log.debug(message);
      $http.get(DOMAIN_NAME+'documents/'+document_id+'/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function() {
        $rootScope.$emit(message+'.notfound', {type: 'danger', msg: "Dokument nie istnieje"});
    });
    };

    this.getExerciseRecipes = function (message, document_id, outerIndex) {
      $log.debug(message);
      $http.get(DOMAIN_NAME+'documents/'+document_id+'/recipes/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data,
                                     doc_id: outerIndex});
      }, function() {
        $rootScope.$emit(message+'.notfound', {type: 'danger', msg: "Dokument nie istnieje"});
    });
    };

    this.createDocument = function (message, document) {
      for (var index of Object.keys(document.exercise_recipes)) {
        document.exercise_recipes[index].exercise = document.exercise_recipes[index].exercise.id;
      }
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'documents/', document, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        documents.validate_400_response(message, response)
      });
    };

    this.updateDocument = function (message, document) {

      for (var index of Object.keys(document.exercise_recipes)) {
        document.exercise_recipes[index].exercise = document.exercise_recipes[index].exercise.id;
        document.exercise_recipes[index].position_number = null
      }
      $rootScope.$emit('LOAD')
      $http.put(DOMAIN_NAME+'documents/'+document.id+'/', document, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        documents.validate_400_response(message, response)
      });
    };

    this.deleteDocument = function (message, document_id) {
      $http.delete(DOMAIN_NAME+'documents/'+document_id+'/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.deleteDocumentRecipe = function (message, document_id, recipe_id) {
      $http.delete(DOMAIN_NAME+'documents/'+document_id+'/recipes/'+recipe_id+'/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    };

    this.createDocumentRecipe = function (message, recipe_data) {
      for (var index in recipe_data) {
        if (recipe_data.hasOwnProperty(index)) {
          recipe_data[index].exercise = recipe_data[index].exercise.id;
        }
      }
      // recipe_data.exercise = recipe_data.exercise.id;
      // recipe_data.document = recipe_data.selectedType.id;
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'documents/'+recipe_data[0].document+'/recipes/', recipe_data, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        documents.validate_400_response(message, response)
      });
    };

    this.generatePdfDocument = function (message, document_id) {
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'documents/'+document_id+'/generate/',{}, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response) {
        if (response.status === 400) {
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message+'.error', {type: 'invalid', msg: response.data,
                        error: response.data});
        }
        else {
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message+'.error', {type: 'danger', msg: "Problem przy generowaniu",
                        error: response.data});
          console.log('inny kod bledu');
        }

    });
    };

    this.generateZipPackage = function (message, document_id) {
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'documents/'+document_id+'/generatetex/',{}, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response) {
        $rootScope.$emit('UNLOAD')
        if (response.status === 400) {
          $rootScope.$emit(message+'.error', {type: 'invalid', msg: response.data,
                        error: response.data});
        }
        else {
          $rootScope.$emit(message+'.error', {type: 'danger', msg: "Problem przy generowaniu",
                        error: response.data});
          console.log('inny kod bledu');
        }
    });
    };

    this.updateExerciseRecipes = function (message, document, outerIndex) {
      let documentCopy = angular.copy(document);
      for (var index of Object.keys(documentCopy.exercise_recipes)) {
        documentCopy.exercise_recipes[index].exercise = documentCopy.exercise_recipes[index].exercise.id;
      }
      $rootScope.$emit('LOAD')
      $http.put(DOMAIN_NAME+'documents/'+documentCopy.id+'/recipes/', documentCopy.exercise_recipes, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data,
                                     doc_id: outerIndex});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        documents.validate_400_response(message, response)
      },function(response) {
        $rootScope.$emit('UNLOAD')
        if (response.status === 400) {
          $rootScope.$emit(message+'.error', {type: 'invalid', msg: response.data,
                        error: response.data});
        }
        else {
          $rootScope.$emit(message+'.error', {type: 'danger', msg: "Wystąpił problem przy aktualizacji szablonów zadań dla dokumentu.",
                        error: response.data});
          console.log('inny kod bledu');
        }
    });
    };

    this.validate_400_response = function(message, response){
      if (response.status === 400) {
        $rootScope.$emit(message, {type: 'novalid', msg: response.data});
      }
      else {
        console.log('inny kod bledu');
      }
    }

}]);


export default app.name;
