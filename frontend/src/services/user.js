import Angular from 'angular';

let app = Angular.module('shared.services', []);

app.service('user',
  ['$domainName', "$http","$location","$sessionStorage","$rootScope", "$window",
  function($domainName, $http, $location, $sessionStorage, $rootScope, $window) {
    let DOMAIN_NAME=$domainName;
    let users = this;
    this.log = function (username, password) {
      let credentials = {};
      credentials.username = username;
      credentials.password = password;
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'api-token-auth/',
        credentials).then(function(response){
          $rootScope.$emit('UNLOAD')
          let token = response.data['token'];
          let is_admin = response.data['is_admin'];
          let exp = response.data['exp'];
          // credentials = {};
          $sessionStorage.put('user_token', token);
          $sessionStorage.put('username_sidzbmd', username);
          $sessionStorage.put('is_administrator_sidzbmd', is_admin);
          $sessionStorage.put('exp_sidzbmd', exp);
          $rootScope.$emit('user.log', {type: 'success', msg: 'Uzytkownik został zalogowany'});
      }, function(response) {
        $rootScope.$emit('UNLOAD')
        if (response.status === 429) {
          $rootScope.$emit('user.log', {type: 'danger', msg: response.data['detail']});
        }
        else {
          $rootScope.$emit('user.log', {type: 'danger', msg: "Błędny login lub hasło"});
        }

        $sessionStorage.remove('username_sidzbmd');
        // $sessionStorage.put('user_token', 'temporary_token');
        // $sessionStorage.put('username_sidzbmd', username);
    });
    };

    this.logout = function () {
      $sessionStorage.remove('user_token');
      $sessionStorage.remove('username_sidzbmd');
      $sessionStorage.remove('is_administrator_sidzbmd');
      $sessionStorage.remove('exp_sidzbmd');
      $window.localStorage.clear();
      $rootScope.$emit('user.logout', {type: 'success', msg: 'Uzytkownik zostal wylogowany'});
    }

    this.get_exp_value = function() {
      return $sessionStorage.get('exp_sidzbmd');
    }

    this.check_token_expiration = function(callback) {
      let currentTime = (new Date()).getTime() / 1000|0;
      let tokenExpireTime = $sessionStorage.get('exp_sidzbmd');
      let token = $sessionStorage.get('user_token');
      // if (currentTime - tokenExpireTime < 0) {
      if (true) {
        $http.post(DOMAIN_NAME+'api-token-refresh/',
          {'token': token}).then(function(response){
            let token = response.data['token'];
            let is_admin = response.data['is_administrator'];
            let exp = response.data['exp'];
            $sessionStorage.put('user_token', token);
            $sessionStorage.put('is_administrator_sidzbmd', is_admin);
            $sessionStorage.put('exp_sidzbmd', exp);
            callback();

          }, function() {
            this.logout();
        });
      }
    }

    this.isUnloggedUser = function() {
      let user_token = $sessionStorage.get('user_token');
      if (user_token) {
        return false;
      }
      else {
        return true;
      }
    }

    this.getUsername = function() {
      let username = $sessionStorage.get('username_sidzbmd');
      return username;
    }

    this.isAdministrator = function() {
      let is_administrator = $sessionStorage.get('is_administrator_sidzbmd');
      if (is_administrator === 'true') {
        is_administrator=true;
      }
      else {
        is_administrator=false;
      }
      return is_administrator;
    }

    this.getGroups = function(message) {
      $http.get(DOMAIN_NAME+'user/groups/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }
    this.getAllGroups = function(message) {
      $http.get(DOMAIN_NAME+'groups/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.getAccessGroupsProfile = function(message) {
      $http.get(DOMAIN_NAME+'user/exercises/permissions/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.getAccessGroupsProfileByQuery = function(message, query) {
      $http.get(DOMAIN_NAME+'user/exercises/permissions/find/'+query,{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.getSingleAccessGroupsProfile = function(message, user_permission_id) {
      $http.get(DOMAIN_NAME+'user/exercises/permissions/'+user_permission_id+'/',{
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function() {
        $rootScope.$emit(message+'.notfound', {type: 'danger', msg: "Zadanie nie istnieje"});
    });
    }

    this.updateAccessGroupsProfile = function(message, profile, selected_group) {
      profile.group = selected_group.id;
      $rootScope.$emit('LOAD')
      $http.put(DOMAIN_NAME+'user/exercises/permissions/'+profile.id+'/', profile, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        users.validate_400_response(message, response)
      });
    }

    this.deleteAccessGroupsProfile = function(message, profile) {
      $http.delete(DOMAIN_NAME+'user/exercises/permissions/'+profile.id+'/', {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      });
    }

    this.createAccessGroupsProfile = function(message, profile, selected_group) {
      profile.group = selected_group.id;
      $rootScope.$emit('LOAD')
      $http.post(DOMAIN_NAME+'user/exercises/permissions/', profile, {
        headers: {'Authorization': 'JWT '+$sessionStorage.get('user_token')}
      }).then(function(response){
        // 'user.getLoggedUserData'
          $rootScope.$emit('UNLOAD')
          $rootScope.$emit(message, {type: 'success', msg: response.data});
      }, function(response){
        $rootScope.$emit('UNLOAD')
        users.validate_400_response(message, response)
      });
    }

    this.validate_400_response = function(message, response){
      if (response.status === 400) {
          // if (response.data.hasOwnProperty("parent") &&
          //   response.data.parent[0] === "parent error") {
          //     response.data.parent = "Podałeś niewłaściwą kategorię nadrzędną"
          // }
          // if (response.data.hasOwnProperty("category_name") &&
          //   response.data.category_name[0] === "category with this category name already exists.") {
          //     response.data.category_name = "Podana kategoria już istnieje"
          // }
        $rootScope.$emit(message, {type: 'novalid', msg: response.data});
      }
      else {
        console.log('inny kod bledu');
      }
    }

}]);


export default app.name;
