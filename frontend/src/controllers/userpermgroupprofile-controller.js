import Angular from 'angular';

import UserService from '../services/user.js';
import SessionStorage from 'angular-sessionstorage';
import UserPermissionCreateComponent from '../components/create-user-permission-component.js';
import UserPermissionUpdateComponent from '../components/create-user-permission-component.js';


let app = Angular.module('controllers.userpermgroupprofile', [UserService, UserPermissionCreateComponent, UserPermissionUpdateComponent]);

app.controller('UserPermGroupProgileCtrl',['$uibModal', '$document', '$log', "$scope", "$element", '$rootScope','user', '$routeParams',
  function($uibModal, $document , $log, $scope, $element, $rootScope, UserService, $routeParams){


    var $ctrl = this;

    $ctrl.isAdmin = function(){
      return UserService.isAdministrator();
    };

    $ctrl.getAccessGroupsProfile = function(){

      let search_query = "";
      if ($ctrl.advanced_search == true) {
        Angular.forEach($ctrl.adv_search, function(value, key) {
          search_query = search_query +'&'+ key + '='+value;
        });
      } else if ($ctrl.advanced_search == false) {
        let simply = $ctrl.sim_search.simply;
        let operation = $ctrl.sim_search.operation;
        search_query = '&name='+simply+
                       '&exercisevis='+simply+
                       '&introvis='+simply+
                       '&group='+simply+
                       '&hintvis='+simply+
                       '&solutionvis='+simply+
                       '&owner='+simply+
                       '&operation='+operation;
      }
      let query = '?page='+$ctrl.bigCurrentPage+'&pagesize='+$ctrl.apiPageSize+search_query;



      UserService.getAccessGroupsProfileByQuery('user.getAccessGroupsProfile', query);
    };

    let registerScope1 = $rootScope.$on('user.getAccessGroupsProfile', function(event, data) {
      if (data.type === 'success') {
        $scope.usergroupprofiles=data.msg.userpermissions;
        $ctrl.bigTotalItems=data.msg.total;
      }
    });
    $scope.$on('$destroy', registerScope1);

    // $ctrl.getAccessGroupsProfile();

    $ctrl.items = ['item1', 'item2', 'item3'];

    $ctrl.default_item = {
      "name": "",
      "group": 1,
      "owner": UserService.getUsername(),
      "exercise_vis": true,
      "intro_vis": true,
      "hint_vis": true,
      "solution_vis": true
    };

    $ctrl.animationsEnabled = true;
    $ctrl.deleteUserGroupProfile = function (selectedData) {
      UserService.deleteAccessGroupsProfile('user.deleteAccessGroupsProfile',
                                            selectedData);
    };

    let registerScope2 = $rootScope.$on('user.deleteAccessGroupsProfile', function(event, data) {
      if (data.type === 'success') {
        if ($ctrl.multipleUserPermissionMode) {
          $ctrl.getAccessGroupsProfile();
        }
        else {
            $location.path("/user/accessprofiles");
        }
      }
    });
    $scope.$on('$destroy', registerScope2);

    $ctrl.openComponentModal = function (selectedData) {
      let objCopy = angular.copy(selectedData);
      var modalInstance = $uibModal.open({
        animation: $ctrl.animationsEnabled,
        component: 'modalComponent',
        resolve: {
          items: function () {
            return objCopy;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        // $ctrl.selected = selectedItem;
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
    };

    let registerScope3 = $rootScope.$on('UserService.updateAccessGroupsProfile', function(event, data) {
      if (data.type === 'success') {
        if ($ctrl.multipleUserPermissionMode) {
          $ctrl.getAccessGroupsProfile();
        }
        else {
          $ctrl.getSingleUserPermission($routeParams['userPermissionId']);
        }
      }
    });
    $scope.$on('$destroy', registerScope3);

    $ctrl.openComponentModalCreate = function () {
      let objCopy = angular.copy($ctrl.default_item);
      var modalInstance = $uibModal.open({
        animation: $ctrl.animationsEnabled,
        component: 'modalComponentCreate',
        resolve: {
          items: function () {
            return objCopy;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
    };

    let registerScope4 = $rootScope.$on('UserService.createAccessGroupsProfile', function(event, data) {
      if (data.type === 'success') {
        $ctrl.getAccessGroupsProfile();
      }
    });
    $scope.$on('$destroy', registerScope4);

    let registerScope5 = $rootScope.$on('UserService.createAccessGroupsProfile', function(event, data) {
      if (data.type === 'success') {
        $ctrl.getAccessGroupsProfile();
      }
    });
    $scope.$on('$destroy', registerScope5);

    $ctrl.getSingleUserPermission = function(userpermission_id) {
      UserService.getSingleAccessGroupsProfile("UserPermission.getSingleUserPermission",
                                  userpermission_id);
    }

    let registerScope6 = $rootScope.$on('UserPermission.getSingleUserPermission', function(event, data) {
      if (data.type === 'success') {
        $scope.usergroupprofiles=[data.msg];
        $ctrl.bigTotalItems=1;
      }
    });
    $scope.$on('$destroy', registerScope6);
    let registerScope7 = $rootScope.$on('UserPermission.getSingleUserPermission.notfound', function(event, data) {
      $location.path("/pageNotFound");
    });
    $scope.$on('$destroy', registerScope7);

    $ctrl.goToSingleUserPermissionPage = function(userpermission_id){
      window.open("user/accessprofiles/"+userpermission_id, '_blank');
    }

    $ctrl.modalUserPermComponentDelete = function (selectedData) {
      let objCopy = angular.copy(selectedData);
      let obj2 = {id: selectedData.id,
                  body: "Czy usunąć profil dostępu o nazwie "+ selectedData.name +"?",
                  header: "Usuwanie profilu dostępu"}
      var modalInstance = $uibModal.open({
        animation: $ctrl.animationsEnabled,
        component: 'modalComponentConfirm',
        resolve: {
          items: function () {
            return obj2;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $ctrl.deleteUserGroupProfile(selectedData);
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
    };


    $ctrl.clear_search = function(){
      $ctrl.adv_search = {
        "document-title": "",
        name: "",
        group: "",
        exercise_vis: "",
        introvis: "",
        hintvis: "",
        solutionvis: "",
        operation: "and"
      };
      $ctrl.sim_search = {
        simply: "",
        operation: "or"
      }

    };
    $ctrl.clear_search();
    $ctrl.advanced_search = false;
    if ($routeParams.hasOwnProperty('userPermissionId')) {
      $ctrl.singleUserPermissionMode=true;
      $ctrl.multipleUserPermissionMode=false;
    }
    else {
      $ctrl.singleUserPermissionMode=false;
      $ctrl.multipleUserPermissionMode=true;
    }

    $scope.maxSize = 3;
    $ctrl.bigTotalItems = 0;
    $ctrl.bigCurrentPage = 1;
    $ctrl.apiPageSize = 10;

    $ctrl.options = {
      apiPageSize: [5, 10, 20, 40, 60, 90]};

    if ($ctrl.multipleUserPermissionMode) {
      $ctrl.getAccessGroupsProfile();
    }
    else {
      $ctrl.getSingleUserPermission($routeParams['userPermissionId']);
    }
}]);

export default app.name;
