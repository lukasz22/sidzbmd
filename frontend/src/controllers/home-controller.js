import Angular from 'angular';

import UserService from '../services/user.js';
import SessionStorage from 'angular-sessionstorage';

let app = Angular.module('controllers.home', [UserService])
  .controller('HomeCtrl',["$scope", "$element", '$rootScope','user',
  function($scope, $element, $rootScope, UserService){



  var $ctrl = this;
  $ctrl.unlogged_user = UserService.isUnloggedUser();
  $ctrl.username = UserService.getUsername();
  $ctrl.is_admin = UserService.isAdministrator();

  let registerScope1 = $rootScope.$on('user.log', function(event, data) {
		if (data.type === 'success') {
      $ctrl.unlogged_user = UserService.isUnloggedUser();
      $ctrl.username = UserService.getUsername();
      $ctrl.is_admin = UserService.isAdministrator();
	 	}
	});
	$scope.$on('$destroy', registerScope1);

  let registerScope2 = $rootScope.$on('user.logout', function(event, data) {
		if (data.type === 'success') {
      $ctrl.unlogged_user = UserService.isUnloggedUser();
      $ctrl.username = "";
      $ctrl.is_admin = UserService.isAdministrator();
	 	}
	});
	$scope.$on('$destroy', registerScope2);


}]);

export default app.name;
