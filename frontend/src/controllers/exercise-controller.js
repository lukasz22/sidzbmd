import Angular from 'angular';

import ExerciseService from '../services/exercise.js';
import SessionStorage from 'angular-sessionstorage';
import CategoryService from '../services/categories.js';
import UserService from '../services/user.js';
import { Base64 } from 'js-base64';

import FileDirective from '../directives/file-directive.js';
import AttachmentCreateComponent from '../components/create-attachment-component.js';
import AttachmentUpdateComponent from '../components/update-attachment-component.js';
import ExerciseCreateComponent from '../components/create-exercise-component.js';
import ExerciseUpdateComponent from '../components/update-exercise-component.js';



let app = Angular.module('controllers.exercise', [ExerciseService, CategoryService, FileDirective, AttachmentCreateComponent, ExerciseCreateComponent, ExerciseUpdateComponent, AttachmentUpdateComponent])
  .controller('ExerciseCtrl',['$uibModal', "$scope", "$element", '$rootScope','exercise', 'category', 'user','$log', '$compile', '$window', '$routeParams', '$location',
  function($uibModal, $scope, $element, $rootScope, ExerciseService, CategoryService, UserService, $log, $compile, $window, $routeParams, $location){
  var $ctrl = this;

  $scope.clear_search = function(){
    $scope.adv_search = {
      title: "",
      intro: "",
      content: "",
      hint: "",
      solution: "",
      owner: "",
      operation: "and"
    };
    $scope.sim_search = {
      simply: "",
      operation: "or"
    }
  };

  $ctrl.getLanguages = function(){
    CategoryService.getAllLanguages('exercise.getLanguages');
  };

  let registerScope1 = $rootScope.$on('exercise.getLanguages', function(event, data) {
    if (data.type === 'success') {
      data.msg=[{id: 'all', name: 'Wszystkie'}, ...data.msg] ;
      $ctrl.languages_opt=data.msg;
      $ctrl.languages_opt_single=$ctrl.languages_opt[0];
      $ctrl.getExercises();
    }
    });
    $scope.$on('$destroy', registerScope1);

  $ctrl.singleExpand = function(i) {
    if ($scope.exercises[i].is_open) {
      $scope.exercises[i].intro_is_open = true;
      $scope.exercises[i].task_content_is_open = true;
      $scope.exercises[i].hint_is_open = true;
      $scope.exercises[i].solution_is_open = true;
      $scope.exercises[i].attachments_is_open = true;
      for (let j = 0; j < $scope.exercises[i].attachments.length; j++) {
          $scope.exercises[i].attachments[j].attachment_is_open = true;
      }
    }
    else {
      $scope.exercises[i].intro_is_open = false;
      $scope.exercises[i].task_content_is_open = true;
      $scope.exercises[i].hint_is_open = false;
      $scope.exercises[i].solution_is_open = false;
      $scope.exercises[i].attachments_is_open = false;
      for (let j = 0; j < $scope.exercises[i].attachments.length; j++) {
          $scope.exercises[i].attachments[j].attachment_is_open = false;
      }
    }
    if ($scope.isExpand === 1) {
      for (let i = 0; i < $scope.exercises.length; i++) {

      }
    }
  };

  $scope.expand = function() {
    if ($scope.isExpand === 1) {
      for (let i = 0; i < $scope.exercises.length; i++) {
          $scope.exercises[i].is_open = true;
          $scope.exercises[i].intro_is_open = true;
          $scope.exercises[i].task_content_is_open = true;
          $scope.exercises[i].hint_is_open = true;
          $scope.exercises[i].solution_is_open = true;
          $scope.exercises[i].attachments_is_open = true;
          for (let j = 0; j < $scope.exercises[i].attachments.length; j++) {
              $scope.exercises[i].attachments[j].attachment_is_open = false;
          }
      }
    }
    else {
      for (let i = 0; i < $scope.exercises.length; i++) {
          $scope.exercises[i].is_open = false;
          $scope.exercises[i].intro_is_open = false;
          $scope.exercises[i].task_content_is_open = true;
          $scope.exercises[i].hint_is_open = false;
          $scope.exercises[i].solution_is_open = false;
          $scope.exercises[i].attachments_is_open = false;
          for (let j = 0; j < $scope.exercises[i].attachments.length; j++) {
              $scope.exercises[i].attachments[j].attachment_is_open = false;
          }
      }
    }
  };

  $ctrl.getExercises = function(){
    // let search_query =
    let search_query = "";
    if ($ctrl.advanced_search == true) {
      Angular.forEach($scope.adv_search, function(value, key) {
        search_query = search_query +'&'+ key + '='+value;
      });
    } else if ($ctrl.advanced_search == false) {
      let simply = $scope.sim_search.simply;
      let operation = $scope.sim_search.operation;
      search_query = '&title='+simply+
                     '&intro='+simply+
                     '&content='+simply+
                     '&hint='+simply+
                     '&solution='+simply+
                     '&owner='+simply+
                     '&operation='+operation;
    }
    let query = '';

    if ($ctrl.languages_opt_single.id === 'all') {
      query = search_query;
    }
    else {
        query='language='+$ctrl.languages_opt_single.id+search_query;
    }

    if (!$ctrl.showOnlySelectedItems) {
      query='page='+$scope.bigCurrentPage+'&pagesize='+$ctrl.apiPageSize+'&'+query;
    }
    if ($ctrl.node.id === 'all') {
      query='?'+query;
    }
    else{
      query='?category='+$ctrl.node.id+'&'+query;
    }
    ExerciseService.getExercises('exercise.getExercises', query);
  };

  let registerScope2 = $rootScope.$on('exercise.getExercises', function(event, data) {
    if (data.type === 'success') {
      let exercise_table = ExerciseService.getExerciseTable();
      let exercises = [];
      for (let index = 0; index < data.msg.exercises.length; index++){
        if (exercise_table[data.msg.exercises[index].id]) {
          data.msg.exercises[index].checkValue=true;
          exercises.push(data.msg.exercises[index]);
        }
        else {
          data.msg.exercises[index].checkValue=false;
        }
      }
      if ($ctrl.showOnlySelectedItems) {
        // $ctrl.apiPageSize
        $ctrl.origTickExercises=exercises;
        $ctrl.slicePartExercises(exercises);
      }
      else {
        $scope.exercises=data.msg.exercises;
        $ctrl.bigTotalItems=data.msg.total;
      }
      $scope.expand(data.msg.exercises);
    }
    });
    $scope.$on('$destroy', registerScope2);

  $ctrl.slicePartExercises = function(exercises){
    let begin = ($scope.bigCurrentPage-1)*$ctrl.apiPageSize;
    $scope.exercises=exercises.slice(begin, begin+$ctrl.apiPageSize);
    $ctrl.bigTotalItems=exercises.length;
  }

  $ctrl.goToOtherPage = function(){
    if ($ctrl.showOnlySelectedItems) {
      $ctrl.slicePartExercises($ctrl.origTickExercises);
    }
    else {
      $ctrl.getExercises();
    }
  }

  $ctrl.selectPage = function(){
    let value = $ctrl.selectedPage;
    for (var i = 0; i < $scope.exercises.length; i++) {
      ExerciseService.updateExerciseTable($scope.exercises[i].id, value);
      $scope.exercises[i].checkValue=value;
    }
    $ctrl.numSelectedExercises = ExerciseService.getNumSelectedExercise();
  };

  $ctrl.unselectAllPages = function(){
    ExerciseService.clearExerciseTable();
    for (var i = 0; i < $scope.exercises.length; i++) {
      $scope.exercises[i].checkValue=false;
    }
    $ctrl.numSelectedExercises = ExerciseService.getNumSelectedExercise();
  };

  $ctrl.toggle = function(obj){
    $ctrl.node=obj;

  };

  $ctrl.encodeImage = function(image){
    let result = Base64.decode(image);
    return result;
  }

  $ctrl.isOwner = function(username){
    let owner_orgin = UserService.getUsername();

    if (owner_orgin === username ) {
      return true;
    }
    else {
      return false;
    }
  };

  $ctrl.isAdmin = function(){
    let zmienna = UserService.isAdministrator();
    return zmienna;
  };

  $ctrl.updateLocalStorage = function(exercise_id, value){
    ExerciseService.updateExerciseTable(exercise_id, value);
    for (var i = 0; i < $scope.exercises.length; i++) {
      if ($scope.exercises[i].id===exercise_id) {
        $scope.exercises[i].checkValue=value;
      }
    }
    if ($ctrl.showOnlySelectedItems) {
      if (value === false) {
        $ctrl.origTickExercises =
          $ctrl.origTickExercises.filter(function(item){
            return !(item.id===exercise_id);
          });
        // $ctrl.origTickExercises.splice(index, 1);
        $ctrl.slicePartExercises($ctrl.origTickExercises);
      }
    }
    else {

    }
    $ctrl.numSelectedExercises = ExerciseService.getNumSelectedExercise();
  }

  $ctrl.modalExerciseComponentDelete = function (exercise) {
      let obj2 = {id: exercise.id,
                  body: "Czy usunąć pozycję "+ exercise.title +"?",
                  header: "Usuwanie zadania"}
      // objCopy.parent = parent;
      var modalInstance = $uibModal.open({
        animation: true,
        component: 'modalComponentConfirm',
        resolve: {
          items: function () {
            return obj2;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $ctrl.deleteExercise(exercise);
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
  };

  $ctrl.deleteExercise = function (selectedData) {
    ExerciseService.deleteExercise('exercise.delete',
                                          selectedData);
  };

  let registerScope3 = $rootScope.$on('exercise.delete', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleExerciseMode) {
        $ctrl.getExercises();
      }
      else {
        $location.path("/exercises");
      }
    }
  });
  $scope.$on('$destroy', registerScope3);

  $ctrl.modalAttachmentsComponentDelete = function (exercise) {
      let obj2 = {id: exercise.id,
                  body: "Czy usunąć wszystkie załączniki dla zadania "+ exercise.title +"?",
                  header: "Usuwanie załączników"}
      // objCopy.parent = parent;
      var modalInstance = $uibModal.open({
        animation: true,
        component: 'modalComponentConfirm',
        resolve: {
          items: function () {
            return obj2;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $ctrl.deleteAttachments(exercise.id);
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
  };

  $ctrl.deleteAttachments = function (exercise_id) {
    ExerciseService.deleteAttachments('attachment.delete.all', exercise_id);
  };

  let registerScope4 = $rootScope.$on('attachment.delete.all', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleExerciseMode) {
        $ctrl.getExercises();
      }
      else {
        $ctrl.getSingleExercise($routeParams['exerciseId']);
      }
    }
  });
  $scope.$on('$destroy', registerScope4);

  $ctrl.modalAttachmentComponentDelete = function (exercise, attachment) {
      let obj2 = {id: exercise.id,
                  body: "Czy usunąć załącznik '"+ attachment.name +"' z zadania '"+exercise.title+"' ?",
                  header: "Usuwanie załącznika"}
      // objCopy.parent = parent;
      var modalInstance = $uibModal.open({
        animation: true,
        component: 'modalComponentConfirm',
        resolve: {
          items: function () {
            return obj2;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $ctrl.deleteAttachment(exercise.id, attachment.id);
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
  };

  $ctrl.deleteAttachment = function (exercise_id, attachment_id) {
    ExerciseService.deleteAttachment('attachment.delete.one', exercise_id, attachment_id);
  };

  let registerScope5 = $rootScope.$on('attachment.delete.one', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleExerciseMode) {
        $ctrl.getExercises();
      }
      else {
        $ctrl.getSingleExercise($routeParams['exerciseId']);
      }
    }
  });
  $scope.$on('$destroy', registerScope5);

  // create attachment
  $ctrl.modalAttachmentComponentCreate = function (exercise_id) {
    // let objCopy1 = angular.copy($ctrl.default_item);
    // let objCopy2 = angular.copy($ctrl.default_item);
    var modalInstance = $uibModal.open({
      animation: $ctrl.animationsEnabled,
      component: 'modalAttachmentComponentCreate',
      resolve: {
        items: function () {
          return Object.assign({}, $ctrl.default_item);
        },
        exercise_id: function () {
          return Object.assign(exercise_id, $ctrl.default_item);
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      // $ctrl.selected = selectedItem;
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };

  let registerScope6 = $rootScope.$on('Attachment.createAttachments.modal', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleExerciseMode) {
        $ctrl.getExercises();
      }
      else {
        $ctrl.getSingleExercise($routeParams['exerciseId']);
      }
    }
  });
  $scope.$on('$destroy', registerScope6);

  // update attachment
  $ctrl.modalAttachmentComponentUpdate = function (selectedData) {
    let objCopy = angular.copy(selectedData);
    let items = {};
    items.attachments=[objCopy];
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'modalAttachmentComponentUpdate',
      resolve: {
        items: function () {
          return items;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      // $ctrl.selected = selectedItem;
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };

  let registerScope7 = $rootScope.$on('Attachment.updateAttachment.modal', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleExerciseMode) {
        $ctrl.getExercises();
      }
      else {
        $ctrl.getSingleExercise($routeParams['exerciseId']);
      }
    }
  });
  $scope.$on('$destroy', registerScope7);

  // create exercise
  $ctrl.createExerciseModal = function (sel_category) {
    // let objCopy = angular.copy($ctrl.default_item);
    $ctrl.default_item2 = {
      "owner": { "username": UserService.getUsername()},
      "category": sel_category
    };
    var modalInstance = $uibModal.open({
      animation: $ctrl.animationsEnabled,
      component: 'modalExerciseComponentCreate',
      resolve: {
        items: function () {
          return Object.assign({}, $ctrl.default_item2);
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      // $ctrl.selected = selectedItem;
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };

  let registerScope8 = $rootScope.$on('Exercise.createExercise.modal', function(event, data) {
    if (data.type === 'success') {
      $ctrl.getExercises();
    }
  });
  $scope.$on('$destroy', registerScope8);

  // update exercise
  $ctrl.updateExerciseModal = function (selectedData) {
    let objCopy = angular.copy(selectedData);
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'modalExerciseComponent',
      resolve: {
        items: function () {
          return objCopy;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      // $ctrl.selected = selectedItem;
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };

  let registerScope9 = $rootScope.$on('Exercise.updateExercise.modal', function(event, data) {
    if (data.type === 'success') {
      if ($ctrl.multipleExerciseMode) {
        $ctrl.getExercises();
      }
      else {
        $ctrl.getSingleExercise($routeParams['exerciseId']);
      }
    }
  });
  $scope.$on('$destroy', registerScope9);


  $ctrl.advanced_search = false;
  if ($routeParams.hasOwnProperty('exerciseId')) {
    $ctrl.singleExerciseMode=true;
    $ctrl.multipleExerciseMode=false;
  }
  else {
    $ctrl.singleExerciseMode=false;
    $ctrl.multipleExerciseMode=true;
  }

  $ctrl.getSingleExercise = function(exercise_id) {
    ExerciseService.getExercise("Exercise.getSingleExercise",
                                exercise_id);
  }

  let registerScope10 = $rootScope.$on('Exercise.getSingleExercise', function(event, data) {
    if (data.type === 'success') {
      $scope.exercises=[data.msg];
      $ctrl.bigTotalItems=1;
    }
  });
  $scope.$on('$destroy', registerScope10);
  let registerScope11 = $rootScope.$on('Exercise.getSingleExercise.notfound', function(event, data) {
    $location.path("/pageNotFound");
  });
  $scope.$on('$destroy', registerScope11);

  $ctrl.goToSingleExercisePage = function(exercise_id){
    window.open("exercises/"+exercise_id, '_blank');
    // $location.path("/exercises/"+$ctrl.items.exercise_recipes[index].exercise.id);
  }

  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };

  $scope.status = {
    isopen: false
  };


  $scope.clear_search();
  $ctrl.numSelectedExercises = ExerciseService.getNumSelectedExercise();
  $ctrl.node={};
  $ctrl.node.id = 'all';
  $ctrl.node.category_name = 'Wszystko';
  $scope.maxSize = 3;
  $ctrl.bigTotalItems = 0;
  $scope.bigCurrentPage = 1;
  $ctrl.apiPageSize = 10;
  $ctrl.deleteSingleAttVisible = true;
  $ctrl.options = {
    apiPageSize: [5, 10, 20, 40, 60, 90],
    attachment_types: [
      { "id": "Główna treść zadania",
        "name": "exercise_content"
      },
      { "id": "Wprowadzenie do zadania",
        "name": "exercise_intro"
      },
      { "id": "Wskazówka do zadania",
        "name": "exercise_hint"
      },
      { "id": "Rozwiązanie zadania",
        "name": "exercise_solution"
      }
    ]
  };
  if ($ctrl.multipleExerciseMode) {
    $ctrl.getLanguages();
  }
  else {
    $ctrl.getSingleExercise($routeParams['exerciseId']);
  }

  $ctrl.test = function(){
    console.log($ctrl.languages_opt_single2);
  }

  $ctrl.openInNewWindow = function(base_64_string) {
    let image = "data:image/png;base64, "+base_64_string
    $window.open(image)
  }

  $ctrl.attachment_filter = function(name, attachments) {
    let result = attachments.filter(attachment => attachment.attachment_type === name);
    if (result.length){
      return true;
    }
    return false;
  }

  let registerScope1_category = $rootScope.$on('category.getAllCategoriesTree', function(event, data) {
    if (data.type === 'success') {
      $ctrl.node={};
      $ctrl.node.id = 'all';
      $ctrl.node.category_name = 'Wszystko';
      if ($ctrl.multipleExerciseMode) {
        $ctrl.getExercises();
      }
      else {
        $ctrl.getSingleExercise($routeParams['exerciseId']);
      }
    }
  });
  $scope.$on('$destroy', registerScope1_category);

}]);

export default app.name;
