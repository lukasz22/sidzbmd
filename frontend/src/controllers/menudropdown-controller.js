import Angular from 'angular';

import CategoryService from '../services/categories.js';
import SessionStorage from 'angular-sessionstorage';
import UserService from '../services/user.js';


import CategoryCreateComponent from '../components/create-category-component.js';
import CategoryUpdateComponent from '../components/update-category-component.js';


let app = Angular.module('controllers.test', [CategoryService, UserService, CategoryCreateComponent, CategoryUpdateComponent])
  .controller('TestCtrl',["$uibModal", "$log", "$scope", "$element", '$rootScope','category', 'user',
  function($uibModal, $log, $scope, $element, $rootScope, CategoryService, UserService){



  var usersctrl = this;

  usersctrl.toggle = function(id){
    usersctrl.selected.isOpen = !usersctrl.selected.isOpen;
    let obj = usersctrl.getObject($scope.data, id);
    usersctrl.selected = obj;
    obj.isOpen = !obj.isOpen;
  };

  usersctrl.getObject = function(object, id){
    if(object.hasOwnProperty('id') && object["id"]==id)
        return object;
    for(var i=0;i<Object.keys(object).length;i++){
        if(typeof object[Object.keys(object)[i]]=="object"){
            let o=usersctrl.getObject(object[Object.keys(object)[i]], id);
            if(o!=null)
                return o;
        }
    }

    return null;
}

  usersctrl.is_admin = function(){
    return UserService.isAdministrator()
  };

  usersctrl.getCategories = function(){
    CategoryService.getAllCategoriesTree('category.getAllCategoriesTree');
  };

  let registerScope1 = $rootScope.$on('category.getAllCategoriesTree', function(event, data) {
    if (data.type === 'success') {
      let children = []
      for (let index = 0; index < data.msg.length; index++){
        children.push(data.msg[index][0])
      }
      $scope.data=[[{id: 'all',
                   category_name: "Wszystko",
                   children: children,
                   isOpen: true
                    }]];
      usersctrl.selected = $scope.data[0][0];
    }
    });
    $scope.$on('$destroy', registerScope1);

  usersctrl.deleteSingleCategory = function(category_id, ptr){
    CategoryService.deleteSingleCategory('category.deleteSingleCategory', category_id);
  };

  let registerScope2 = $rootScope.$on('category.deleteSingleCategory', function(event, data) {
    if (data.type === 'success') {
      usersctrl.getCategories();
    }
    });
    $scope.$on('$destroy', registerScope2);

  usersctrl.deleteSubCategories = function(category_id, ptr){
    CategoryService.deleteSubCategories('category.deleteSubCategories', category_id);
  };

  let registerScope2b = $rootScope.$on('category.deleteSubCategories', function(event, data) {
    if (data.type === 'success') {
      usersctrl.getCategories();
    }
    });
    $scope.$on('$destroy', registerScope2b);

  usersctrl.default_item = {
    "category_name": "test",
    "parent": ""
  };

  usersctrl.modalCategoryComponentCreate = function (parent, ptr) {
    let objCopy = angular.copy(parent);
    objCopy.parent = parent;
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'modalCategoryComponentCreate',
      resolve: {
        items: function () {
          return objCopy;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });


  };

  let registerScope3 = $rootScope.$on('CategoryService.createCategory', function(event, data) {
    if (data.type === 'success') {
      usersctrl.getCategories();
    }
  });
  $scope.$on('$destroy', registerScope3);

  usersctrl.modalCategoryComponentUpdate = function (category, ptr) {
      let objCopy = angular.copy(category);

      // objCopy.parent = parent;
      var modalInstance = $uibModal.open({
        animation: true,
        component: 'modalCategoryComponentUpdate',
        resolve: {
          items: function () {
            return objCopy;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
  };

  let registerScope4 = $rootScope.$on('CategoryService.updateCategory', function(event, data) {
      if (data.type === 'success' ) {
        usersctrl.getCategories();
      }
      else if (data.type === 'novalid') { $ctrl.backendtype = data.type
      }
  });
  $scope.$on('$destroy', registerScope4);


  usersctrl.getCategories();

  usersctrl.modalCategoryComponentDelete = function (category_id, category_name, ptr) {
      let objCopy = angular.copy(category_id);
      let obj2 = {id: category_id,
                  body: "Czy usunąć pozycję "+ category_name +"?",
                  header: "Usuwanie kategorii"}
      // objCopy.parent = parent;
      var modalInstance = $uibModal.open({
        animation: true,
        component: 'modalComponentConfirm',
        resolve: {
          items: function () {
            return obj2;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        usersctrl.deleteSingleCategory(category_id, ptr);
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
  };

  usersctrl.deleteSubCategoriesComponentDelete = function (category_id, category_name, ptr) {
      let objCopy = angular.copy(category_id);
      let obj2 = {id: category_id,
                  body: "Czy usunąć podkategorie pozycji "+ category_name +"?",
                  header: "Usuwanie podkategorii"}
      // objCopy.parent = parent;
      var modalInstance = $uibModal.open({
        animation: true,
        component: 'modalComponentConfirm',
        resolve: {
          items: function () {
            return obj2;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        usersctrl.deleteSubCategories(category_id, ptr);
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
      });
  };


}]);

export default app.name;
