import Angular from 'angular';

import UserService from '../services/user.js';
import SessionStorage from 'angular-sessionstorage';

let app = Angular.module('controllers.usergroups', [UserService])
  .controller('UserGroupsCtrl',["$scope", "$element", '$rootScope','user',
  function($scope, $element, $rootScope, UserService){



  var usersctrl = this;


  usersctrl.getUserGroups = function(){
    UserService.getGroups('user.getGroups');
  };

  let registerScope1 = $rootScope.$on('user.getGroups', function(event, data) {
    if (data.type === 'success') {
      $scope.usergroups=data.msg;
    }
  });
  $scope.$on('$destroy', registerScope1);

  usersctrl.getUserGroups();

}]);

export default app.name;
