import Angular from 'angular';
import AngularBootstrap from 'angular-ui-bootstrap';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import ngTouch from 'angular-touch';

import NavView from '../templates/nav-view.html';

import UserService from '../services/user.js'
import SessionStorage from 'angular-sessionstorage'


let app = Angular.module('nav.navbarRightCtrl', [SessionStorage, AngularBootstrap, ngAnimate, ngSanitize, ngTouch, UserService]);

app.controller("navbarRightCtrl",
	["$rootScope","$scope","$http","$location","$sessionStorage", 'user', '$timeout', '$uibModal', '$log', function($rootScope, $scope,$http,$location, $sessionStorage, UserService, $timeout, $uibModal, $log){
	var ctrl = this;
	ctrl.credentials = {};
	// ctrl.credentials = {username: "lukasz.bartocha@gmail.com", password: "lukasz123"}
	//ctrl.credentials = {username: "adam.adamowski@gmail.com", password: "adam123"}

	$scope.unloggedUser = UserService.isUnloggedUser();
	if (!$scope.unloggedUser) {
		ctrl.credentials.username=UserService.getUsername()
	}
	ctrl.loginUser = function(keyEvent=undefined){
		if (keyEvent===undefined || keyEvent.which === 13) {
			UserService.log(ctrl.credentials.username, ctrl.credentials.password);
		}
  };

	let registerScope1 = $rootScope.$on('user.log', function(event, data) {
		if (data.type === 'success') {
			ctrl.isUnLoggedUser();
			ctrl.waitForTokenExpired();
		 $location.path('/home');
	 	}
		ctrl.credentials.password="";
	});
	$scope.$on('$destroy', registerScope1);

	ctrl.waitForTokenExpired = function(){
		let exp_value = UserService.get_exp_value();
		let currentTime = (new Date()).getTime() / 1000|0;
		let when_start = exp_value - currentTime;
		ctrl.time_id = $timeout(function() {ctrl.modalTokenExpComponentRefresh()} , 1000*(when_start-60));
	};

	ctrl.goToAcountDetailsPage = function(){
		$location.path("/account");
	};

	ctrl.goToUserGroupsPage = function(){
		$location.path("/user/groups");
	};

	ctrl.goToUserPermGroupProfilePage = function(){
		$location.path("/user/accessprofiles");
	};

	ctrl.goToDocumentPage = function(){
		$location.path("/documents");
	};


	ctrl.logoutUser = function(){
		UserService.logout();
		ctrl.credentials.username="";
		$location.path('/home');
		// $scope.unloggedUser = true;
		ctrl.isUnLoggedUser();
		if (ctrl.time_id) {
			$timeout.cancel(ctrl.time_id);
		}

		};
	ctrl.isUnLoggedUser = function(){
		return UserService.isUnloggedUser();
	}

	ctrl.modalTokenExpComponentRefresh = function () {
		  let exp_value = UserService.get_exp_value();
      let obj2 = {body: "Ważność tokenu wygaśnie za około",
                  header: "Ważność tokenu",
								  exp: exp_value}
      // objCopy.parent = parent;

      var modalInstance = $uibModal.open({
        animation: true,
        component: 'modalComponentConfirm',
				backdrop  : 'static',
				keyboard  : false,
        resolve: {
          items: function () {
            return obj2;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
				UserService.check_token_expiration(ctrl.waitForTokenExpired);
      }, function () {
        $log.info('modal-component dismissed at: ' + new Date());
				let currentTime = (new Date()).getTime() / 1000|0;
				let when_start = exp_value - currentTime;
				$timeout(ctrl.logoutUser, 1000*when_start);
      });
  };

  }]);

export default app.name;
