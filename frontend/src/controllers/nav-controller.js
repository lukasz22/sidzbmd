import Angular from 'angular';
import AngularBootstrap from 'angular-ui-bootstrap';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import ngTouch from 'angular-touch';

import UserService from '../services/user.js'
import SessionStorage from 'angular-sessionstorage'

import NavView from '../templates/nav-view.html';

let app = Angular.module('nav.controller', [SessionStorage, AngularBootstrap, ngAnimate, ngSanitize, ngTouch, UserService]);

app.controller("navbarLeftCtrl",
	["$http","$location","$sessionStorage", 'user','$rootScope','$scope',function($http,$location, $sessionStorage, UserService, $rootScope, $scope){
	var ctrl = this;

	ctrl.goToHomePage = function(){
		$location.path("/home");
	};

	ctrl.goToAboutPage = function(){
		$location.path("/about");
	};

	ctrl.goToUsersPage = function(){
		$location.path("/users");
	};

	ctrl.goToExercisesPage = function(){
		$location.path("/exercises");
	};

	ctrl.goToDocumentPage = function(){
		$location.path("/documents");
	};


	ctrl.goToPreambulePage = function(){
		$location.path("/preambles");
	};

	ctrl.isUnLoggedUser = function(){
		return UserService.isUnloggedUser();
	}

  }]);

export default app.name;
