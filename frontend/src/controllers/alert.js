import Angular from 'angular';


let app = Angular.module('directives', [])
  .controller('AlertCtrl',["$scope", "$element", '$rootScope', function($scope, $element, $rootScope){
    $scope.alerts = [];

   let registerScope1 = $rootScope.$on('user.log', function(event, data) {
    $scope.addAlert(data.type, data.msg)
   });
   $scope.$on('$destroy', registerScope1);
   let registerScope2 = $rootScope.$on('user.logout', function(event, data) {
    $scope.addAlert(data.type, data.msg)
   });
   $scope.$on('$destroy', registerScope2);

   let registerScope3 = $rootScope.$on('Attachment.createAttachments.modal', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Załącznik został utworzony!")
     }
   });
   $scope.$on('$destroy', registerScope3);

   let registerScope4 = $rootScope.$on('Attachment.updateAttachment.modal', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Załącznik został zaktualizowany!")
     }
   });
   $scope.$on('$destroy', registerScope4);

   let registerScope5 = $rootScope.$on('attachment.delete.all', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Załączniki zostały usunięte!")
     }
   });
   $scope.$on('$destroy', registerScope5);

   let registerScope6 = $rootScope.$on('attachment.delete.one', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Załącznik został usunięty!")
     }
   });
   $scope.$on('$destroy', registerScope6);
   // ///////////////////////////////////////////////////////////////////////
   // ///////////////////////////////////////////////////////////////////////
   let registerScope7 = $rootScope.$on('UserService.createAccessGroupsProfile', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Profil dostępowy został utworzony!")
     }
   });
   $scope.$on('$destroy', registerScope7);

   let registerScope8 = $rootScope.$on('UserService.updateAccessGroupsProfile', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Profil dostępowy został zaktualizowany!")
     }
   });
   $scope.$on('$destroy', registerScope8);

   let registerScope9 = $rootScope.$on('user.deleteAccessGroupsProfile', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Profil dostępowy został usunięty!")
     }
   });
   $scope.$on('$destroy', registerScope9);
   // ///////////////////////////////////////////////////////////////////////
   // ///////////////////////////////////////////////////////////////////////

   let registerScope10 = $rootScope.$on('Exercise.updateExercise.modal', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Zadanie zostało zaktualizowane!")
     }
   });
   $scope.$on('$destroy', registerScope10);

   let registerScope11 = $rootScope.$on('Exercise.createExercise.modal', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Zadanie zostało utworzone!")
     }
   });
   $scope.$on('$destroy', registerScope11);

   let registerScope12 = $rootScope.$on('exercise.delete', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Zadanie zostało usunięte!")
     }
   });
   $scope.$on('$destroy', registerScope12);

   // ///////////////////////////////////////////////////////////////////////
   // ///////////////////////////////////////////////////////////////////////

   let registerScope13 = $rootScope.$on('CategoryService.createCategory', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Kategoria została utworzona!")
     }
     else if (data.type === 'danger') {
       $scope.addAlert(data.type, data.msg)
     }
   });
   $scope.$on('$destroy', registerScope13);

   let registerScope14 = $rootScope.$on('CategoryService.updateCategory', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Kategoria została zaktualizowana!")
     }
     else if (data.type === 'danger') {
       $scope.addAlert(data.type, data.msg)
     }
   });
   $scope.$on('$destroy', registerScope14);

   let registerScope15 = $rootScope.$on('category.deleteSingleCategory', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Kategoria została usunięta!")
     }
     else if (data.type === 'danger') {
       $scope.addAlert(data.type, data.msg)
     }
   });
   $scope.$on('$destroy', registerScope15);

   // ///////////////////////////////////////////////////////////////////////
   // ///////////////////////////////////////////////////////////////////////

   let registerScope16 = $rootScope.$on('Document.createDocument.modal', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Szablon dokumentu został utworzony!")
     }
   });
   $scope.$on('$destroy', registerScope16);

   let registerScope17 = $rootScope.$on('Document.updateDocument.modal', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Szablon dokumentu został zaktualizowany!")
     }
   });
   $scope.$on('$destroy', registerScope17);

   let registerScope18 = $rootScope.$on('Document.createDocumentRecipe.modal', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Zadanie zostało dodane do szablonu dokumentu!")
     }
   });
   $scope.$on('$destroy', registerScope18);

   let registerScope19 = $rootScope.$on('document.deleteDocument', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Szablon dokumentu został usunięty!")
     }
   });
   $scope.$on('$destroy', registerScope19);

   let registerScope20 = $rootScope.$on('document.deleteExerciseRecipe', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Zadanie z szablonu dokumentu zostało usunięte!")
     }
   });
   $scope.$on('$destroy', registerScope20);

   // ///////////////////////////////////////////////////////////////////////
   // ///////////////////////////////////////////////////////////////////////

   let registerScope21 = $rootScope.$on('document.generatePdfDocument.open.error', function(event, data) {
     if (data.type === 'invalid') {
       $scope.addAlert('danger', data.msg.validation)
     }
   });
   $scope.$on('$destroy', registerScope21);

   let registerScope22 = $rootScope.$on('document.generatePdfDocument.download.error', function(event, data) {
     if (data.type === 'invalid') {
       $scope.addAlert('danger', data.msg.validation)
     }
   });
   $scope.$on('$destroy', registerScope22);

   let registerScope23 = $rootScope.$on('category.deleteSubCategories', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Podkategorie zostały usunięte!")
     }
     else if (data.type === 'danger') {
       $scope.addAlert(data.type, data.msg)
     }
   });
   $scope.$on('$destroy', registerScope23);
   let registerScope24 = $rootScope.$on('Document.updateExerciseRecipes', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Szablony zadań dla dokumentu zostały zaktualizowane!")
     }
     else if (data.type === 'danger') {
       $scope.addAlert(data.type, data.msg)
     }
   });
   $scope.$on('$destroy', registerScope24);

   let registerScope25 = $rootScope.$on('Document.getExerciseRecipes', function(event, data) {
     if (data.type === 'success') {
       $scope.addAlert(data.type, "Szablony zadań dla dokumentu zostały przywrócone!")
     }
     else if (data.type === 'danger') {
       $scope.addAlert(data.type, data.msg)
     }
   });
   $scope.$on('$destroy', registerScope25);
   var alertCtr = this;
   let registerScope26 = $rootScope.$on('LOAD', function() {
     alertCtr.loading=true
   });
   let registerScope27 = $rootScope.$on('UNLOAD', function() {
     alertCtr.loading=false
   });
   alertCtr.loading = false;
   $scope.$on('$destroy', registerScope26);
   $scope.$on('$destroy', registerScope27);


   // Attachment.updateAttachment.modal
   // Attachment.createAttachments.modal
   // attachment.delete.all
   // attachment.delete.one
   //
   //
   // UserService.createAccessGroupsProfile
   // UserService.updateAccessGroupsProfile
   // user.deleteAccessGroupsProfile
   //
   // Exercise.updateExercise.modal
   // Exercise.createExercise.modal
   // exercise.delete
   //
   //
   //
   // CategoryService.createCategory
   // CategoryService.updateCategory
   // category.deleteSingleCategory
   //
   // Document.createDocument.modal
   // Document.updateDocument.modal
   // Document.createDocumentRecipe.modal
   // document.deleteDocument
   // document.deleteExerciseRecipe

  // danger success
   $scope.addAlert = function(typ, mess) {
     let dana = {type: typ, msg: mess};
     $scope.alerts.push(dana);
   };

   $scope.closeAlert = function(index) {
     $scope.alerts.splice(index, 1);
   };

  //  $scope.$on('$viewContentLoaded', function(){
  //   //Here your view content is fully loaded !!
  // });
}]);

export default app.name;
