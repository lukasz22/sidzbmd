import Angular from 'angular';

import UserService from '../services/user.js';
import PreambuleService from '../services/preambule.js';
import SessionStorage from 'angular-sessionstorage';

let app = Angular.module('controllers.preambule', [UserService, PreambuleService])
  .controller('PreambuleCtrl',['$uibModal', "$scope", "$element", '$rootScope','user', 'preambule', '$log',
  function($uibModal, $scope, $element, $rootScope, UserService, PreambuleService, $log){

  var usersctrl = this;


  // usersctrl.getUserGroups = function(){
  //   UserService.getGroups('user.getGroups');
  // };
  //
  // let registerScope1 = $rootScope.$on('user.getGroups', function(event, data) {
  //   if (data.type === 'success') {
  //     $scope.usergroups=data.msg;
  //   }
  // });
  // $scope.$on('$destroy', registerScope1);

  // usersctrl.getUserGroups();

  usersctrl.getPreambule = function(){
    PreambuleService.getPreambule('preambule.getPreambule', 1);
  };

  let registerScope2 = $rootScope.$on('preambule.getPreambule', function(event, data) {
    if (data.type === 'success') {
      $scope.preambules=[data.msg];
    }
  });
  $scope.$on('$destroy', registerScope2);

  usersctrl.getPreambule();

  usersctrl.modalPreambuleComponentUpdate = function (selectedData) {
    let objCopy = angular.copy(selectedData);
    var modalInstance = $uibModal.open({
      animation: true,
      component: 'modalPreambuleComponent',
      resolve: {
        items: function () {
          return objCopy;
        }
      }
    });
    modalInstance.result.then(function (selectedItem) {
      $scope.preambules= [selectedItem.msg]

    }, function () {
      $log.info('modal-component dismissed at: ' + new Date());
    });
  };

  usersctrl.is_admin = function(){
    return UserService.isAdministrator()
  };

}]);

export default app.name;
